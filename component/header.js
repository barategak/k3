import React, { Component } from 'react'
import { View, Text, StyleSheet, ActivityIndicator, Image, ImageBackground } from 'react-native'

export default class header extends Component {
    render() {
        return (
            <View style={style.header}>
                <Image source={require("../asset/Logo.png")} style={style.headerImage} />
                <Text style={style.headerText}>MODUL K3</Text>
            </View>
        )
    }
}

const style = StyleSheet.create({
    header: {
        width: "100%",
        marginTop: 30,
        flexDirection: "row",
        justifyContent: "center",
        alignItems: "center"
    },
    headerImage: {
        width: 45,
        height: 45,
        resizeMode: "center"
    },
    headerText: {
        fontSize: 25,
        color: "black",
        fontFamily: "Roboto-Bold",
        marginLeft: 5,
        color: "#9b59b6"
    }
})