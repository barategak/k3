
import { StyleSheet } from 'react-native'

const styleComponent = StyleSheet.create({
    fontRegular: {
        fontFamily: "Roboto-Regular"
    },
    fontBlack: {
        fontFamily: "Roboto-Black"
    },
    fontBold: {
        fontFamily: "Roboto-Bold"
    },
    fontMedium: {
        fontMedium: "Roboto-Medium"
    },
    fontLight: {
        fontMedium: "Roboto-Light"
    }
    
})

export default styleComponent;
