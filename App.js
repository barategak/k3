import React, { Component } from 'react'
import {View, Text, Button, TouchableOpacity} from 'react-native'
import SplashPage from "./page/SplashScreen"
import rekap from './page/rekap'
import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import materiPage from "./page/Materi"
import detailMateriPage from "./page/detailMateri"
import soalMateriPage from "./page/soalMateri"
import preload from "./page/preload"
import mainMenu from "./page/mainMenu"
import materiMenu from "./page/menuMateri"
import soalMenu from "./page/menuSoal"
import hasilKuis from "./page/hasilKuis"
import about from "./page/about"
import menuSubMateri from './page/menuSubMateri'
import menuAbout from './page/menuAbout'
import menuDaftarPustaka from './page/menuDaftarPustaka'

export default class App extends Component {

  constructor(props) {
    super();
    this.state = {
        isLoading: true,
    }
  }

  componentDidMount() {
    setTimeout(() => {
      this.setState({isLoading: false})
    }, 2000)
  }
  

  render() {
    const Stack = createNativeStackNavigator();
    const Content = createNativeStackNavigator();
    return (
      
      this.state.isLoading ?
        <SplashPage />
      :
      <NavigationContainer>
          
        <Stack.Navigator initialRouteName='Preload' >

          <Stack.Screen name="Preload" component={preload} options={{
            headerShown: false
          }} />
          <Stack.Screen name="MainMenu" component={mainMenu} options={{
            headerShown: false
          }} />
          <Stack.Screen name="subMenuAbout" component={menuAbout} options={{
            headerShown: false
          }} />
          <Stack.Screen name="About" component={about} options={{
            headerShown: false
          }} />
          <Stack.Screen name="MateriMenu" component={materiMenu} options={{
            headerShown: false
          }} />
          <Stack.Screen name="MenuDafPus" component={menuDaftarPustaka} options={{
            headerShown: false
          }} />
          <Stack.Screen name="SubMateriMenu" component={menuSubMateri} options={{
            headerShown: false
          }} />
          <Stack.Screen name="SoalMenu" component={soalMenu} options={{
            headerShown: false
          }} />
          <Stack.Screen name="HasilKuis" component={hasilKuis} options={{
            headerShown: false
          }} />
          <Stack.Screen name="Materi" component={materiPage} options={{
            headerStyle: {
              backgroundColor: "#192a56"
            },
            headerTitleStyle: {
              fontFamily: "Roboto-Medium",
              color: "white"
            },
            title: "Menu Materi"
          }} />
          <Stack.Screen name="DetailMateri" component={detailMateriPage} options={{
            headerStyle: {
              backgroundColor: "white",
            },
            headerTitleStyle: {
              fontFamily: "Roboto-Medium",
              color: "#9b59b6"
            },
            headerTintColor: "#9b59b6"
            }} />
          

          <Stack.Screen name="SoalMateri" component={soalMateriPage} options={{
            headerShown: false
          }} />

          <Stack.Screen name="Rekap" component={rekap} options={{
            headerShown: false
          }} />

          
        </Stack.Navigator>
        
      </NavigationContainer>
    
    )
  }
}
