import React, { Component } from 'react'
import { View, Text, StyleSheet,ScrollView, ActivityIndicator, Image, ImageBackground, TouchableOpacity } from 'react-native'
import Header from "../component/header.js"
import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome'
import { faStickyNote, faArrowRight, faPen, faInfo, faBook } from '@fortawesome/free-solid-svg-icons'

export default class menuDaftarPustaka extends Component {
    render() {
        return (
            <ScrollView style={{backgroundColor: "white", height: "100%"}}>
                <Header></Header>

                <View style={style.titleContainerStyle}>
                    <FontAwesomeIcon icon={faBook} color={"#9b59b6"} size={30} />
                    <Text style={style.textTitle}>Menu Daftar Pustaka</Text>
                </View>

                <View style={style.container}>
                <TouchableOpacity style={style.list}
                        onPress={() => {
                            this.props.navigation.navigate("DetailMateri", {url: "https://pdfhost.io/v/tLkAv..QC_1", judul: "Daftar Pustaka Materi 1"})
                        }}
                    >
                        <FontAwesomeIcon icon={ faBook } color={'white'} size={30} />
                        <Text style={style.listText}>Daftar Pustaka Materi 1</Text>
                        <FontAwesomeIcon icon={ faArrowRight } color={'white'} size={28} style={style.listArrow} />
                    </TouchableOpacity>
                    <TouchableOpacity style={style.list}
                        onPress={() => {
                            this.props.navigation.navigate("DetailMateri", {url: "https://pdfhost.io/v/dclFh5w7P_2", judul: "Daftar Pustaka Materi 2"})
                        }}
                    >
                        <FontAwesomeIcon icon={ faBook } color={'white'} size={30} />
                        <Text style={style.listText}>Daftar Pustaka Materi 2</Text>
                        <FontAwesomeIcon icon={ faArrowRight } color={'white'} size={28} style={style.listArrow} />
                    </TouchableOpacity>
                    <TouchableOpacity style={style.list}
                        onPress={() => {
                            this.props.navigation.navigate("DetailMateri", {url: "https://pdfhost.io/v/Ghs1mGZ3C_3", judul: "Daftar Pustaka Materi 3"})
                        }}
                    >
                        <FontAwesomeIcon icon={ faBook } color={'white'} size={30} />
                        <Text style={style.listText}>Daftar Pustaka Materi 3</Text>
                        <FontAwesomeIcon icon={ faArrowRight } color={'white'} size={28} style={style.listArrow} />
                    </TouchableOpacity>
                    <TouchableOpacity style={style.list}
                        onPress={() => {
                            this.props.navigation.navigate("DetailMateri", {url: "https://pdfhost.io/v/zbKPdY7fd_4", judul: "Daftar Pustaka Materi 4"})

                        }}
                    >
                        <FontAwesomeIcon icon={ faBook } color={'white'} size={30} />
                        <Text style={style.listText}>Daftar Pustaka Materi 4</Text>
                        <FontAwesomeIcon icon={ faArrowRight } color={'white'} size={28} style={style.listArrow} />
                    </TouchableOpacity>
                    <TouchableOpacity style={style.list}
                        onPress={() => {
                            this.props.navigation.navigate("DetailMateri", {url: "https://pdfhost.io/v/Yy3aZe6pc_5", judul: "Daftar Pustaka Materi 5"})
                        }}
                    >
                        <FontAwesomeIcon icon={ faBook } color={'white'} size={30} />
                        <Text style={style.listText}>Daftar Pustaka Materi 5</Text>
                        <FontAwesomeIcon icon={ faArrowRight } color={'white'} size={28} style={style.listArrow} />
                    </TouchableOpacity>
                    <TouchableOpacity style={style.list}
                        onPress={() => {
                            this.props.navigation.navigate("DetailMateri", {url: "https://pdfhost.io/v/jalNzNYov_6", judul: "Daftar Pustaka Materi 6"})
                        }}
                    >
                        <FontAwesomeIcon icon={ faBook } color={'white'} size={30} />
                        <Text style={style.listText}>Daftar Pustaka Materi 6</Text>
                        <FontAwesomeIcon icon={ faArrowRight } color={'white'} size={28} style={style.listArrow} />
                    </TouchableOpacity>
                </View>
            </ScrollView>
        
        )
    }
}
const style = StyleSheet.create({
    container: {
        marginTop: 50
    },
    list: {
        alignItems: "center",
        flexDirection: "row",
        padding: 20,
        backgroundColor: "#9b59b6",
        borderRadius: 10,
        margin: 10,
    },
    listText: {
        fontSize: 18,
        marginLeft: 10,
        fontFamily: "Roboto-Medium",
        color: "white",
    },
    listArrow: {
        position: "absolute",
        right: 15
    },textTitle: {
        fontSize: 20,
        color: "black",
        fontFamily: "Roboto-Medium",
        marginLeft: 5,
        color: "#9b59b6",
        marginTop: 5
    },titleContainerStyle: {
        flexDirection: "row",
        marginTop: 50,
        marginBottom: 0,
        marginLeft: 10
    }
    
})