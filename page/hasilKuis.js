import React, { Component } from 'react'
import { View, Text, StyleSheet,ScrollView, ActivityIndicator, Image, ImageBackground, TouchableOpacity } from 'react-native'
import Header from "../component/header.js"
import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome'
import { faStickyNote, faArrowRight, faPen, faInfo, faNewspaper, faCheck } from '@fortawesome/free-solid-svg-icons'

export default class hasilKuis extends Component {

    constructor(props) {
        super();
        this.state = {
            nilai: 0,
            pesan: ""
        }
      }

    componentDidMount() {
        let nilai = this.props.route.params.hasil
        this.setState({
            nilai: nilai
        })

        if(nilai == 100) {
            this.setState({
                pesan: "Selamat, Tingkatkan terus prestasinya"
            })
        }else {
            this.setState({
                pesan: "Tingkatkan Terus Prestasimu!"
            })
        }
    }

    render() {
        return (
            <ScrollView style={{backgroundColor: "white", height: "100%"}}>
               <Header />

               <View style={style.containerTitle}>
                   <FontAwesomeIcon icon={faCheck} color="#9b59b6" style={{marginTop: 5}} />
                   <Text style={style.textTitle}>KUIS SELESAI</Text>
               </View>

               <View style={style.container}>
                   <Text style={style.textContent}>Kamu Mendapatkan Nilai</Text>
                   <View style={style.containerNilai}>
                       <Text style={style.textNilai}>{this.state.nilai}</Text>
                   </View>
               </View>

               <View>
                    <Text style={{fontFamily: "Roboto-Bold", color: "#9b59b6", fontSize: 20,textAlign: "center", marginTop: 20}}>{this.state.pesan.toUpperCase()}</Text>
                    <TouchableOpacity style={{width: "50%", backgroundColor: "#9b59b6", alignSelf: "center", marginTop: 20, padding: 10, borderRadius: 10}}
                        onPress={() => {
                            this.props.navigation.navigate("MainMenu")
                        }}
                    >
                        <Text style={{textAlign: "center", fontFamily: "Roboto-Bold", color: "white"}}>Kembali Ke Menu</Text>
                    </TouchableOpacity>
               </View>

            </ScrollView>
        )
    }
}

const style  = StyleSheet.create({
    containerTitle: {
        width: "100%",
        padding: 10,
        marginTop: 40,
        borderRadius: 10,
        justifyContent: "center",
        flexDirection: "row",
    },
    textTitle: {
        fontFamily: "Roboto-Medium",
        color: "#9b59b6",
        fontSize: 20,
        marginLeft: 8
    },
    container: {
        width: "100%",
        padding: 10,
        marginTop: 40,
        borderRadius: 20,
        alignItems: "center",
        backgroundColor: "#9b59b6"
    },
    textContent: {
        color: "white",
        fontFamily: "Roboto",
        fontSize: 18
    },
    containerNilai: {
        backgroundColor: "white",
        borderRadius: 300,
        width: 200,
        height: 200,
        marginTop: 20,
        marginBottom: 20,
        alignItems: "center",
        justifyContent: "center"
    },
    textNilai: {
        fontFamily: "Roboto-Bold",
        color: "#9b59b6",
        fontSize: 60
    }
})