import React, { Component } from 'react'
import { TextInput,Text, View, TouchableOpacity,Image, StyleSheet,ScrollView  } from 'react-native'
import RadioForm, {RadioButton, RadioButtonInput, RadioButtonLabel} from 'react-native-simple-radio-button';
import Header from "../component/header.js"
import axios from 'axios';

export default class soalMateri extends Component {

    constructor(props) {
        super();
        this.state = {
            idMateri: "",
            jawaban: [6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6],
            nama:"",
            nis:"",
            materi:"",
            kelas:"",
        }

        
      }
    
    componentDidMount() {
        let id = this.props.route.params.id
        let JawabanBenar = []
        if(id == 1) {
            JawabanBenar = [1,4,3,0,2,4,4,0,4,4,1,1,0,2,1,1,3,4,2,2];
            this.setState({
                jawaban: JawabanBenar
            })
            this.state.jawaban = JawabanBenar;
        }else if(id == 2) {
            JawabanBenar = [0,3,2,1, 4,0,2,3, 2,4,0,1, 2,1,0,1, 4,0,2,1];
            this.setState({
                jawaban: JawabanBenar
            })
        }else if(id == 3) {
            JawabanBenar = [0,0,3,1, 0,2,4,0, 2,4,0,3, 0,2,4,0, 2,4,1,2];
            this.setState({
                jawaban: JawabanBenar
            })
        }else if(id == 4) {
            JawabanBenar = [1,0,1,2, 3,2,4,3, 0,0,2,4, 0,4,1,3, 0,1,1,4];
            this.setState({
                jawaban: JawabanBenar
            })
        }else if(id == 5) {
            JawabanBenar = [1,2,0,1, 0,1,2,0, 1,3,1,2, 0,2,3,4, 1,0,4,0];
            this.setState({
                jawaban: JawabanBenar
            })
        }else if(id == 6) {
            JawabanBenar = [1,0,2,0,2,4,0,2,2,1,4,0,0,0,2,0,2,4,1,0];
            this.setState({
                jawaban: JawabanBenar
            })
        }

        this.setState({
            idMateri: id
        })
        // this.setState({
        //     jawaban: [6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6],
        //     nama:"",
        //     nis:"",
        //     materi:"",
        //     kelas:"",
        // })

        this.props.navigation.setOptions({ 
            title: 'Soal Materi'
         })
    }

    buttonSelesai = () => {
        if(this.state.idMateri == 1) {
            let Jawaban = this.state.jawaban;
            let JawabanBenar = [1,4,3,0,2,4,4,0,4,4,1,1,0,2,1,1,3,4,2,2];
            let dataJawabanSalah = [];

            for(let i = 0; i < JawabanBenar.length; i++) {
                if(Jawaban[i] != JawabanBenar[i]) {
                    dataJawabanSalah.push(i + 1)
                }
            }

            let rumusNilai = (JawabanBenar.length - dataJawabanSalah.length) * (100 / JawabanBenar.length)

            const params = JSON.stringify({
                "NIM": this.state.nis,
                "Nama": this.state.nama,
                "Materi": this.state.idMateri,
                "Nilai":rumusNilai,
                "Kelas":this.state.kelas,
            });

            axios.post("https://sheet.best/api/sheets/18c670c7-c128-4921-9046-9c9aad8b36d7", params, {"headers": {"content-type": "application/json",}})
            .then((response) => {
                console.log(response);
            })
            .catch((error) => {
                console.log(error);
                axios.post("https://sheetdb.io/api/v1/58413jgkz38yz", params, {"headers": {"content-type": "application/json",}})
                .then((response) => {
                    console.log(response);
                })
                .catch((error) => {
                    console.log(error);
                    axios.post("https://sheetdb.io/api/v1/urc3l3kb2znx0", params, {"headers": {"content-type": "application/json",}})
                    .then((response) => {
                        console.log(response);
                    })
                    .catch((error) => {
                        console.log(error);
                    });
                });
            });

           

            this.props.navigation.navigate("HasilKuis", {hasil: rumusNilai})
        }else if(this.state.idMateri == 2) {
            let Jawaban = this.state.jawaban;
            let JawabanBenar = [0,3,2,1, 4,0,2,3, 2,4,0,1, 2,1,0,1, 4,0,2,1];
            let dataJawabanSalah = [];

            for(let i = 0; i < JawabanBenar.length; i++) {
                if(Jawaban[i] != JawabanBenar[i]) {
                    dataJawabanSalah.push(i + 1)
                }
            }

            

            let rumusNilai = (JawabanBenar.length - dataJawabanSalah.length) * (100 / JawabanBenar.length)

            const params = JSON.stringify({
                "NIM": this.state.nis,
                "Nama": this.state.nama,
                "Materi": this.state.idMateri,
                "Nilai":rumusNilai,
                "Kelas":this.state.kelas,
            });

            axios.post("https://sheet.best/api/sheets/18c670c7-c128-4921-9046-9c9aad8b36d7", params, {"headers": {"content-type": "application/json",}})
            .then((response) => {
                console.log(response);
            })
            .catch((error) => {
                console.log(error);
                axios.post("https://sheetdb.io/api/v1/58413jgkz38yz", params, {"headers": {"content-type": "application/json",}})
                .then((response) => {
                    console.log(response);
                })
                .catch((error) => {
                    console.log(error);
                    axios.post("https://sheetdb.io/api/v1/urc3l3kb2znx0", params, {"headers": {"content-type": "application/json",}})
                    .then((response) => {
                        console.log(response);
                    })
                    .catch((error) => {
                        console.log(error);
                    });
                });
            });

            

            this.props.navigation.navigate("HasilKuis", {hasil: rumusNilai})
        }else if(this.state.idMateri == 3) {
            let Jawaban = this.state.jawaban;
            let JawabanBenar = [0,0,3,1, 0,2,4,0, 2,4,0,3, 0,2,4,0, 2,4,1,2];
            let dataJawabanSalah = [];

            for(let i = 0; i < JawabanBenar.length; i++) {
                if(Jawaban[i] != JawabanBenar[i]) {
                    dataJawabanSalah.push(i + 1)
                }
            }

            let rumusNilai = (JawabanBenar.length - dataJawabanSalah.length) * (100 / JawabanBenar.length)

            const params = JSON.stringify({
                "NIM": this.state.nis,
                "Nama": this.state.nama,
                "Materi": this.state.idMateri,
                "Nilai":rumusNilai,
                "Kelas":this.state.kelas,
            });

            axios.post("https://sheet.best/api/sheets/18c670c7-c128-4921-9046-9c9aad8b36d7", params, {"headers": {"content-type": "application/json",}})
            .then((response) => {
                console.log(response);
            })
            .catch((error) => {
                console.log(error);
                axios.post("https://sheetdb.io/api/v1/58413jgkz38yz", params, {"headers": {"content-type": "application/json",}})
                .then((response) => {
                    console.log(response);
                })
                .catch((error) => {
                    console.log(error);
                    axios.post("https://sheetdb.io/api/v1/urc3l3kb2znx0", params, {"headers": {"content-type": "application/json",}})
                    .then((response) => {
                        console.log(response);
                    })
                    .catch((error) => {
                        console.log(error);
                    });
                });
            });


            this.props.navigation.navigate("HasilKuis", {hasil: rumusNilai})
        }else if(this.state.idMateri == 4) {
            let Jawaban = this.state.jawaban;
            let JawabanBenar = [1,0,1,2, 3,2,4,3, 0,0,2,4, 0,4,1,3, 0,1,1,4];
            let dataJawabanSalah = [];

            for(let i = 0; i < JawabanBenar.length; i++) {
                if(Jawaban[i] != JawabanBenar[i]) {
                    dataJawabanSalah.push(i + 1)
                }
            }

            let rumusNilai = (JawabanBenar.length - dataJawabanSalah.length) * (100 / JawabanBenar.length)

            const params = JSON.stringify({
                "NIM": this.state.nis,
                "Nama": this.state.nama,
                "Materi": this.state.idMateri,
                "Nilai":rumusNilai,
                "Kelas":this.state.kelas,
            });

            axios.post("https://sheet.best/api/sheets/18c670c7-c128-4921-9046-9c9aad8b36d7", params, {"headers": {"content-type": "application/json",}})
            .then((response) => {
                console.log(response);
            })
            .catch((error) => {
                console.log(error);
                axios.post("https://sheetdb.io/api/v1/58413jgkz38yz", params, {"headers": {"content-type": "application/json",}})
                .then((response) => {
                    console.log(response);
                })
                .catch((error) => {
                    console.log(error);
                    axios.post("https://sheetdb.io/api/v1/urc3l3kb2znx0", params, {"headers": {"content-type": "application/json",}})
                    .then((response) => {
                        console.log(response);
                    })
                    .catch((error) => {
                        console.log(error);
                    });
                });
            });


            this.props.navigation.navigate("HasilKuis", {hasil: rumusNilai})
        }else if(this.state.idMateri == 5) {
            let Jawaban = this.state.jawaban;
            let JawabanBenar = [1,2,0,1, 0,1,2,0, 1,3,1,2, 0,2,3,4, 1,0,4,0];
            let dataJawabanSalah = [];

            for(let i = 0; i < JawabanBenar.length; i++) {
                if(Jawaban[i] != JawabanBenar[i]) {
                    dataJawabanSalah.push(i + 1)
                }
            }

            let rumusNilai = (JawabanBenar.length - dataJawabanSalah.length) * (100 / JawabanBenar.length)

            const params = JSON.stringify({
                "NIM": this.state.nis,
                "Nama": this.state.nama,
                "Materi": this.state.idMateri,
                "Nilai":rumusNilai,
                "Kelas":this.state.kelas,
            });

            axios.post("https://sheet.best/api/sheets/18c670c7-c128-4921-9046-9c9aad8b36d7", params, {"headers": {"content-type": "application/json",}})
            .then((response) => {
                console.log(response);
            })
            .catch((error) => {
                console.log(error);
                axios.post("https://sheetdb.io/api/v1/58413jgkz38yz", params, {"headers": {"content-type": "application/json",}})
                .then((response) => {
                    console.log(response);
                })
                .catch((error) => {
                    console.log(error);
                    axios.post("https://sheetdb.io/api/v1/urc3l3kb2znx0", params, {"headers": {"content-type": "application/json",}})
                    .then((response) => {
                        console.log(response);
                    })
                    .catch((error) => {
                        console.log(error);
                    });
                });
            });


            this.props.navigation.navigate("HasilKuis", {hasil: rumusNilai})
        }else if(this.state.idMateri == 6) {
            let Jawaban = this.state.jawaban;
            let JawabanBenar = [1,0,2,0,2,4,0,2,2,1,4,0,0,0,2,0,2,4,1,0];
            let dataJawabanSalah = [];

            for(let i = 0; i < JawabanBenar.length; i++) {
                if(Jawaban[i] != JawabanBenar[i]) {
                    dataJawabanSalah.push(i + 1)
                }
            }

            let rumusNilai = (JawabanBenar.length - dataJawabanSalah.length) * (100 / JawabanBenar.length)

            const params = JSON.stringify({
                "NIM": this.state.nis,
                "Nama": this.state.nama,
                "Materi": this.state.idMateri,
                "Nilai":rumusNilai,
                "Kelas":this.state.kelas,
            });

            axios.post("https://sheet.best/api/sheets/18c670c7-c128-4921-9046-9c9aad8b36d7", params, {"headers": {"content-type": "application/json",}})
            .then((response) => {
                console.log(response);
            })
            .catch((error) => {
                console.log(error);
                axios.post("https://sheetdb.io/api/v1/58413jgkz38yz", params, {"headers": {"content-type": "application/json",}})
                .then((response) => {
                    console.log(response);
                })
                .catch((error) => {
                    console.log(error);
                    axios.post("https://sheetdb.io/api/v1/urc3l3kb2znx0", params, {"headers": {"content-type": "application/json",}})
                    .then((response) => {
                        console.log(response);
                    })
                    .catch((error) => {
                        console.log(error);
                    });
                });
            });


            this.props.navigation.navigate("HasilKuis", {hasil: rumusNilai})
        }
    }
    
    render() {
        if(this.state.idMateri == 1) {
            var dataJawaban = [
                //1
                [
                    {label: 'Kepuasan dan kemampuan kerja.', value: 0 },
                    {label: 'Proses dan sistem kerja', value: 1 },
                    {label: 'Motivasi dan kepuasan kerja.', value: 2 },
                    {label: 'Proses dan disiplin kerja.', value: 3 },
                    {label: 'Motivasi dan kemampuan kerja.', value: 4 },
                ],
                //2
                [
                    {label: 'Potensi bahaya yang menimbulkan risiko dampak jangka panjang pada kesehatan.', value: 0 },
                    {label: 'Risiko terhadap kesejahteraan atau kesehatan sehari-hari.', value: 1 },
                    {label: 'Potensi bahaya yang menimbulkan risiko langsung pada keselamatan.', value: 2 },
                    {label: 'Potensi bahaya yang menimbulkan risiko pribadi dan psikologis.', value: 3 },
                    {label: 'Risiko terhadap dampak jangka pendek dan tidak langsung pada kesemalatan.', value: 4 },
                ],
                //3
                [
                    {label: 'Ergonomis', value: 0 },
                    {label: 'Biologi.', value: 1 },
                    {label: 'Kimia.', value: 2 },
                    {label: 'Psikososial.', value: 3 },
                    {label: 'Fisik.', value: 4 },
                ],
                //4
                [
                    {label: 'Ergonomis', value: 0 },
                    {label: 'Biologi.', value: 1 },
                    {label: 'Kimia.', value: 2 },
                    {label: 'Psikososial.', value: 3 },
                    {label: 'Fisik.', value: 4 },
                ],
                //5
                [
                    {label: 'Kimia, biologi, fisik, dan ergonomis.', value: 0 },
                    {label: 'Kebakaran, listrik, mekanik, dan house keeping.', value: 1 },
                    {label: 'Pelecehan, kekerasan, stress, dan narkoba.', value: 2 },
                    {label: 'Air minum, toilet, kantin, dan transportasi.', value: 3 },
                    {label: 'Kebakaran, air minum, lingkungan, dan pelecehan.', value: 4 },
                ],
                //6
                [
                    {label: 'Dilakukan perencanaan pengendalian risiko untuk mengurangi risiko sampai batas minimal.', value: 0 },
                    {label: 'Memberitahukan petugas atau atasan setelah terjadi kecelakaan kerja.', value: 1 },
                    {label: 'Dilakukan perancanaan pengendalian risiko untuk mengurangi risiko sampai menuju tak terbatas dan melampauinya.', value: 2 },
                    {label: 'Membiarkan dan berpura-pura tidak peduli dengan risiko yang ada selama tidak merugikan diri sendiri.', value: 3 },
                    {label: 'Dilakukan perencanaan pengendalian risiko untuk mengurangi risiko sampai batas maksimal.', value: 4 },
                ],
                //7
                [
                    {label: 'Long Term Gain, and Permanent Term Gain.', value: 0 },
                    {label: 'Short Term Gain, and Temporary Term Gain.', value: 1 },
                    {label: 'Middle Term Gain, and Long Term Gain.', value: 2 },
                    {label: 'Middle Term Gain, and Short Term Gain.', value: 3 },
                    {label: 'Permanent Term Gain, and Temporary Term Gain.', value: 4 },
                ],
                //8
                [
                    {label: '(2),(1),(3),(5), dan (4).', value: 0 },
                    {label: '(3),(2),(4),(5), dan (1).', value: 1 },
                    {label: '(5),(1),(2),(4), dan (3). ', value: 2 },
                    {label: '(5),(1),(2),(4), dan (3). ', value: 3 },
                    {label: '(1),(5),(4),(2), dan (3).', value: 4 },
                ],
                //9
                [
                    {label: '(2),(1),(3),(5), dan (4).', value: 0 },
                    {label: '(3),(2),(4),(5), dan (1).', value: 1 },
                    {label: '(5),(1),(2),(4), dan (3). ', value: 2 },
                    {label: '(1),(5),(4),(2), dan (3). ', value: 3 },
                    {label: '(4),(5),(3),(1), dan (2).', value: 4 },
                ],
                //10
                [
                    {label: 'Substitusi.', value: 0 },
                    {label: 'Eliminasi.', value: 1 },
                    {label: 'Perancangan. ', value: 2 },
                    {label: 'Alat pelindung diri. ', value: 3 },
                    {label: 'Administrasi.', value: 4 },
                ],
                //11
                [
                    {label: 'Mengidentifikasi, pengendalian bahaya, penilaian risiko.', value: 0 },
                    {label: 'Mengidentifikasi, penilaian risiko, pengendalian bahaya.', value: 1 },
                    {label: 'Menghilangkan, pengendalian bahaya, penilaian risiko. ', value: 2 },
                    {label: 'Menghilangkan, penilaian risiko, pengendalian bahaya.  ', value: 3 },
                    {label: 'Melaporkan, penghapusan, perawatan alat pelindung diri.', value: 4 },
                ],
                //12
                [
                    {label: 'FTA (Fault Tree Analysis).', value: 0 },
                    {label: 'FMEA (Failure Mode and Effect Analysis).', value: 1 },
                    {label: 'ETA (Event Tree Analysis). ', value: 2 },
                    {label: 'PHA (Premilinary Hazard Analysis).', value: 3 },
                    {label: 'JHA (Job Hazard Analysis).', value: 4 },
                ],
                //13
                [
                    {label: 'FTA (Fault Tree Analysis).', value: 0 },
                    {label: 'FMEA (Failure Mode and Effect Analysis).', value: 1 },
                    {label: 'ETA (Event Tree Analysis). ', value: 2 },
                    {label: 'PHA (Premilinary Hazard Analysis).', value: 3 },
                    {label: 'JHA (Job Hazard Analysis).', value: 4 },
                ],
                //14
                [
                    {label: 'Manajer risiko.', value: 0 },
                    {label: 'Karyawan.', value: 1 },
                    {label: 'Amin richman. ', value: 2 },
                    {label: 'Pemilik saham.  ', value: 3 },
                    {label: 'Ahli K3.', value: 4 },
                ],
                //15
                [
                    {label: 'Sumber daya.', value: 0 },
                    {label: 'Standar atau regulasi.', value: 1 },
                    {label: 'Pendapat ahli. ', value: 2 },
                    {label: 'Rencana keadaan darurat.', value: 3 },
                    {label: 'Data kecelakaan terdahulu.', value: 4 },
                ],
                //16
                [
                    {label: 'Risk Reduction.', value: 0 },
                    {label: 'Risk Increation.', value: 1 },
                    {label: 'Transferred Risk.', value: 2 },
                    {label: 'Avoidance Risk.', value: 3 },
                    {label: 'Acceptable Risk.', value: 4 },
                ],
                //17
                [
                    {label: 'Mengadakan Training.', value: 0 },
                    {label: 'Menggunakan prosedur kerja yang benar.', value: 1 },
                    {label: 'Pemeliharaan pralatan atau instansi.', value: 2 },
                    {label: 'Membayar lebih mahal pekerja.', value: 3 },
                    {label: 'Monitoring lingkungan kerja secara rutin.', value: 4 },
                ],
                //18
                [
                    {label: '(2),(1),(3),(5), dan (4).', value: 0 },
                    {label: '(3),(2),(4),(5), dan (1).', value: 1 },
                    {label: '(5),(1),(2),(4), dan (3).', value: 2 },
                    {label: '(1),(5),(4),(2), dan (3).', value: 3 },
                    {label: '(4),(5),(3),(1), dan (2).', value: 4 },
                ],
                //19
                [
                    {label: 'Menyebabkan kematian 1 orang atau lebih, kerusakan berat pada mesin sehingga mengganggu proses produksi. ', value: 0 },
                    {label: 'Menyebabkan cidera yang menyebabkan cacatnya angota tubuh permanen, peralatan rusak berat. ', value: 1 },
                    {label: 'Tidak ada cedera, kerugian biaya rendah, kerusakan peralatan ringan.  ', value: 2 },
                    {label: 'Menyebabkan cidera yang memerlukan perawatan medis ke rumah sakit, peralatan rusak sedang.  ', value: 3 },
                    {label: 'Cedera ringan (hanya membutuhkan P3K), peralatan rusak ringan.', value: 4 },
                ],
                //20
                [
                    {label: 'Risiko rendah. ', value: 0 },
                    {label: 'Risiko menengah. ', value: 1 },
                    {label: 'Risiko tinggi.  ', value: 2 },
                    {label: 'Tergantung dengan keputusan manajemen.  ', value: 3 },
                    {label: 'Tergantuk pihak kepolisian.', value: 4 },
                ]
            ]
            return (
                <ScrollView style={{padding: 10}}>
                    {/* Soal */}
                    <Header />
                    <View style={{backgroundColor: "#9b59b6", marginTop: 50, padding: 10, borderRadius: 10}}>
                        <Text style={{fontFamily: "Roboto-Bold", color: "white",  fontSize: 18, alignSelf: "center"}}>{"Soal Kuis dan Jawaban".toUpperCase()}</Text>
                        <Text style={{fontFamily: "Roboto", color: "white",  fontSize: 16, alignSelf: "center", textAlign: "center", marginTop: 10}}>{"Merancang Strategi Pengendalian Risiko K3 di Tempat Kerja.".toUpperCase()}</Text>
                    </View>

                   

                    

                    <View style={{marginTop: 25}}>
                        <View style={{flexDirection: "row", marginTop: 10, flexWrap: 'wrap'}}>
                            <Text style={style.textSoalNomer}>1. </Text>
                            <Text style={style.textSoal}>Risiko adalah sesuatu yang berpotensi menyebabkan terjadinya kerugian, kerusakan, cedera, sakit, kecelakaan, atau bahkan dapat menyebabkan kematian yang berhubungan dengan?</Text>
                        </View>
                        <RadioForm
                            radio_props={dataJawaban[0]}
                            initial={this.state.jawaban[0]}
                            style={{margin:10, width: '90%'}}
                            onPress={(value) => {
                                let jawaban = this.state.jawaban;
                                jawaban[0] = value;
                                this.setState({jawaban:jawaban})
                            }}
                            />
                    </View>

                    <View>
                        <View style={{flexDirection: "row", marginTop: 10, flexWrap: 'wrap'}}>
                            <Text style={style.textSoalNomer}>2. </Text>
                            <Text style={style.textSoal}>Beberapa kategori berikut yang tidak termasuk dalam kategoripotensi bahaya K3 adalah?</Text>
                        </View>
                        <RadioForm
                            radio_props={dataJawaban[1]}
                            initial={this.state.jawaban[1]}
                            style={{margin:10, width: '90%'}}
                            onPress={(value) => {
                                let jawaban = this.state.jawaban;
                                jawaban[1] = value;
                                this.setState({jawaban:jawaban})
                            }}
                            />
                    </View>

                    <View>
                        <View style={{flexDirection: "row", marginTop: 10, flexWrap: 'wrap'}}>
                            <Text style={style.textSoalNomer}>3. </Text>
                            <Text style={style.textSoal}>Berlebihnya beban kerja, komunikasi, pengendalian manajemen, lingkungan sosial tempat kerja, kekerasan dan intimidasi termasuk bahaya...</Text>
                        </View>
                        <RadioForm
                            radio_props={dataJawaban[2]}
                            initial={this.state.jawaban[2]}
                            style={{margin:10, width: '90%'}}
                            onPress={(value) => {
                                let jawaban = this.state.jawaban;
                                jawaban[2] = value;
                                this.setState({jawaban:jawaban})
                            }}
                            />
                    </View>

                    <View>
                        <View style={{flexDirection: "row", marginTop: 10, flexWrap: 'wrap'}}>
                            <Text style={style.textSoalNomer}>4. </Text>
                            <Text style={style.textSoal}>Postur/posisi kerja, pengangkutan manual, gerakan berulang serta ergonomi tempat kerja/alat/mesin termasuk bahaya...</Text>
                        </View>
                        <RadioForm
                            radio_props={dataJawaban[3]}
                            initial={this.state.jawaban[3]}
                            style={{margin:10, width: '90%'}}
                            onPress={(value) => {
                                let jawaban = this.state.jawaban;
                                jawaban[3] = value;
                                this.setState({jawaban:jawaban})
                            }}
                            />
                    </View>

                    <View>
                        <View style={{flexDirection: "row", marginTop: 10, flexWrap: 'wrap'}}>
                            <Text style={style.textSoalNomer}>5. </Text>
                            <Text style={style.textSoal}>Beberapa potensi bahaya K3 berikut yang termasuk dalam kategori D adalah?</Text>
                        </View>
                        <RadioForm
                            radio_props={dataJawaban[4]}
                            initial={this.state.jawaban[4]}
                            style={{margin:10, width: '90%'}}
                            onPress={(value) => {
                                let jawaban = this.state.jawaban;
                                jawaban[4] = value;
                                this.setState({jawaban:jawaban})
                            }}
                            />
                    </View>

                    <View>
                        <View style={{flexDirection: "row", marginTop: 10, flexWrap: 'wrap'}}>
                            <Text style={style.textSoalNomer}>6. </Text>
                            <Text style={style.textSoal}>Apa yang dilakukan setelah mengidentifikasi dan menilai potensi bahaya, yaitu?</Text>
                        </View>
                        <RadioForm
                            radio_props={dataJawaban[5]}
                            initial={this.state.jawaban[5]}
                            style={{margin:10, width: '90%'}}
                            onPress={(value) => {
                                let jawaban = this.state.jawaban;
                                jawaban[5] = value;
                                this.setState({jawaban:jawaban})
                            }}
                            />
                    </View>



                    <View>
                        <View style={{flexDirection: "row", marginTop: 10, flexWrap: 'wrap'}}>
                            <Text style={style.textSoalNomer}>7. </Text>
                            <Text style={style.textSoal}>Pengendalian risiko dapat mengikuti pendekatan Hirarki Pengendalian (Hirarchy of Control), yang merupakan Hirarki Pengendalian adalah?</Text>
                        </View>
                        <RadioForm
                            radio_props={dataJawaban[6]}
                            initial={this.state.jawaban[6]}
                            style={{margin:10, width: '90%'}}
                            onPress={(value) => {
                                let jawaban = this.state.jawaban;
                                jawaban[6] = value;
                                this.setState({jawaban:jawaban})
                            }}
                            />
                    </View>


                    <View>
                        <View style={{flexDirection: "row", marginTop: 10, flexWrap: 'wrap'}}>
                            <Text style={style.textSoalNomer}>8. </Text>
                            <Text style={style.textSoal}>Perhatikan pengendalian risiko dibawah ini.(1) substitusi; (2) eliminasi; (3) perancangan; (4) alat pelindung diri; (5) administrasi.</Text>
                        </View>
                        <View style={{ marginTop: 2, marginLeft: 10}}>
                            <Text style={style.textSoal}>Beberapa persyaratan berikut yang termasuk pada pengendalian berorientasi jangka panjang adalah? </Text>
                        </View>
                        <RadioForm
                            radio_props={dataJawaban[7]}
                            initial={this.state.jawaban[7]}
                            style={{margin:10, width: '90%'}}
                            onPress={(value) => {
                                let jawaban = this.state.jawaban;
                                jawaban[7] = value;
                                this.setState({jawaban:jawaban})
                            }}
                            />
                    </View>

                    <View>
                    <View style={{flexDirection: "row", marginTop: 10, flexWrap: 'wrap'}}>
                            <Text style={style.textSoalNomer}>9. </Text>
                            <Text style={style.textSoal}>Perhatikan pengendalian risiko dibawah ini.(1) substitusi; (2) eliminasi; (3) perancangan; (4) alat pelindung diri; (5) administrasi.</Text>
                        </View>
                        <View style={{ marginTop: 2, marginLeft: 10}}>
                            <Text style={style.textSoal}>Beberapa persyaratan berikut yang termasuk pada pengendalian berorientasi jangka pendek adalah?</Text>
                        </View>
                        <RadioForm
                            radio_props={dataJawaban[8]}
                            initial={this.state.jawaban[8]}
                            style={{margin:10, width: '90%'}}
                            onPress={(value) => {
                                let jawaban = this.state.jawaban;
                                jawaban[8] = value;
                                this.setState({jawaban:jawaban})
                            }}
                            />
                    </View>

                    <View>
                        <View style={{flexDirection: "row", marginTop: 10, flexWrap: 'wrap'}}>
                            <Text style={style.textSoalNomer}>10. </Text>
                            <Text style={style.textSoal}>Salah satu contoh hirarki pengendalian risiko dengan melakukan penyediaan ijin kerja (working permit) saat pekerja akan melakukan pemasangan travo 20KV di ketinggian 10 meter. Langkah ini dikategorikan sebagai pengendalian....</Text>
                        </View>
                        <RadioForm
                            radio_props={dataJawaban[9]}
                            initial={this.state.jawaban[9]}
                            style={{margin:10, width: '90%'}}
                            onPress={(value) => {
                                let jawaban = this.state.jawaban;
                                jawaban[9] = value;
                                this.setState({jawaban:jawaban})
                            }}
                            />
                    </View>


                    <View>
                        <View style={{flexDirection: "row", marginTop: 10, flexWrap: 'wrap'}}>
                            <Text style={style.textSoalNomer}>11. </Text>
                            <Text style={style.textSoal}>Form identifikasi bahaya biasanya digunakan untuk .... semua potensi bahaya K3 yang terdapat di dalam aktivitas-aktivitas Organisasi/Perusahaan di tempat kerja, dilanjutkan dengan melakukan ..... dari potensi bahaya tersebut serta menentukan langkah-langkah ...... dan risiko K3. </Text>
                        </View>
                        <View style={{flexDirection: "row", marginTop: 10, flexWrap: 'wrap'}}>
                            <Text style={style.textSoal}>Isilah titik-titik sesuai jawaban yang benar pada kalimat tersebut.</Text>
                        </View>
                        <RadioForm
                            radio_props={dataJawaban[10]}
                            initial={this.state.jawaban[10]}
                            style={{margin:10, width: '90%'}}
                            onPress={(value) => {
                                let jawaban = this.state.jawaban;
                                jawaban[10] = value;
                                this.setState({jawaban:jawaban})
                            }}
                            />
                    </View>

                    <View>
                        <View style={{flexDirection: "row", marginTop: 10, flexWrap: 'wrap'}}>
                            <Text style={style.textSoalNomer}>12. </Text>
                            <Text style={style.textSoal}>Beberapa metode berikut yang termasuk dalam metode identifikasi risiko dengan menganalisis berbagai pertimbangan kesalahan dari peralatan yang digunakan dan mengevaluasi dampak kesalahan tersebut, adalah?</Text>
                        </View>
                        <RadioForm
                            radio_props={dataJawaban[11]}
                            initial={this.state.jawaban[11]}
                            style={{margin:10, width: '90%'}}
                            onPress={(value) => {
                                let jawaban = this.state.jawaban;
                                jawaban[11] = value;
                                this.setState({jawaban:jawaban})
                            }}
                            />
                    </View>


                    <View>
                        <View style={{flexDirection: "row", marginTop: 10, flexWrap: 'wrap'}}>
                            <Text style={style.textSoalNomer}>13. </Text>
                            <Text style={style.textSoal}>Beberapa metode berikut yang termasuk dalam metode identifikasi risiko dengan menganalisis dampak yang mungkin terjadi dengan diawali oleh identifikasi pemicu kejadian dan proses dalam setiap tahapan yang menimbulkan terjadinya kecelakaan, adalah?</Text>
                        </View>
                        <RadioForm
                            radio_props={dataJawaban[12]}
                            initial={this.state.jawaban[12]}
                            style={{margin:10, width: '90%'}}
                            onPress={(value) => {
                                let jawaban = this.state.jawaban;
                                jawaban[12] = value;
                                this.setState({jawaban:jawaban})
                            }}
                            />
                    </View>


                    <View>
                        <View style={{flexDirection: "row", marginTop: 10, flexWrap: 'wrap'}}>
                            <Text style={style.textSoalNomer}>14. </Text>
                            <Text style={style.textSoal}>Mengetahui kemungkinan – kemungkinan terjadinya suatu kerugian dan harus berhati – hati atas kemungkinan timbulnya setiap kerugian di perusahaan adalah tugas utama seorang?</Text>
                        </View>
                        <RadioForm
                            radio_props={dataJawaban[13]}
                            initial={this.state.jawaban[13]}
                            style={{margin:10, width: '90%'}}
                            onPress={(value) => {
                                let jawaban = this.state.jawaban;
                                jawaban[13] = value;
                                this.setState({jawaban:jawaban})
                            }}
                            />
                    </View>


                    <View>
                        <View style={{flexDirection: "row", marginTop: 10, flexWrap: 'wrap'}}>
                            <Text style={style.textSoalNomer}>15. </Text>
                            <Text style={style.textSoal}>Beberapa tindakan pengendalian risiko berikut yang tidak termasuk dalam menentukan suatu risiko apakah dapat diterima atau tidak akan tergantung kepada penilaian/pertimbangan dari suatu organisasi, adalah?</Text>
                        </View>
                        <RadioForm
                            radio_props={dataJawaban[14]}
                            initial={this.state.jawaban[14]}
                            style={{margin:10, width: '90%'}}
                            onPress={(value) => {
                                let jawaban = this.state.jawaban;
                                jawaban[14] = value;
                                this.setState({jawaban:jawaban})
                            }}
                            />
                    </View>


                    <View>
                        <View style={{flexDirection: "row", marginTop: 10, flexWrap: 'wrap'}}>
                            <Text style={style.textSoalNomer}>16. </Text>
                            <Text style={style.textSoal}>Beberapa pilihan pengendalian berikut yang tidak termasuk dalam identifikasi pilihan pengendalian risiko adalah?</Text>
                        </View>
                        <RadioForm
                            radio_props={dataJawaban[15]}
                            initial={this.state.jawaban[15]}
                            style={{margin:10, width: '90%'}}
                            onPress={(value) => {
                                let jawaban = this.state.jawaban;
                                jawaban[15] = value;
                                this.setState({jawaban:jawaban})
                            }}
                            />
                    </View>


                    <View>
                        <View style={{flexDirection: "row", marginTop: 10, flexWrap: 'wrap'}}>
                            <Text style={style.textSoalNomer}>17. </Text>
                            <Text style={style.textSoal}>Beberapa pilihan berikut yang tidak termasuk dalam pilihan untuk menurunkan probabilitas risiko adalah?</Text>
                        </View>
                        <RadioForm
                            radio_props={dataJawaban[16]}
                            initial={this.state.jawaban[16]}
                            style={{margin:10, width: '90%'}}
                            onPress={(value) => {
                                let jawaban = this.state.jawaban;
                                jawaban[16] = value;
                                this.setState({jawaban:jawaban})
                            }}
                            />
                    </View>


                    <View>
                        <View style={{flexDirection: "row", marginTop: 10, flexWrap: 'wrap'}}>
                            <Text style={style.textSoalNomer}>18. </Text>
                            <Text style={style.textSoal}>Perhatikan pengendalian risiko dibawah ini.</Text>
                        </View>
                        <View style={{ marginTop: 2, marginLeft: 10}}>
                            <Text style={style.textSoal}>(1) Menentukan pengendalian risiko;  </Text>
                            <Text style={style.textSoal}>(2) Melaporkan hasil identifikasi;</Text>
                            <Text style={style.textSoal}>(3) Melaksanakan penilaian risiko; </Text>
                            <Text style={style.textSoal}>(4) Melaksanakan observasi lapangan; </Text>
                            <Text style={style.textSoal}>(5) melaksanakan identifikasi bahaya;</Text>
                            <Text style={style.textSoal}>Beberapa persyaratan berikut yang termasuk pada prosedur identifikasi bahaya, penilaian risiko dan pengendalian risiko K3, adalah?pengendalian berorientasi jangka pendek adalah?</Text>
                        </View>
                        <RadioForm
                            radio_props={dataJawaban[17]}
                            initial={this.state.jawaban[17]}
                            style={{margin:10, width: '90%'}}
                            onPress={(value) => {
                                let jawaban = this.state.jawaban;
                                jawaban[17] = value;
                                this.setState({jawaban:jawaban})
                            }}
                            />
                    </View>


                    <View>
                        <View style={{flexDirection: "row", marginTop: 10, flexWrap: 'wrap'}}>
                            <Text style={style.textSoalNomer}>19. </Text>
                            <Text style={style.textSoal}>Pada matriks risiko HIRADC, maksud dari Risiko (R) level 1 adalah....</Text>
                        </View>
                        <RadioForm
                            radio_props={dataJawaban[18]}
                            initial={this.state.jawaban[18]}
                            style={{margin:10, width: '90%'}}
                            onPress={(value) => {
                                let jawaban = this.state.jawaban;
                                jawaban[18] = value;
                                this.setState({jawaban:jawaban})
                            }}
                            />
                    </View>


                    <View>
                        <View style={{flexDirection: "row", marginTop: 10, flexWrap: 'wrap'}}>
                            <Text style={style.textSoalNomer}>20. </Text>
                            <Text style={style.textSoal}>Berikut adalah salah satu potensi bahaya yang harus anda nilai tingkat risikonya, “Bila kegiatan kerja sering dilakukan (minimal hampir setiap 2 hari), dan apabila terjadi kecelakaan akibat proses kerja tersebut dapat merusak peralatan mencapai biaya Rp. 200 juta.”</Text>
                        </View>
                        <View style={{flexDirection: "row", marginTop: 10, flexWrap: 'wrap'}}>
                            <Text style={style.textSoal}>Menurut anda bagaimana tingkat risiko pekerjaan tersebut (bila mengacu pada matriks HIRADC) adalah?</Text>
                        </View>
                        <RadioForm
                            radio_props={dataJawaban[19]}
                            initial={this.state.jawaban[19]}
                            style={{margin:10, width: '90%'}}
                            onPress={(value) => {
                                let jawaban = this.state.jawaban;
                                jawaban[19] = value;
                                this.setState({jawaban:jawaban})
                            }}
                            />
                    </View>
                    

                    
                </ScrollView>
            )
        }else if(this.state.idMateri == 2) {
            var dataJawaban = [
                //1
                [
                    {label: 'Seluruh Pekerja.', value: 0 },
                    {label: 'Manajemen Perusahaan.', value: 1 },
                    {label: 'Ahli K3.', value: 2 },
                    {label: 'BNPB.', value: 3 },
                    {label: 'Pemerintah.', value: 4 },
                ],
                //2
                [
                    {label: 'Masyarakat, pemerintah, dan pekerja.', value: 0 },
                    {label: 'Kemanusiaan, komersil, dan kesejahteraan sosial. ', value: 1 },
                    {label: 'Kesejahteraan sosial, komersil, dan pencegahan kerugian.', value: 2 },
                    {label: 'Kemanusiaan, pencegahan kerugian, dan komersil.', value: 3 },
                    {label: 'Kesejahteraan sosial, kemanusiaan, dan pencegahan kerugian.', value: 4 },
                ],
                //3
                [
                    {label: 'Kemanusiaan.', value: 0 },
                    {label: 'Pencegahan kerugian. ', value: 1 },
                    {label: 'Komersil.', value: 2 },
                    {label: 'Kesejahteraan sosial.', value: 3 },
                    {label: 'Masyarakat pekerja.', value: 4 },
                ],
                //4
                [
                    {label: 'Kemanusiaan.', value: 0 },
                    {label: 'Pencegahan kerugian. ', value: 1 },
                    {label: 'Komersil.', value: 2 },
                    {label: 'Kesejahteraan sosial.', value: 3 },
                    {label: 'Masyarakat pekerja.', value: 4 },
                ],
                //5
                [
                    {label: 'Banjir.', value: 0 },
                    {label: 'Gempa. ', value: 1 },
                    {label: 'Angin topan', value: 2 },
                    {label: 'Petir', value: 3 },
                    {label: 'Perselisihan', value: 4 },
                ],
                //6
                [
                    {label: 'Sebagai salah satu langkah pengendalian terpadu dalam rangka mengendalikan dan menanggulangi keadaan darurat yang timbul di tempat kerja.', value: 0 },
                    {label: 'Sebagai salah satu langkah dalam memberikan pedoman dan pengarahan terhadap usaha penanggulangan bencana yang mencakup pencegahan bencana, penanganan keadaan darurat bencana, rehabilitasi, dan rekonstruksi secara adil dan setara. ', value: 1 },
                    {label: 'Menetapkan standardisasi dan kebutuhan penyelenggaraan penanggulangan bencana berdasarkan peraturan perundang-undangan.', value: 2 },
                    {label: 'Menyampaikan informasi kegiatan penanggulangan bencana kepada masyarakat.', value: 3 },
                    {label: 'Melaporkan penyelenggaraan penanggulangan bencana kepada Presiden setiap sebulan sekali dalam kondisi normal dan setiap saat dalam kondisi darurat bencana.', value: 4 },
                ],
                //7
                [
                    {label: 'Team pemadaman kebakaran.', value: 0 },
                    {label: 'Team evakuasi. ', value: 1 },
                    {label: 'Team produksi.', value: 2 },
                    {label: 'Team lingkungan.', value: 3 },
                    {label: 'Team keamanan.', value: 4 },
                ],
                //8
                [
                    {label: '(1), (2), dan (3).', value: 0 },
                    {label: '(2), (3), dan (4). ', value: 1 },
                    {label: '(3). (4), dan (5).', value: 2 },
                    {label: '(1), (2), dan (4).', value: 3 },
                    {label: '(1), (3), dan (5).', value: 4 },
                ],
                //9
                [
                    {label: 'Pendidikan dan latihan.', value: 0 },
                    {label: 'Penanggulangan keadaan darurat. ', value: 1 },
                    {label: 'Rencana dalam menghadapi keadaan darurat.', value: 2 },
                    {label: 'Pemindahan dan penutup.', value: 3 },
                    {label: 'Evaluasi dalam menghadapi keadaan darurat.', value: 4 },
                ],
                //10
                [
                    {label: 'Peringatan dari luar.', value: 0 },
                    {label: 'Alarm kebakaran. ', value: 1 },
                    {label: 'System peralatan deteksi.', value: 2 },
                    {label: 'Teriakan para pekerja.', value: 3 },
                    {label: 'Malfunction indicator light.', value: 4 },
                ],
                //11
                [
                    {label: 'Memahami kegunaan dari prosedur tanggap darurat dan rencana dalam menghadapi keadaan darurat.', value: 0 },
                    {label: 'Menggunakan dan mempertanggungjawabkan sumbangan/bantuan nasional dan internasional. ', value: 1 },
                    {label: 'Mempertanggungjawabkan penggunaan anggaran yang diterima dari Anggaran Pendapatan dan Belanja Negara.', value: 2 },
                    {label: 'Melaksanakan kewajiban lain sesuai dengan peraturan perundang-undangan.', value: 3 },
                    {label: 'Menetapkan standardisasi dan kebutuhan penyelenggaraan penanggulangan bencana berdasarkan peraturan perundang-undangan.', value: 4 },
                ],
                //12
                [
                    {label: 'Banyak berdo’a kepada tuhan.', value: 0 },
                    {label: 'Merencanakan tanggap darurat. ', value: 1 },
                    {label: 'Perbanyak olahraga.', value: 2 },
                    {label: 'Tidak peduli, karena merasa aman.', value: 3 },
                    {label: 'Menjadi orang yang pesimis. ', value: 4 },
                ],
                //13
                [
                    {label: 'Pelatihan administrasi darurat.', value: 0 },
                    {label: 'Pelatihan baris-berbaris. ', value: 1 },
                    {label: 'Pelatihan simulasi darurat.', value: 2 },
                    {label: 'Pelatihan simulasi terstruktur.', value: 3 },
                    {label: 'Pelatihan dasar kepemimpinan. ', value: 4 },
                ],
                //14
                [
                    {label: 'Keadaan darurat tingkat I (Tier I).', value: 0 },
                    {label: 'Keadaan darurat tingkat II (Tier II). ', value: 1 },
                    {label: 'Keadaan darurat tingkat III (Tier III).', value: 2 },
                    {label: 'Keadaan darurat tingkat IV (Tier IV).', value: 3 },
                    {label: 'Keadaan darurat tingkat V (Tier V). ', value: 4 },
                ],
                //15
                [
                    {label: 'Keadaan darurat tingkat I (Tier I).', value: 0 },
                    {label: 'Keadaan darurat tingkat II (Tier II). ', value: 1 },
                    {label: 'Keadaan darurat tingkat III (Tier III).', value: 2 },
                    {label: 'Keadaan darurat tingkat IV (Tier IV).', value: 3 },
                    {label: 'Keadaan darurat tingkat V (Tier V). ', value: 4 },
                ],
                //16
                [
                    {label: 'Pencegahan atau mitigasi.', value: 0 },
                    {label: 'Lapor polisi, RT dan RW. ', value: 1 },
                    {label: 'Kesiap-siagaan pada tahap sebelum darurat.', value: 2 },
                    {label: 'Tanggap darurat.', value: 3 },
                    {label: 'Rehabilitasi dan rekonstruksi pada tahap setelah bencana. ', value: 4 },
                ],
                //17
                [
                    {label: 'Memahami filosofi K3.', value: 0 },
                    {label: 'Mengenal kegiatan unit kerja. ', value: 1 },
                    {label: 'Memahami peralatan/sarana darurat secara operasional.', value: 2 },
                    {label: 'Memahami tata laksana kerja organisasi.', value: 3 },
                    {label: 'Masyarakat awam. ', value: 4 },
                ],
                //18
                [
                    {label: 'Minimum 1 (satu) rute emergency exit untuk menjadi jalan ke tempat evakuasi personel.', value: 0 },
                    {label: 'Rute emergency exit berada di lokasi yg permanen & sepanjang rute tidak terdapat bahan-bahan/peralatan yg mudah terbakar.', value: 1 },
                    {label: 'Rute emergency exit menuju daerah yg lebih aman.', value: 2 },
                    {label: 'Rute emergency exit harus menyediakan tanda yg dapat menyala sepanjang rute sebagai panduan bagi personel bila keadaan gelap.', value: 3 },
                    {label: 'Setiap personel yg terlibat harus memahami lokasi & rute emergency exit.', value: 4 },
                ],
                //19
                [
                    {label: 'Fasilitas ibadah.', value: 0 },
                    {label: 'Lampu & tenaga listrik darurat.', value: 1 },
                    {label: 'Peralatan pemadam kebakaran.', value: 2 },
                    {label: 'Fasilitas komunikasi.', value: 3 },
                    {label: 'Tempat perlindungan. ', value: 4 },
                ],
                //20
                [
                    {label: '(1) (2),(3),(4),(5),(6),(7),(8),(9),dan (10).', value: 0 },
                    {label: '(1) (3),(2),(5),(4),(6),(7),(9),(8),dan (10). ', value: 1 },
                    {label: '(2) (1),(3),(5),(4),(6),(9),(8),(7),dan (10).', value: 2 },
                    {label: '(2) (3),(1),(7),(5),(6),(4),(9),(8),dan (10).', value: 3 },
                    {label: '(10) (9),(8),(7),(6),(5),(4),(3),(2),dan (1). ', value: 4 },
                ]
            ]
            return (
                <ScrollView style={{padding: 10}}>
                    <Header />
                    <View style={{backgroundColor: "#9b59b6", marginTop: 50, padding: 10, borderRadius: 10}}>
                        <Text style={{fontFamily: "Roboto-Bold", color: "white",  fontSize: 18, alignSelf: "center"}}>{"Soal Kuis dan Jawaban".toUpperCase()}</Text>
                        <Text style={{fontFamily: "Roboto", color: "white",  fontSize: 16, alignSelf: "center", textAlign: "center", marginTop: 10}}>{"Merancang Sistem Tanggap Darurat.".toUpperCase()}</Text>
                    </View>
                    {/* Soal */}

                    <View>
                        <View style={{flexDirection: "row", marginTop: 10, flexWrap: 'wrap'}}>
                            <Text style={style.textSoalNomer}>1. </Text>
                            <Text style={style.textSoal}>Persiapan keadaan darurat merupakan tanggung jawab oleh?</Text>
                        </View>
                        <RadioForm
                            radio_props={dataJawaban[0]}
                            initial={this.state.jawaban[0]}
                            style={{margin:10, width: '90%'}}
                            onPress={(value) => {
                                let jawaban = this.state.jawaban;
                                jawaban[0] = value;
                                this.setState({jawaban:jawaban})
                            }}
                            />
                    </View>

                    <View>
                        <View style={{flexDirection: "row", marginTop: 10, flexWrap: 'wrap'}}>
                            <Text style={style.textSoalNomer}>2. </Text>
                            <Text style={style.textSoal}>Secara garis besar, maksud dan tujuan sistem tanggap darurat meliputi tiga aspek, adalah?</Text>
                        </View>
                        <RadioForm
                            radio_props={dataJawaban[1]}
                            initial={this.state.jawaban[1]}
                            style={{margin:10, width: '90%'}}
                            onPress={(value) => {
                                let jawaban = this.state.jawaban;
                                jawaban[1] = value;
                                this.setState({jawaban:jawaban})
                            }}
                            />
                    </View>

                    <View>
                        <View style={{flexDirection: "row", marginTop: 10, flexWrap: 'wrap'}}>
                            <Text style={style.textSoalNomer}>3. </Text>
                            <Text style={style.textSoal}>Kelangsungan operasional perusahaan agar kegiatan bisnis dan produksi tidak terhenti, serta memberikan informasi kepada seluruh penghuni gedung tentang bahaya industri dan cara-cara penanggulangannya. Termasuk sistem tanggap darurat dalam aspek, yaitu?</Text>
                        </View>
                        <RadioForm
                            radio_props={dataJawaban[2]}
                            initial={this.state.jawaban[2]}
                            style={{margin:10, width: '90%'}}
                            onPress={(value) => {
                                let jawaban = this.state.jawaban;
                                jawaban[2] = value;
                                this.setState({jawaban:jawaban})
                            }}
                            />
                    </View>

                    <View>
                        <View style={{flexDirection: "row", marginTop: 10, flexWrap: 'wrap'}}>
                            <Text style={style.textSoalNomer}>4. </Text>
                            <Text style={style.textSoal}>Meminimalisir kerugian terhadap aset-aset perusahaan dan lingkungan sekitar, mencegah menjalarnya keadaan darurat, dan meminimalisir bahaya yang timbul akibat keadaan darurat. Termasuk sistem tanggap darurat dalam aspek, yaitu?</Text>
                        </View>
                        <RadioForm
                            radio_props={dataJawaban[3]}
                            initial={this.state.jawaban[3]}
                            style={{margin:10, width: '90%'}}
                            onPress={(value) => {
                                let jawaban = this.state.jawaban;
                                jawaban[3] = value;
                                this.setState({jawaban:jawaban})
                            }}
                            />
                    </View>

                    <View>
                        <View style={{flexDirection: "row", marginTop: 10, flexWrap: 'wrap'}}>
                            <Text style={style.textSoalNomer}>5. </Text>
                            <Text style={style.textSoal}>Beberapa contoh berikut yang bukan termasuk dalam contoh natural hazard adalah?</Text>
                        </View>
                        <RadioForm
                            radio_props={dataJawaban[4]}
                            initial={this.state.jawaban[4]}
                            style={{margin:10, width: '90%'}}
                            onPress={(value) => {
                                let jawaban = this.state.jawaban;
                                jawaban[4] = value;
                                this.setState({jawaban:jawaban})
                            }}
                            />
                    </View>

                    <View>
                        <View style={{flexDirection: "row", marginTop: 10, flexWrap: 'wrap'}}>
                            <Text style={style.textSoalNomer}>6. </Text>
                            <Text style={style.textSoal}>Tujuan dibentuknya team tanggap darurat dalam perusahaan, yaitu?</Text>
                        </View>
                        <RadioForm
                            radio_props={dataJawaban[5]}
                            initial={this.state.jawaban[5]}
                            style={{margin:10, width: '90%'}}
                            onPress={(value) => {
                                let jawaban = this.state.jawaban;
                                jawaban[5] = value;
                                this.setState({jawaban:jawaban})
                            }}
                            />
                    </View>



                    <View>
                        <View style={{flexDirection: "row", marginTop: 10, flexWrap: 'wrap'}}>
                            <Text style={style.textSoalNomer}>7. </Text>
                            <Text style={style.textSoal}>Beberapa contoh berikut yang tidak termasuk dalam contoh team tanggap darurat dalam penanggulangan dan pengendalian adalah?</Text>
                        </View>
                        <RadioForm
                            radio_props={dataJawaban[6]}
                            initial={this.state.jawaban[6]}
                            style={{margin:10, width: '90%'}}
                            onPress={(value) => {
                                let jawaban = this.state.jawaban;
                                jawaban[6] = value;
                                this.setState({jawaban:jawaban})
                            }}
                            />
                    </View>


                    <View>
                        <View style={{flexDirection: "row", marginTop: 10, flexWrap: 'wrap'}}>
                            <Text style={style.textSoalNomer}>8. </Text>
                            <Text style={style.textSoal}>Perhatikan kalimat berikut.</Text>
                        </View>
                        <View style={{ marginTop: 2, marginLeft: 10}}>
                            <Text style={style.textSoal}>(1) Melakukan koordinasi dengan anggota team untuk menanggulangi dan menangani keadaan darurat.. </Text>
                            <Text style={style.textSoal}>(2) Memberikan pertolongan dan evakuasi korban.</Text>
                            <Text style={style.textSoal}>(3) Menetapkan standardisasi dan kebutuhan penyelenggaraan penanggulangan bencana berdasarkan peraturan perundang-undangan.</Text>
                            <Text style={style.textSoal}>(4) Melakukan komunikasi efektif dengan pihak berwajib, serta melakukan pemulihan (rehabilitasi) lingkungan.</Text>
                            <Text style={style.textSoal}>(5) Melaporkan penyelenggaraan penanggulangan bencana kepada Presiden setiap sebulan sekali dalam kondisi normal dan setiap saat dalam kondisi darurat bencana.</Text>
                            <Text style={style.textSoal}>Beberapa persyaratan berikut yang termasuk dalam peran dan tanggung jawab dari sebuah team tanggap darurat dalam penanganan situasi darurat adalah?</Text>
                        </View>
                        <RadioForm
                            radio_props={dataJawaban[7]}
                            initial={this.state.jawaban[7]}
                            style={{margin:10, width: '90%'}}
                            onPress={(value) => {
                                let jawaban = this.state.jawaban;
                                jawaban[7] = value;
                                this.setState({jawaban:jawaban})
                            }}
                            />
                    </View>

                    <View>
                        <View style={{flexDirection: "row", marginTop: 10, flexWrap: 'wrap'}}>
                            <Text style={style.textSoalNomer}>9. </Text>
                            <Text style={style.textSoal}>Mempersiapkan koordinasi dan petunjuk bagi rencana kegiatan organisasi/perusahaan, kesiagaan untuk bertindak dan mendeteksi kejanggalan pada kegiatan organisasi (pada proses pelayanan) dan/atau gejala alam, dimana diduga kemungkinan akan adanya kecelakaan baik perseorangan, gangguan di wilayah kerja atau kekacauan lingkungan. Merupakan pengertian dari pengertian?</Text>
                        </View>
                        <RadioForm
                            radio_props={dataJawaban[8]}
                            initial={this.state.jawaban[8]}
                            style={{margin:10, width: '90%'}}
                            onPress={(value) => {
                                let jawaban = this.state.jawaban;
                                jawaban[8] = value;
                                this.setState({jawaban:jawaban})
                            }}
                            />
                    </View>

                    <View>
                        <View style={{flexDirection: "row", marginTop: 10, flexWrap: 'wrap'}}>
                            <Text style={style.textSoalNomer}>10. </Text>
                            <Text style={style.textSoal}>Tanda dan peringatan yang tidak boleh digunakan dalam menghadapi keadaan darurat, adalah?</Text>
                        </View>
                        <RadioForm
                            radio_props={dataJawaban[9]}
                            initial={this.state.jawaban[9]}
                            style={{margin:10, width: '90%'}}
                            onPress={(value) => {
                                let jawaban = this.state.jawaban;
                                jawaban[9] = value;
                                this.setState({jawaban:jawaban})
                            }}
                            />
                    </View>


                    <View>
                        <View style={{flexDirection: "row", marginTop: 10, flexWrap: 'wrap'}}>
                            <Text style={style.textSoalNomer}>11. </Text>
                            <Text style={style.textSoal}>Persyaratan utama yang harus dimengerti oleh pekerja dalam sistem tanggap darurat, adalah?</Text>
                        </View>
                        <RadioForm
                            radio_props={dataJawaban[10]}
                            initial={this.state.jawaban[10]}
                            style={{margin:10, width: '90%'}}
                            onPress={(value) => {
                                let jawaban = this.state.jawaban;
                                jawaban[10] = value;
                                this.setState({jawaban:jawaban})
                            }}
                            />
                    </View>

                    <View>
                        <View style={{flexDirection: "row", marginTop: 10, flexWrap: 'wrap'}}>
                            <Text style={style.textSoalNomer}>12. </Text>
                            <Text style={style.textSoal}>Mengantisipasi bencana yang kapan waktunya tidak dapat diketahui datangnya, maka yang seharusnya dilakukan sebagai bagian dari K3, yaitu?</Text>
                        </View>
                        <RadioForm
                            radio_props={dataJawaban[11]}
                            initial={this.state.jawaban[11]}
                            style={{margin:10, width: '90%'}}
                            onPress={(value) => {
                                let jawaban = this.state.jawaban;
                                jawaban[11] = value;
                                this.setState({jawaban:jawaban})
                            }}
                            />
                    </View>


                    <View>
                        <View style={{flexDirection: "row", marginTop: 10, flexWrap: 'wrap'}}>
                            <Text style={style.textSoalNomer}>13. </Text>
                            <Text style={style.textSoal}>Agar semua insan pelaku dalam organisasi tanggap darurat menjadi familiar dengan tugas dan tanggung jawab, serta semua sistem/sarana/peralatan darurat selalu siap pakai jika dibutuhkan. Merupakan pengertian dari penjelasan?</Text>
                        </View>
                        <RadioForm
                            radio_props={dataJawaban[12]}
                            initial={this.state.jawaban[12]}
                            style={{margin:10, width: '90%'}}
                            onPress={(value) => {
                                let jawaban = this.state.jawaban;
                                jawaban[12] = value;
                                this.setState({jawaban:jawaban})
                            }}
                            />
                    </View>


                    <View>
                        <View style={{flexDirection: "row", marginTop: 10, flexWrap: 'wrap'}}>
                            <Text style={style.textSoalNomer}>14. </Text>
                            <Text style={style.textSoal}>Keadaan darurat yang merupakan suatu kecelakaan besar dimana semua karyawan yang bertugas dibantu dengan peralatan dan material yang tersedia di instalasi/pabrik tersebut, tidak lagi mampu mengendalikan keadaaan darurat tersebut, sehingga mengakibatkan terjadinya beberapa korban manusia.Penjelasan diatas termasuk keadaan darurat tingkat?</Text>
                        </View>
                        <RadioForm
                            radio_props={dataJawaban[13]}
                            initial={this.state.jawaban[13]}
                            style={{margin:10, width: '90%'}}
                            onPress={(value) => {
                                let jawaban = this.state.jawaban;
                                jawaban[13] = value;
                                this.setState({jawaban:jawaban})
                            }}
                            />
                    </View>


                    <View>
                        <View style={{flexDirection: "row", marginTop: 10, flexWrap: 'wrap'}}>
                            <Text style={style.textSoalNomer}>15. </Text>
                            <Text style={style.textSoal}>    15. Keadaan darurat yang berpotensi mengancam nyawa manusia dan harta benda (asset), yan secara normal dapat diatasi oleh personil jaga dan suatu instalasi/pabrik dengan menggunakan prosedur yang telah dipersiapkan, tanpa perlu adanya regu bantuan yang dikonsinyir. Penjelasan diatas termasuk keadaan darurat tingkat?</Text>
                        </View>
                        <RadioForm
                            radio_props={dataJawaban[14]}
                            initial={this.state.jawaban[14]}
                            style={{margin:10, width: '90%'}}
                            onPress={(value) => {
                                let jawaban = this.state.jawaban;
                                jawaban[14] = value;
                                this.setState({jawaban:jawaban})
                            }}
                            />
                    </View>


                    <View>
                        <View style={{flexDirection: "row", marginTop: 10, flexWrap: 'wrap'}}>
                            <Text style={style.textSoalNomer}>16. </Text>
                            <Text style={style.textSoal}>Manajemen darurat merupakan kegiatan yang berkesinambungan meliputi 4 tahap kegiatan, kecuali?</Text>
                        </View>
                        <RadioForm
                            radio_props={dataJawaban[15]}
                            initial={this.state.jawaban[15]}
                            style={{margin:10, width: '90%'}}
                            onPress={(value) => {
                                let jawaban = this.state.jawaban;
                                jawaban[15] = value;
                                this.setState({jawaban:jawaban})
                            }}
                            />
                    </View>


                    <View>
                        <View style={{flexDirection: "row", marginTop: 10, flexWrap: 'wrap'}}>
                            <Text style={style.textSoalNomer}>17. </Text>
                            <Text style={style.textSoal}>Beberapa kriteria berikut yang tidak termasuk dalam kriteria tim penyusun rencana tanggap darurat adalah?</Text>
                        </View>
                        <RadioForm
                            radio_props={dataJawaban[16]}
                            initial={this.state.jawaban[16]}
                            style={{margin:10, width: '90%'}}
                            onPress={(value) => {
                                let jawaban = this.state.jawaban;
                                jawaban[16] = value;
                                this.setState({jawaban:jawaban})
                            }}
                            />
                    </View>


                    <View>
                        <View style={{flexDirection: "row", marginTop: 10, flexWrap: 'wrap'}}>
                            <Text style={style.textSoalNomer}>18. </Text>
                            <Text style={style.textSoal}>Beberapa ketentuan berikut yang tidak termasuk dalam ketentuan emergency exit yang benar adalah?</Text>
                        </View>
                        <RadioForm
                            radio_props={dataJawaban[17]}
                            initial={this.state.jawaban[17]}
                            style={{margin:10, width: '90%'}}
                            onPress={(value) => {
                                let jawaban = this.state.jawaban;
                                jawaban[17] = value;
                                this.setState({jawaban:jawaban})
                            }}
                            />
                    </View>


                    <View>
                        <View style={{flexDirection: "row", marginTop: 10, flexWrap: 'wrap'}}>
                            <Text style={style.textSoalNomer}>19. </Text>
                            <Text style={style.textSoal}>Beberapa hal berikut yang tidak termasuk dalam peralatan darurat adalah?</Text>
                        </View>
                        <RadioForm
                            radio_props={dataJawaban[18]}
                            initial={this.state.jawaban[18]}
                            style={{margin:10, width: '90%'}}
                            onPress={(value) => {
                                let jawaban = this.state.jawaban;
                                jawaban[18] = value;
                                this.setState({jawaban:jawaban})
                            }}
                            />
                    </View>


                    <View>
                        <View style={{flexDirection: "row", marginTop: 10, flexWrap: 'wrap'}}>
                            <Text style={style.textSoalNomer}>20. </Text>
                            <Text style={style.textSoal}>Perhatikan penjelasan ini.</Text>
                        </View>
                        <View style={{ marginTop: 2, marginLeft: 10}}>
                            <Text style={style.textSoal}>(1) Bentuk Tim Penyusun Rencana Tanggap Darurat </Text>
                            <Text style={style.textSoal}>(2) Identifikasi & Penilaian Risiko Kebakaran</Text>
                            <Text style={style.textSoal}>(3) Membuat/Menentukan Tujuan Dan Ruang Lingkup</Text>
                            <Text style={style.textSoal}>(4) Susun rencana untuk pelatihan simulasi atau emergency drill.</Text>
                            <Text style={style.textSoal}>(5) Menyusun Kesiapsiagaan Tanggap Darurat.</Text>
                            <Text style={style.textSoal}>(6) Evaluasi & Pemuthakhiran Prosedur.</Text>
                            <Text style={style.textSoal}>(7) Susun Organisasi Tanggap Darurat.</Text>
                            <Text style={style.textSoal}>(8) Simulasi Tanggap Darurat.</Text>
                            <Text style={style.textSoal}>(9) Susun Prosedur Tanggap Darurat.</Text>
                            <Text style={style.textSoal}>(10) Evaluasi dan Pemutakhiran.</Text>
                            <Text style={style.textSoal}>Beberapa persyaratan berikut yang termasuk alur Penyusunan Prosedur Tanggap Darurat Kebakaran & Implementasinya secara berurutan kerja adalah?</Text>
                        </View>
                        <RadioForm
                            radio_props={dataJawaban[19]}
                            initial={this.state.jawaban[19]}
                            style={{margin:10, width: '90%'}}
                            onPress={(value) => {
                                let jawaban = this.state.jawaban;
                                jawaban[19] = value;
                                this.setState({jawaban:jawaban})
                            }}
                            />
                    </View>
                    

                    
                </ScrollView>
            )
        }else if(this.state.idMateri == 3) {
            var dataJawaban = [
                //1
                [
                    {label: 'Pembaca dengan pemirsanya.', value: 0 },
                    {label: 'Pihak yang menyampaikan dengan pihak yang menerima.', value: 1 },
                    {label: 'Pembicara dengan pendengarnya.', value: 2 },
                    {label: 'Penulis dengan pembacanya.', value: 3 },
                    {label: 'Pelakon pentas dengan penontonnya.', value: 4 },
                ],
                //2
                [
                    {label: 'Merupakan alat dasar dalam praktek pencegahan kecelakaan.', value: 0 },
                    {label: 'Memberikan nasihat dan mengingatkan cara kerja selamat guna mencegah terjadinya kecelakaan. ', value: 1 },
                    {label: 'Mencegah kecelakaan disebabkan oleh masalah komuniksi.', value: 2 },
                    {label: 'Mencegah penyampaiannya tidak efektif karena beranggapan merupakan prosedur rutin.', value: 3 },
                    {label: 'Merupakan cara untuk memberikan nasehat dan komunikasi.', value: 4 },
                ],
                //3
                [
                    {label: 'Pengirim pesan.', value: 0 },
                    {label: 'Pesan. ', value: 1 },
                    {label: 'Penerima pesan.', value: 2 },
                    {label: 'Komunikasi.', value: 3 },
                    {label: 'Encoding.', value: 4 },
                ],
                //4
                [
                    {label: 'Pertukaran fakta, opini, dan emosi antar dua orang atau lebih. ', value: 0 },
                    {label: 'Si penyebar pesan, pesan itu sendiri, dan si penerima pesan. ', value: 1 },
                    {label: 'Pertukaran pikiran, keterangan dan mencari solusi.', value: 2 },
                    {label: 'Media tulisan berupa surat, warkat, pos.', value: 3 },
                    {label: 'Media berupa handphone, telegram, dan lonceng.', value: 4 },
                ],
                //5
                [
                    {label: 'Passive communication.', value: 0 },
                    {label: 'Assertive communication. ', value: 1 },
                    {label: 'Aggressive communication.', value: 2 },
                    {label: 'Effective communication.', value: 3 },
                    {label: 'Attractive communication.', value: 4 },
                ],
                //6
                [
                    {label: 'Passive communication.', value: 0 },
                    {label: 'Assertive communication. ', value: 1 },
                    {label: 'Aggressive communication.', value: 2 },
                    {label: 'Effective communication.', value: 3 },
                    {label: 'Attractive communication.', value: 4 },
                ],
                //7
                [
                    {label: 'Merasa orang lain tidak punya hak-hak untuk menanyakan sesuatu yang mereka inginkan.', value: 0 },
                    {label: 'Cenderung menyerang orang lain dan menimbulkan konflik.', value: 1 },
                    {label: 'Tidak menghargai hak-hak orang lain.', value: 2 },
                    {label: 'Tidak memohon maaf jika ada kesalahan.', value: 3 },
                    {label: 'Menghargai kebutuhan, pendapat, dan perasaan.', value: 4 },
                ],
                //8
                [
                    {label: 'Merasa tidak memiliki hak untuk menanyakan apa yang diinginkan.', value: 0 },
                    {label: 'Merasa nyaman menanyakan sesuatu yang mereka inginkan.', value: 1 },
                    {label: 'Memohon maaf jika ada kesalahan.', value: 2 },
                    {label: 'Komunikasi secara mudah dan terbuka.', value: 3 },
                    {label: 'Tidak menghargai kebutuhan, pendapat, dan perasaan.', value: 4 },
                ],
                //9
                [
                    {label: 'Jangan pernah mengatakan No comment. ', value: 0 },
                    {label: 'Menjawab pertanyaan yang ditanyakan. ', value: 1 },
                    {label: 'Selalu menggunakan istilah-istilah teknik.', value: 2 },
                    {label: 'Mengadakan wawancara dengan persiapan bahan.', value: 3 },
                    {label: 'Jangan pernah menggunakan bahasa hukum.', value: 4 },
                ],
                //10
                [
                    {label: 'Lembar keselamatan kerja. ', value: 0 },
                    {label: 'Manual SOP K3. ', value: 1 },
                    {label: 'Pengisian Log Sheet harian K3.', value: 2 },
                    {label: 'Laporan bulanan.', value: 3 },
                    {label: 'Lembaran kerja komunikasi K3', value: 4 },
                ],
                //11
                [
                    {label: 'Umpan balik. ', value: 0 },
                    {label: 'Encoding ', value: 1 },
                    {label: 'Decoding', value: 2 },
                    {label: 'Media audio visual.', value: 3 },
                    {label: 'Channels', value: 4 },
                ],
                //12
                [
                    {label: 'Hasil-hasil investigasi kecelakaan kerja. ', value: 0 },
                    {label: 'Perkembangan aktivitas pengendalian bahaya di tempat kerja. ', value: 1 },
                    {label: 'Tujuan K3 dan aktivitas peningkatan berkelanjutan lainnya.', value: 2 },
                    {label: 'Aturan lalu lintas di tempat kerja.', value: 3 },
                    {label: 'Identifikasi bahaya, penilaian dan pengendalian resiko K3 di tempat kerja.', value: 4 },
                ],
                //13
                [
                    {label: 'Komitmen Perusahaan terhadap Penerapan K3 di tempat kerja.', value: 0 },
                    {label: 'Persyaratan-persyaratan K3 untuk tamu. ', value: 1 },
                    {label: 'Prosedur evakuasi darurat.', value: 2 },
                    {label: 'Aturan akses tempat kerja dan pengawalan.', value: 3 },
                    {label: 'APD (Alat Pelindung Diri) yang digunakan di tempat kerja.', value: 4 },
                ],
                //14
                [
                    {label: 'Komunikasi K3 kepada orang dengan tipe kinestetik. ', value: 0 },
                    {label: 'Komunikasi K3 kepada orang dengan tipe passive. ', value: 1 },
                    {label: 'Komunikasi K3 kepada orang dengan tipe visual.', value: 2 },
                    {label: 'Komunikasi K3 kepada orang dengan tipe examply.', value: 3 },
                    {label: 'Komunikasi K3 kepada orang dengan tipe auditory.', value: 4 },
                ],
                //15
                [
                    {label: 'Komunikasi K3 kepada orang dengan tipe kinestetik. ', value: 0 },
                    {label: 'Komunikasi K3 kepada orang dengan tipe passive. ', value: 1 },
                    {label: 'Komunikasi K3 kepada orang dengan tipe visual.', value: 2 },
                    {label: 'Komunikasi K3 kepada orang dengan tipe examply.', value: 3 },
                    {label: 'Komunikasi K3 kepada orang dengan tipe auditory.', value: 4 },
                ],
                //16
                [
                    {label: 'Komunikasi K3 kepada orang dengan tipe kinestetik. ', value: 0 },
                    {label: 'Komunikasi K3 kepada orang dengan tipe passive. ', value: 1 },
                    {label: 'Komunikasi K3 kepada orang dengan tipe visual.', value: 2 },
                    {label: 'Komunikasi K3 kepada orang dengan tipe examply.', value: 3 },
                    {label: 'Komunikasi K3 kepada orang dengan tipe auditory.', value: 4 },
                ],
                //17
                [
                    {label: 'Pelatihan Formal', value: 0 },
                    {label: 'Rambu-rambu.', value: 1 },
                    {label: 'Pelatihan Eksternal.', value: 2 },
                    {label: 'Pamflet dan Poster.', value: 3 },
                    {label: 'Komunikasi Langsung.', value: 4 },
                ],
                //18
                [
                    {label: 'Specify objectives.', value: 0 },
                    {label: 'Measures the objectives.', value: 1 },
                    {label: 'Collect and analyze data.', value: 2 },
                    {label: 'Report the result to decision maker.', value: 3 },
                    {label: 'Apply the result to decisions.', value: 4 },
                ],
                //19
                [
                    {label: 'Specify objectives.', value: 0 },
                    {label: 'Measures the objectives. ', value: 1 },
                    {label: 'Collect and analyze data.', value: 2 },
                    {label: 'Report the result to decision maker.', value: 3 },
                    {label: 'Apply the result to decisions.', value: 4 },
                ],
                [
                    {label: 'Seseorang merasa mudah mendapat kecelakaan. ', value: 0 },
                    {label: 'Orang percaya bahwa kecelakaan dapat dicegah. ', value: 1 },
                    {label: 'Seseorang yang pesimis akan adanya kecelakaan.', value: 2 },
                    {label: 'Orang memandang bahwa kecelakaan dapat berakibat fatal.', value: 3 },
                    {label: 'Orang tersebut mampu menjangkau dan memanfaatkan fasilitas K3.', value: 4 },
                ]
            ]
            return (
                <ScrollView style={{padding: 10}}>
                    <Header />
                    <View style={{backgroundColor: "#9b59b6", marginTop: 50, padding: 10, borderRadius: 10}}>
                        <Text style={{fontFamily: "Roboto-Bold", color: "white",  fontSize: 18, alignSelf: "center"}}>{"Soal Kuis dan Jawaban".toUpperCase()}</Text>
                        <Text style={{fontFamily: "Roboto", color: "white",  fontSize: 16, alignSelf: "center", textAlign: "center", marginTop: 10}}>{"Melakukan Komunikasi K3.".toUpperCase()}</Text>
                    </View>
                    {/* Soal */}

                   

                    
                    <View>
                        <View style={{flexDirection: "row", marginTop: 10, flexWrap: 'wrap'}}>
                            <Text style={style.textSoalNomer}>1. </Text>
                            <Text style={style.textSoal}>Komunikasi, apapun bentuknya/ pada dasarnya adalah hubungan timbal balik antara, kecuali?</Text>
                        </View>
                        <RadioForm
                            radio_props={dataJawaban[0]}
                            initial={this.state.jawaban[0]}
                            style={{margin:10, width: '90%'}}
                            onPress={(value) => {
                                let jawaban = this.state.jawaban;
                                jawaban[0] = value;
                                this.setState({jawaban:jawaban})
                            }}
                            />
                    </View>

                    <View>
                        <View style={{flexDirection: "row", marginTop: 10, flexWrap: 'wrap'}}>
                            <Text style={style.textSoalNomer}>2. </Text>
                            <Text style={style.textSoal}>Komunikasi K3 akan menimbulkan motivasi untuk berubah, sehingga perilaku "kerja selamat" tumbuh dalam diri setiap karyawan serta?</Text>
                        </View>
                        <RadioForm
                            radio_props={dataJawaban[1]}
                            initial={this.state.jawaban[1]}
                            style={{margin:10, width: '90%'}}
                            onPress={(value) => {
                                let jawaban = this.state.jawaban;
                                jawaban[1] = value;
                                this.setState({jawaban:jawaban})
                            }}
                            />
                    </View>

                    <View>
                        <View style={{flexDirection: "row", marginTop: 10, flexWrap: 'wrap'}}>
                            <Text style={style.textSoalNomer}>3. </Text>
                            <Text style={style.textSoal}>Pertukaran fakta, gagasan, opini atau emosi antar dua orang atau lebih disebut?</Text>
                        </View>
                        <RadioForm
                            radio_props={dataJawaban[2]}
                            initial={this.state.jawaban[2]}
                            style={{margin:10, width: '90%'}}
                            onPress={(value) => {
                                let jawaban = this.state.jawaban;
                                jawaban[2] = value;
                                this.setState({jawaban:jawaban})
                            }}
                            />
                    </View>

                    <View>
                        <View style={{flexDirection: "row", marginTop: 10, flexWrap: 'wrap'}}>
                            <Text style={style.textSoalNomer}>4. </Text>
                            <Text style={style.textSoal}>Proses komunikasi paling sedikit terdapat tiga unsur, yaitu?</Text>
                        </View>
                        <RadioForm
                            radio_props={dataJawaban[3]}
                            initial={this.state.jawaban[3]}
                            style={{margin:10, width: '90%'}}
                            onPress={(value) => {
                                let jawaban = this.state.jawaban;
                                jawaban[3] = value;
                                this.setState({jawaban:jawaban})
                            }}
                            />
                    </View>

                    <View>
                        <View style={{flexDirection: "row", marginTop: 10, flexWrap: 'wrap'}}>
                            <Text style={style.textSoalNomer}>5. </Text>
                            <Text style={style.textSoal}>Pola komunikasi yang bertujuan untuk menghindari konflik atau konfrontasi. Dalam pola komunikasi ini, seseorang tidak banyak bicara, kontak mata dengan lawan bicara sangat kurang bahkan menggunakan nada suara yang rendah. Adalah pengertian dari pola komunikasi?</Text>
                        </View>
                        <RadioForm
                            radio_props={dataJawaban[4]}
                            initial={this.state.jawaban[4]}
                            style={{margin:10, width: '90%'}}
                            onPress={(value) => {
                                let jawaban = this.state.jawaban;
                                jawaban[4] = value;
                                this.setState({jawaban:jawaban})
                            }}
                            />
                    </View>

                    <View>
                        <View style={{flexDirection: "row", marginTop: 10, flexWrap: 'wrap'}}>
                            <Text style={style.textSoalNomer}>6. </Text>
                            <Text style={style.textSoal}>Dalam berkomunikasi dengan para agresor, kontak mata yang terjadi terasa tajam dan intimidatif. Komunikasi agresif selalu melibatkan manipulasi, berusaha untuk membuat orang melakukan atau menyetujui apa yang dikatakan. Adalah pengertian dari pola komunikasi?</Text>
                        </View>
                        <RadioForm
                            radio_props={dataJawaban[5]}
                            initial={this.state.jawaban[5]}
                            style={{margin:10, width: '90%'}}
                            onPress={(value) => {
                                let jawaban = this.state.jawaban;
                                jawaban[5] = value;
                                this.setState({jawaban:jawaban})
                            }}
                            />
                    </View>



                    <View>
                        <View style={{flexDirection: "row", marginTop: 10, flexWrap: 'wrap'}}>
                            <Text style={style.textSoalNomer}>7. </Text>
                            <Text style={style.textSoal}>Beberapa ciri-ciri berikut yang tidak termasuk dalam ciri-ciri pola komunikasi Aggressive communication adalah?</Text>
                        </View>
                        <RadioForm
                            radio_props={dataJawaban[6]}
                            initial={this.state.jawaban[6]}
                            style={{margin:10, width: '90%'}}
                            onPress={(value) => {
                                let jawaban = this.state.jawaban;
                                jawaban[6] = value;
                                this.setState({jawaban:jawaban})
                            }}
                            />
                    </View>


                    <View>
                        <View style={{flexDirection: "row", marginTop: 10, flexWrap: 'wrap'}}>
                            <Text style={style.textSoalNomer}>8. </Text>
                            <Text style={style.textSoal}>Beberapa ciri berikut yang termasuk dalam ciri pola komunikasi Passive communication adalah?</Text>
                        </View>
                        <RadioForm
                            radio_props={dataJawaban[7]}
                            initial={this.state.jawaban[7]}
                            style={{margin:10, width: '90%'}}
                            onPress={(value) => {
                                let jawaban = this.state.jawaban;
                                jawaban[7] = value;
                                this.setState({jawaban:jawaban})
                            }}
                            />
                    </View>

                    <View>
                        <View style={{flexDirection: "row", marginTop: 10, flexWrap: 'wrap'}}>
                            <Text style={style.textSoalNomer}>9. </Text>
                            <Text style={style.textSoal}>Komunikasi media terdapat kode etik yang perlu diperhatikan, kecuali?</Text>
                        </View>
                        <RadioForm
                            radio_props={dataJawaban[8]}
                            initial={this.state.jawaban[8]}
                            style={{margin:10, width: '90%'}}
                            onPress={(value) => {
                                let jawaban = this.state.jawaban;
                                jawaban[8] = value;
                                this.setState({jawaban:jawaban})
                            }}
                            />
                    </View>

                    <View>
                        <View style={{flexDirection: "row", marginTop: 10, flexWrap: 'wrap'}}>
                            <Text style={style.textSoalNomer}>10. </Text>
                            <Text style={style.textSoal}>Implementasi Komunikasi K3 di perusahaan dapat menggunakan daftar periksa berupa, yaitu?</Text>
                        </View>
                        <RadioForm
                            radio_props={dataJawaban[9]}
                            initial={this.state.jawaban[9]}
                            style={{margin:10, width: '90%'}}
                            onPress={(value) => {
                                let jawaban = this.state.jawaban;
                                jawaban[9] = value;
                                this.setState({jawaban:jawaban})
                            }}
                            />
                    </View>


                    <View>
                        <View style={{flexDirection: "row", marginTop: 10, flexWrap: 'wrap'}}>
                            <Text style={style.textSoalNomer}>11. </Text>
                            <Text style={style.textSoal}>Proses komunikasi yang efektif melibatkan komunikator, pesan yang ingin disampaikan, dan penerima pesan selalu memerlukan?</Text>
                        </View>
                        <RadioForm
                            radio_props={dataJawaban[10]}
                            initial={this.state.jawaban[10]}
                            style={{margin:10, width: '90%'}}
                            onPress={(value) => {
                                let jawaban = this.state.jawaban;
                                jawaban[10] = value;
                                this.setState({jawaban:jawaban})
                            }}
                            />
                    </View>

                    <View>
                        <View style={{flexDirection: "row", marginTop: 10, flexWrap: 'wrap'}}>
                            <Text style={style.textSoalNomer}>12. </Text>
                            <Text style={style.textSoal}>Beberapa informasi berikut yang termasuk dalam Informasi-informasi terkait komunikasi eksternal dengan pengunjung/tamu adalah?</Text>
                        </View>
                        <RadioForm
                            radio_props={dataJawaban[11]}
                            initial={this.state.jawaban[11]}
                            style={{margin:10, width: '90%'}}
                            onPress={(value) => {
                                let jawaban = this.state.jawaban;
                                jawaban[11] = value;
                                this.setState({jawaban:jawaban})
                            }}
                            />
                    </View>


                    <View>
                        <View style={{flexDirection: "row", marginTop: 10, flexWrap: 'wrap'}}>
                            <Text style={style.textSoalNomer}>13. </Text>
                            <Text style={style.textSoal}>Beberapa informasi berikut yang tidak termasuk dalam Informasi-informasi terkait komunikasi eksternal dengan pengunjung/tamu adalah?</Text>
                        </View>
                        <RadioForm
                            radio_props={dataJawaban[12]}
                            initial={this.state.jawaban[12]}
                            style={{margin:10, width: '90%'}}
                            onPress={(value) => {
                                let jawaban = this.state.jawaban;
                                jawaban[12] = value;
                                this.setState({jawaban:jawaban})
                            }}
                            />
                    </View>


                    <View>
                        <View style={{flexDirection: "row", marginTop: 10, flexWrap: 'wrap'}}>
                            <Text style={style.textSoalNomer}>14. </Text>
                            <Text style={style.textSoal}>Orang yang bertipe ini tandanya orang pinter ngomong, cenderung cerewet, kemapuannya secara verbal sangat baik, dan kelemahan orang tipe ini adalah sebagai pendengar yang buruk. Merupakan ciri – ciri orang yang berkomunikasi dengan tipe?</Text>
                        </View>
                        <RadioForm
                            radio_props={dataJawaban[13]}
                            initial={this.state.jawaban[13]}
                            style={{margin:10, width: '90%'}}
                            onPress={(value) => {
                                let jawaban = this.state.jawaban;
                                jawaban[13] = value;
                                this.setState({jawaban:jawaban})
                            }}
                            />
                    </View>


                    <View>
                        <View style={{flexDirection: "row", marginTop: 10, flexWrap: 'wrap'}}>
                            <Text style={style.textSoalNomer}>15. </Text>
                            <Text style={style.textSoal}>Orang yang bertipe ini biasanya sebagai pendengar yang baik namun kurang dalam komunikasi K3 secara verbal sangat logik dalam berfikir. Merupakan ciri – ciri orang yang berkomunikasi dengan tipe?</Text>
                        </View>
                        <RadioForm
                            radio_props={dataJawaban[14]}
                            initial={this.state.jawaban[14]}
                            style={{margin:10, width: '90%'}}
                            onPress={(value) => {
                                let jawaban = this.state.jawaban;
                                jawaban[14] = value;
                                this.setState({jawaban:jawaban})
                            }}
                            />
                    </View>


                    <View>
                        <View style={{flexDirection: "row", marginTop: 10, flexWrap: 'wrap'}}>
                            <Text style={style.textSoalNomer}>16. </Text>
                            <Text style={style.textSoal}>Orang yang bertipe ini biasanya selalu menggunakan perasaan dalam bertindak, ciri cirinya jika berbicara suaranya agak dalam dan banyak jeda, ciri cirinya orang bertipe ini adalah tidak bisa diam, lebih suka praktek dan menyukai permainan, akan cepat gelisah jika matanya dipandang. Merupakan ciri – ciri orang yang berkomunikasi dengan tipe?</Text>
                        </View>
                        <RadioForm
                            radio_props={dataJawaban[15]}
                            initial={this.state.jawaban[15]}
                            style={{margin:10, width: '90%'}}
                            onPress={(value) => {
                                let jawaban = this.state.jawaban;
                                jawaban[15] = value;
                                this.setState({jawaban:jawaban})
                            }}
                            />
                    </View>


                    <View>
                        <View style={{flexDirection: "row", marginTop: 10, flexWrap: 'wrap'}}>
                            <Text style={style.textSoalNomer}>17. </Text>
                            <Text style={style.textSoal}>Beberapa metode berikut yang tidak termasuk dalam metode komunikai K3 adalah?</Text>
                        </View>
                        <RadioForm
                            radio_props={dataJawaban[16]}
                            initial={this.state.jawaban[16]}
                            style={{margin:10, width: '90%'}}
                            onPress={(value) => {
                                let jawaban = this.state.jawaban;
                                jawaban[16] = value;
                                this.setState({jawaban:jawaban})
                            }}
                            />
                    </View>


                    <View>
                        <View style={{flexDirection: "row", marginTop: 10, flexWrap: 'wrap'}}>
                            <Text style={style.textSoalNomer}>18. </Text>
                            <Text style={style.textSoal}>Setiap hasil program harus diterapkan pada pengambilan keputusan, Adalah pengertian dari langkah evaluasi komunikasi K3 yaitu?</Text>
                        </View>
                        <RadioForm
                            radio_props={dataJawaban[17]}
                            initial={this.state.jawaban[17]}
                            style={{margin:10, width: '90%'}}
                            onPress={(value) => {
                                let jawaban = this.state.jawaban;
                                jawaban[17] = value;
                                this.setState({jawaban:jawaban})
                            }}
                            />
                    </View>


                    <View>
                        <View style={{flexDirection: "row", marginTop: 10, flexWrap: 'wrap'}}>
                            <Text style={style.textSoalNomer}>19. </Text>
                            <Text style={style.textSoal}>Melakukan pengukuran efek yang sudah dicapai dari program yang sudah dijalankan atas jabaran objektif / tujuan. Adalah pengertian dari langkah evaluasi komunikasi K3 yaitu?</Text>
                        </View>
                        <RadioForm
                            radio_props={dataJawaban[18]}
                            initial={this.state.jawaban[18]}
                            style={{margin:10, width: '90%'}}
                            onPress={(value) => {
                                let jawaban = this.state.jawaban;
                                jawaban[18] = value;
                                this.setState({jawaban:jawaban})
                            }}
                            />
                    </View>


                    <View>
                        <View style={{flexDirection: "row", marginTop: 10, flexWrap: 'wrap'}}>
                            <Text style={style.textSoalNomer}>20. </Text>
                            <Text style={style.textSoal}>Perilaku K3 meliputi pengetahuan, sikap dan tindakan yang berkaitan dengan konsep K3 serta upaya pelaksanaannya. Ada empat faktor utama seseorang mau melakukan K3, kecuali?</Text>
                        </View>
                        <View style={{ marginTop: 2, marginLeft: 10}}>
                            <Text style={style.textSoal}>(1) Bentuk Tim Penyusun Rencana Tanggap Darurat </Text>
                            <Text style={style.textSoal}>(2) Identifikasi & Penilaian Risiko Kebakaran</Text>
                            <Text style={style.textSoal}>(3) Membuat/Menentukan Tujuan Dan Ruang Lingkup</Text>
                            <Text style={style.textSoal}>(4) Susun rencana untuk pelatihan simulasi atau emergency drill.</Text>
                            <Text style={style.textSoal}>(5) Menyusun Kesiapsiagaan Tanggap Darurat.</Text>
                            <Text style={style.textSoal}>(6) Evaluasi & Pemuthakhiran Prosedur.</Text>
                            <Text style={style.textSoal}>(7) Susun Organisasi Tanggap Darurat.</Text>
                            <Text style={style.textSoal}>(8) Simulasi Tanggap Darurat.</Text>
                            <Text style={style.textSoal}>(9) Susun Prosedur Tanggap Darurat.</Text>
                            <Text style={style.textSoal}>(10) Evaluasi dan Pemutakhiran.</Text>
                            <Text style={style.textSoal}>Beberapa persyaratan berikut yang termasuk alur Penyusunan Prosedur Tanggap Darurat Kebakaran & Implementasinya secara berurutan kerja adalah?</Text>
                        </View>
                        <RadioForm
                            radio_props={dataJawaban[19]}
                            initial={this.state.jawaban[19]}
                            style={{margin:10, width: '90%'}}
                            onPress={(value) => {
                                let jawaban = this.state.jawaban;
                                jawaban[19] = value;
                                this.setState({jawaban:jawaban})
                            }}
                            />
                    </View>
                    

                    
                </ScrollView>
            )
        }else if(this.state.idMateri == 4) {
            var dataJawaban = [
                //1
                [
                    {label: 'Lama waktu kerja.', value: 0 },
                    {label: 'Tingkat resiko dan kompleksitas pekerjaan.', value: 1 },
                    {label: 'Jumlah tenaga kerja.', value: 2 },
                    {label: 'Lokasi kerja.', value: 3 },
                    {label: 'Alat-alat ynag digunakan saat bekerja.', value: 4 },
                ],
                //2
                [
                    {label: 'Manajemen, supervisor, operator, dan pekerja.', value: 0 },
                    {label: 'Sesama pekerja. ', value: 1 },
                    {label: 'Pekerja dengan masyarakat sekitar.', value: 2 },
                    {label: 'Pekerja dengan kontraktor.', value: 3 },
                    {label: 'Perusahaan dan kontraktor.', value: 4 },
                ],
                //3
                [
                    {label: 'Terciptanya lingkungan kerja yang aman dan nyaman.', value: 0 },
                    {label: 'Menjelaskan kepada pekerjaan tentang identitas pekerjaan. ', value: 1 },
                    {label: 'Meminimalisir angka kecelakaan kerja.', value: 2 },
                    {label: 'Formalitas saja.', value: 3 },
                    {label: 'Supaya tidak mendapat teguran dari perusahaan.', value: 4 },
                ],
                //4
                [
                    {label: 'Job safety analysis.', value: 0 },
                    {label: 'Preliminary hazard analysis. ', value: 1 },
                    {label: 'Cold work permitt.', value: 2 },
                    {label: 'Hazard operability study.', value: 3 },
                    {label: 'Risk based inspection.', value: 4 },
                ],
                //5
                [
                    {label: 'Hijau.', value: 0 },
                    {label: 'Biru. ', value: 1 },
                    {label: 'Putih.', value: 2 },
                    {label: 'Merah.', value: 3 },
                    {label: 'Kuning.', value: 4 },
                ],
                //6
                [
                    {label: 'Deskripsi pekerjaan.', value: 0 },
                    {label: 'Deskripsi lokasi kerja. ', value: 1 },
                    {label: 'Daftar rekan kerja.', value: 2 },
                    {label: 'Rincian peralatan kerja.', value: 3 },
                    {label: 'Rincian potensi bahaya.', value: 4 },
                ],
                //7
                [
                    {label: 'Mengetahui bahan apa saja yang digunakan ditempat kerja.', value: 0 },
                    {label: 'Mengerti alat-alat yang digunakan di tempat kerja. ', value: 1 },
                    {label: 'Memahami kondisi pekerja.', value: 2 },
                    {label: 'Menganalisis kondisi di lingkungan kerja.', value: 3 },
                    {label: 'Proses dan potensi bahaya di tempat kerja.', value: 4 },
                ],
                //8
                [
                    {label: 'Pembatalan permitt sesuai dengsn pemenuhan terhadap pekerjaan tersebut.', value: 0 },
                    {label: 'Menerima permitt dari authorized person. ', value: 1 },
                    {label: 'Melakukan pengawasan dibawah competent person.', value: 2 },
                    {label: 'Memastikan bahwa permitt sudah cocok dan sesuai dengan persyaratan.', value: 3 },
                    {label: 'Memberi solusi kepada manager untuk melakukan pekerjaan dengan aman.', value: 4 },
                ],
                //9
                [
                    {label: 'Checklist.', value: 0 },
                    {label: 'Grafik. ', value: 1 },
                    {label: 'Tabel.', value: 2 },
                    {label: 'Data.', value: 3 },
                    {label: 'Foto.', value: 4 },
                ],
                //10
                [
                    {label: 'Planning/perencanaan.', value: 0 },
                    {label: 'Monitoring/pemantauan. ', value: 1 },
                    {label: 'Tindakan darurat.', value: 2 },
                    {label: 'Perpanjang permitt.', value: 3 },
                    {label: 'Tanda tangan.', value: 4 },
                ],
                //11
                [
                    {label: '3 hari .', value: 0 },
                    {label: '5 hari. ', value: 1 },
                    {label: '7 hari.', value: 2 },
                    {label: '10 hari.', value: 3 },
                    {label: '15 hari.', value: 4 },
                ],
                //12
                [
                    {label: 'Hazard assesment .', value: 0 },
                    {label: 'Safety induction. ', value: 1 },
                    {label: 'Safety champagn.', value: 2 },
                    {label: 'HSE marshall.', value: 3 },
                    {label: 'Job safety analysis.', value: 4 },
                ],
                //13
                [
                    {label: 'Permitt issuer .', value: 0 },
                    {label: 'Authorized person. ', value: 1 },
                    {label: 'Supervisor.', value: 2 },
                    {label: 'Site manager.', value: 3 },
                    {label: 'Kontraktor.', value: 4 },
                ],
                //14
                [
                    {label: 'Tidak perlu dilakukan perpanjangan permitt.', value: 0 },
                    {label: 'Kondisi pekerja tidak memungkinkan untuk melakukan pekerjaan. ', value: 1 },
                    {label: 'Pekerja sudah tidak bekerja lagi.', value: 2 },
                    {label: 'Hand-over shift antar pekerja.', value: 3 },
                    {label: 'Satu shift sudah selesai tetapi pekerjaan belum selesai.', value: 4 },
                ],
                //15
                [
                    {label: 'Pekerja melanggar peraturan.', value: 0 },
                    {label: 'Terdapat keadaan darurat. ', value: 1 },
                    {label: 'Pergantian shift pekerja.', value: 2 },
                    {label: 'Kondisi di tempat kerja tidak aman.', value: 3 },
                    {label: 'Pergantian personil.', value: 4 },
                ],
                //16
                [
                    {label: 'Sesekali.', value: 0 },
                    {label: 'Kondisi tertentu. ', value: 1 },
                    {label: 'Sebulan sekali.', value: 2 },
                    {label: 'Berkelanjutan.', value: 3 },
                    {label: 'Pergantian shift kerja.', value: 4 },
                ],
                //17
                [
                    {label: 'Permitt issuer dan supervisor.', value: 0 },
                    {label: 'Pekerja. ', value: 1 },
                    {label: 'Kontraktor.', value: 2 },
                    {label: 'Competent person.', value: 3 },
                    {label: 'Spesialist.', value: 4 },
                ],
                //18
                [
                    {label: 'Meminimalisir angka kecelakaan kerja.', value: 0 },
                    {label: 'Memastikan tempat kerja dalam kondisi aman. ', value: 1 },
                    {label: 'Meminimalisir kerugian yang ditanggung perusahaan.', value: 2 },
                    {label: 'Meninjau efektifitas permitt yang telah dilaksanakan.', value: 3 },
                    {label: 'Memutuskan apakah akan melanjutkan sistem permitt sebelumnya atau tidak.', value: 4 },
                ],
                //19
                [
                    {label: 'Kontraktor.', value: 0 },
                    {label: 'Permitt issuer. ', value: 1 },
                    {label: 'Pekerja.', value: 2 },
                    {label: 'Authorized person.', value: 3 },
                    {label: 'Competent person.', value: 4 },
                ],
                //20
                [
                    {label: '1 bulan.', value: 0 },
                    {label: '3 bulan. ', value: 1 },
                    {label: '6 bulan .', value: 2 },
                    {label: '10 bulan.', value: 3 },
                    {label: '12 bulan.', value: 4 },
                ]
            ]
            return (
                <ScrollView style={{padding: 10}}>
                    <Header />
                    <View style={{backgroundColor: "#9b59b6", marginTop: 50, padding: 10, borderRadius: 10}}>
                        <Text style={{fontFamily: "Roboto-Bold", color: "white",  fontSize: 18, alignSelf: "center"}}>{"Soal Kuis dan Jawaban".toUpperCase()}</Text>
                        <Text style={{fontFamily: "Roboto", color: "white",  fontSize: 16, alignSelf: "center", textAlign: "center", marginTop: 10}}>{"Mengawasi Pelaksanaan Izin Kerja.".toUpperCase()}</Text>
                    </View>
                    {/* Soal */}

                   

                    
                    <View>
                        <View style={{flexDirection: "row", marginTop: 10, flexWrap: 'wrap'}}>
                            <Text style={style.textSoalNomer}>1. </Text>
                            <Text style={style.textSoal}>Keputusan untuk memberlakukan sistem izin kerja untuk pekerjaan tertentu akan tergantung pada keadaan yaitu?</Text>
                        </View>
                        <RadioForm
                            radio_props={dataJawaban[0]}
                            initial={this.state.jawaban[0]}
                            style={{margin:10, width: '90%'}}
                            onPress={(value) => {
                                let jawaban = this.state.jawaban;
                                jawaban[0] = value;
                                this.setState({jawaban:jawaban})
                            }}
                            />
                    </View>

                    <View>
                        <View style={{flexDirection: "row", marginTop: 10, flexWrap: 'wrap'}}>
                            <Text style={style.textSoalNomer}>2. </Text>
                            <Text style={style.textSoal}>Sistem izin kerja adalah sarana komunikasi antara lain, yaitu?</Text>
                        </View>
                        <RadioForm
                            radio_props={dataJawaban[1]}
                            initial={this.state.jawaban[1]}
                            style={{margin:10, width: '90%'}}
                            onPress={(value) => {
                                let jawaban = this.state.jawaban;
                                jawaban[1] = value;
                                this.setState({jawaban:jawaban})
                            }}
                            />
                    </View>

                    <View>
                        <View style={{flexDirection: "row", marginTop: 10, flexWrap: 'wrap'}}>
                            <Text style={style.textSoalNomer}>3. </Text>
                            <Text style={style.textSoal}>Tujuan dengan adanya sistem izin kerja atau permitt to work adalah?</Text>
                        </View>
                        <RadioForm
                            radio_props={dataJawaban[2]}
                            initial={this.state.jawaban[2]}
                            style={{margin:10, width: '90%'}}
                            onPress={(value) => {
                                let jawaban = this.state.jawaban;
                                jawaban[2] = value;
                                this.setState({jawaban:jawaban})
                            }}
                            />
                    </View>

                    <View>
                        <View style={{flexDirection: "row", marginTop: 10, flexWrap: 'wrap'}}>
                            <Text style={style.textSoalNomer}>4. </Text>
                            <Text style={style.textSoal}>Beberapa hal berikut yang termasuk dalam Salah satu jenis dari sistem izin kerja atau permit to work adalah ?</Text>
                        </View>
                        <RadioForm
                            radio_props={dataJawaban[3]}
                            initial={this.state.jawaban[3]}
                            style={{margin:10, width: '90%'}}
                            onPress={(value) => {
                                let jawaban = this.state.jawaban;
                                jawaban[3] = value;
                                this.setState({jawaban:jawaban})
                            }}
                            />
                    </View>

                    <View>
                        <View style={{flexDirection: "row", marginTop: 10, flexWrap: 'wrap'}}>
                            <Text style={style.textSoalNomer}>5. </Text>
                            <Text style={style.textSoal}>Apa warna yang digunakan pada kertas izin kerja panas?</Text>
                        </View>
                        <RadioForm
                            radio_props={dataJawaban[4]}
                            initial={this.state.jawaban[4]}
                            style={{margin:10, width: '90%'}}
                            onPress={(value) => {
                                let jawaban = this.state.jawaban;
                                jawaban[4] = value;
                                this.setState({jawaban:jawaban})
                            }}
                            />
                    </View>

                    <View>
                        <View style={{flexDirection: "row", marginTop: 10, flexWrap: 'wrap'}}>
                            <Text style={style.textSoalNomer}>6. </Text>
                            <Text style={style.textSoal}>Beberapa persyaratan berikut yang tidak termasuk dalam persyaratan formulir izin kerja adalah?</Text>
                        </View>
                        <RadioForm
                            radio_props={dataJawaban[5]}
                            initial={this.state.jawaban[5]}
                            style={{margin:10, width: '90%'}}
                            onPress={(value) => {
                                let jawaban = this.state.jawaban;
                                jawaban[5] = value;
                                this.setState({jawaban:jawaban})
                            }}
                            />
                    </View>



                    <View>
                        <View style={{flexDirection: "row", marginTop: 10, flexWrap: 'wrap'}}>
                            <Text style={style.textSoalNomer}>7. </Text>
                            <Text style={style.textSoal}>Kemampuan dan kompetensi apa yang harus dikuasai oleh orang yang mengeluarkan formulir izin kerja, yaitu?</Text>
                        </View>
                        <RadioForm
                            radio_props={dataJawaban[6]}
                            initial={this.state.jawaban[6]}
                            style={{margin:10, width: '90%'}}
                            onPress={(value) => {
                                let jawaban = this.state.jawaban;
                                jawaban[6] = value;
                                this.setState({jawaban:jawaban})
                            }}
                            />
                    </View>


                    <View>
                        <View style={{flexDirection: "row", marginTop: 10, flexWrap: 'wrap'}}>
                            <Text style={style.textSoalNomer}>8. </Text>
                            <Text style={style.textSoal}>Tugas dan tanggung jawab engineer dalam menerapkan sistem izin kerja adalah?</Text>
                        </View>
                        <RadioForm
                            radio_props={dataJawaban[7]}
                            initial={this.state.jawaban[7]}
                            style={{margin:10, width: '90%'}}
                            onPress={(value) => {
                                let jawaban = this.state.jawaban;
                                jawaban[7] = value;
                                this.setState({jawaban:jawaban})
                            }}
                            />
                    </View>

                    <View>
                        <View style={{flexDirection: "row", marginTop: 10, flexWrap: 'wrap'}}>
                            <Text style={style.textSoalNomer}>9. </Text>
                            <Text style={style.textSoal}>Monitoring work permitt diperlukan untuk memastikan tempat kerja aman. Biasanya monitoring work permitt dibuat dalam bentuk, yaitu?</Text>
                        </View>
                        <RadioForm
                            radio_props={dataJawaban[8]}
                            initial={this.state.jawaban[8]}
                            style={{margin:10, width: '90%'}}
                            onPress={(value) => {
                                let jawaban = this.state.jawaban;
                                jawaban[8] = value;
                                this.setState({jawaban:jawaban})
                            }}
                            />
                    </View>

                    <View>
                        <View style={{flexDirection: "row", marginTop: 10, flexWrap: 'wrap'}}>
                            <Text style={style.textSoalNomer}>10. </Text>
                            <Text style={style.textSoal}>Beberapa persyaratan berikut yang termasuk dalam tahap persiapan dalam penerapan sistem permitt to work adalah?</Text>
                        </View>
                        <RadioForm
                            radio_props={dataJawaban[9]}
                            initial={this.state.jawaban[9]}
                            style={{margin:10, width: '90%'}}
                            onPress={(value) => {
                                let jawaban = this.state.jawaban;
                                jawaban[9] = value;
                                this.setState({jawaban:jawaban})
                            }}
                            />
                    </View>


                    <View>
                        <View style={{flexDirection: "row", marginTop: 10, flexWrap: 'wrap'}}>
                            <Text style={style.textSoalNomer}>11. </Text>
                            <Text style={style.textSoal}>Masa berlaku permitt to work tergantung dari kebutuhan pekerjaan. Paling lama masa berlaku permitt to work adalah?</Text>
                        </View>
                        <RadioForm
                            radio_props={dataJawaban[10]}
                            initial={this.state.jawaban[10]}
                            style={{margin:10, width: '90%'}}
                            onPress={(value) => {
                                let jawaban = this.state.jawaban;
                                jawaban[10] = value;
                                this.setState({jawaban:jawaban})
                            }}
                            />
                    </View>

                    <View>
                        <View style={{flexDirection: "row", marginTop: 10, flexWrap: 'wrap'}}>
                            <Text style={style.textSoalNomer}>12. </Text>
                            <Text style={style.textSoal}>Salah satu teknik perencanaan dalam permiit to work yang cukup efektif adalah?</Text>
                        </View>
                        <RadioForm
                            radio_props={dataJawaban[11]}
                            initial={this.state.jawaban[11]}
                            style={{margin:10, width: '90%'}}
                            onPress={(value) => {
                                let jawaban = this.state.jawaban;
                                jawaban[11] = value;
                                this.setState({jawaban:jawaban})
                            }}
                            />
                    </View>


                    <View>
                        <View style={{flexDirection: "row", marginTop: 10, flexWrap: 'wrap'}}>
                            <Text style={style.textSoalNomer}>13. </Text>
                            <Text style={style.textSoal}>Siapa orang yang bertugas mendapatkan salinan distribusi formulir work permitt, yaitu?</Text>
                        </View>
                        <RadioForm
                            radio_props={dataJawaban[12]}
                            initial={this.state.jawaban[12]}
                            style={{margin:10, width: '90%'}}
                            onPress={(value) => {
                                let jawaban = this.state.jawaban;
                                jawaban[12] = value;
                                this.setState({jawaban:jawaban})
                            }}
                            />
                    </View>


                    <View>
                        <View style={{flexDirection: "row", marginTop: 10, flexWrap: 'wrap'}}>
                            <Text style={style.textSoalNomer}>14. </Text>
                            <Text style={style.textSoal}>Perpanjangan permitt biasanya dilakukan pada beberapa kondisi, yaitu?</Text>
                        </View>
                        <RadioForm
                            radio_props={dataJawaban[13]}
                            initial={this.state.jawaban[13]}
                            style={{margin:10, width: '90%'}}
                            onPress={(value) => {
                                let jawaban = this.state.jawaban;
                                jawaban[13] = value;
                                this.setState({jawaban:jawaban})
                            }}
                            />
                    </View>


                    <View>
                        <View style={{flexDirection: "row", marginTop: 10, flexWrap: 'wrap'}}>
                            <Text style={style.textSoalNomer}>15. </Text>
                            <Text style={style.textSoal}>Beberapa persyaratan berikut diperlukan dalam persyaratan pembatalan permitt yaitu?</Text>
                        </View>
                        <RadioForm
                            radio_props={dataJawaban[14]}
                            initial={this.state.jawaban[14]}
                            style={{margin:10, width: '90%'}}
                            onPress={(value) => {
                                let jawaban = this.state.jawaban;
                                jawaban[14] = value;
                                this.setState({jawaban:jawaban})
                            }}
                            />
                    </View>


                    <View>
                        <View style={{flexDirection: "row", marginTop: 10, flexWrap: 'wrap'}}>
                            <Text style={style.textSoalNomer}>16. </Text>
                            <Text style={style.textSoal}>Berapa kali pemantauan/monitoring dalam penerapan sistem permitt to work dilakukan, yaitu?</Text>
                        </View>
                        <RadioForm
                            radio_props={dataJawaban[15]}
                            initial={this.state.jawaban[15]}
                            style={{margin:10, width: '90%'}}
                            onPress={(value) => {
                                let jawaban = this.state.jawaban;
                                jawaban[15] = value;
                                this.setState({jawaban:jawaban})
                            }}
                            />
                    </View>


                    <View>
                        <View style={{flexDirection: "row", marginTop: 10, flexWrap: 'wrap'}}>
                            <Text style={style.textSoalNomer}>17. </Text>
                            <Text style={style.textSoal}>Beberapa persyaratan berikut yang termasuk dalam pengembalian formulir permitt to wotk, siapa orang yang harus menandatangani salinan tersebut, adalah?</Text>
                        </View>
                        <RadioForm
                            radio_props={dataJawaban[16]}
                            initial={this.state.jawaban[16]}
                            style={{margin:10, width: '90%'}}
                            onPress={(value) => {
                                let jawaban = this.state.jawaban;
                                jawaban[16] = value;
                                this.setState({jawaban:jawaban})
                            }}
                            />
                    </View>


                    <View>
                        <View style={{flexDirection: "row", marginTop: 10, flexWrap: 'wrap'}}>
                            <Text style={style.textSoalNomer}>18. </Text>
                            <Text style={style.textSoal}>Tujuan dari dilakukannya inspeksi lokal dalam tahap penyelesaian permitt to work adalah?</Text>
                        </View>
                        <RadioForm
                            radio_props={dataJawaban[17]}
                            initial={this.state.jawaban[17]}
                            style={{margin:10, width: '90%'}}
                            onPress={(value) => {
                                let jawaban = this.state.jawaban;
                                jawaban[17] = value;
                                this.setState({jawaban:jawaban})
                            }}
                            />
                    </View>


                    <View>
                        <View style={{flexDirection: "row", marginTop: 10, flexWrap: 'wrap'}}>
                            <Text style={style.textSoalNomer}>19. </Text>
                            <Text style={style.textSoal}>Sistem permitt to work harus dibuat catatan dan disimpan oleh siapa?</Text>
                        </View>
                        <RadioForm
                            radio_props={dataJawaban[18]}
                            initial={this.state.jawaban[18]}
                            style={{margin:10, width: '90%'}}
                            onPress={(value) => {
                                let jawaban = this.state.jawaban;
                                jawaban[18] = value;
                                this.setState({jawaban:jawaban})
                            }}
                            />
                    </View>


                    <View>
                        <View style={{flexDirection: "row", marginTop: 10, flexWrap: 'wrap'}}>
                            <Text style={style.textSoalNomer}>20. </Text>
                            <Text style={style.textSoal}>Berapa lama periode penyimpanan formulir permitt to work, yaitu?</Text>
                        </View>
                        <RadioForm
                            radio_props={dataJawaban[19]}
                            initial={this.state.jawaban[19]}
                            style={{margin:10, width: '90%'}}
                            onPress={(value) => {
                                let jawaban = this.state.jawaban;
                                jawaban[19] = value;
                                this.setState({jawaban:jawaban})
                            }}
                            />
                    </View>
                    

                    
                </ScrollView>
            )
        }else if(this.state.idMateri == 5) {
            var dataJawaban = [
                [
                    {label: 'Aktivitas rutin maupun non-rutin.', value: 0 },
                    {label: 'Aktivitas keluarga pekerja.', value: 1 },
                    {label: 'Aktivitas siapa saja yang mendapat akses ke tempat kerja.', value: 2 },
                    {label: 'Faktor budaya manusia.', value: 3 },
                    {label: 'Bahaya aspek lingkungan di tempat kerja.', value: 4 },
                ],
                [
                    {label: 'Penilaian risiko.', value: 0 },
                    {label: 'Pengendalian bahaya. ', value: 1 },
                    {label: 'Identifikasi risiko.', value: 2 },
                    {label: 'Administrasi bahaya.', value: 3 },
                    {label: 'Penghapusan risiko.', value: 4 },
                ],
                [
                    {label: 'Masyarakat.', value: 0 },
                    {label: 'Infrastruktur. ', value: 1 },
                    {label: 'Peralatan.', value: 2 },
                    {label: 'Permesinan.', value: 3 },
                    {label: 'Bahan dan material.', value: 4 },
                ],
                [
                    {label: 'Mengidentifikasi, pengendalian bahaya, penilaian risiko.', value: 0 },
                    {label: 'Mengidentifikasi, penilaian risiko, pengendalian bahaya.', value: 1 },
                    {label: 'Menghilangkan, pengendalian bahaya, penilaian risiko.', value: 2 },
                    {label: 'Menghilangkan, penilaian risiko, pengendalian bahaya.', value: 3 },
                    {label: 'Melaporkan, penghapusan, perawatan alat pelindung diri.', value: 4 },
                ],
                [
                    {label: 'Dijadikan dasar perencanaan dan pelaksanaan K3.', value: 0 },
                    {label: 'Dijadikan lanjutan dalam penanggulangan bencana.', value: 1 },
                    {label: 'Sebagai bahan masukan kepada manajer perusahaan.', value: 2 },
                    {label: 'Dibiarkan saja, karena sudah ada bagian yang menanganinya.', value: 3 },
                    {label: 'Sebagai pertimbangan dalam perencanaan keuangan perusahaan.', value: 4 },
                ],
                [
                    {label: 'Masyarakat.', value: 0 },
                    {label: 'Tamu.', value: 1 },
                    {label: 'Pengunjung.', value: 2 },
                    {label: 'Kontraktor.', value: 3 },
                    {label: 'Suplier.', value: 4 },
                ],
                [
                    {label: 'Tanah.', value: 0 },
                    {label: 'Air. ', value: 1 },
                    {label: 'Manusia.', value: 2 },
                    {label: 'Flora.', value: 3 },
                    {label: 'Fauna.', value: 4 },
                ],
                [
                    {label: 'Kebisingan, suhu ekstrim, getaran.', value: 0 },
                    {label: 'Air. Debu, gas, uap. ', value: 1 },
                    {label: 'Manusia. Bakteri, virus, jamur.', value: 2 },
                    {label: 'Stress.', value: 3 },
                    {label: 'Sikap dan cara kerja yang tidak sesuai.', value: 4 },
                ],
                [
                    {label: 'Meningkatnya detak jantung.', value: 0 },
                    {label: 'Kanker. ', value: 1 },
                    {label: 'Pusing.', value: 2 },
                    {label: 'Mual.', value: 3 },
                    {label: 'Perubahan irama jantung.', value: 4 },
                ],
                [
                    {label: 'Berubahnya fungsi jaringan yang terpapar radiasi.', value: 0 },
                    {label: 'Terjadi kematian sel.', value: 1 },
                    {label: 'Perubahan pada sistem biologik.', value: 2 },
                    {label: 'Sifat sel baru akan diwariskan kepada turunannya/pewarisan.', value: 3 },
                    {label: 'Sel tumbuh menjadi jaringan ganas/kanker.', value: 4 },
                ],
                [
                    {label: 'Radiasi boleh asal tidak menimbulkan kecelakaan.', value: 0 },
                    {label: 'Dosis radiasi tidak boleh kelebihi nilai ambang batas yang telah ditentukan.', value: 1 },
                    {label: 'Radiasi hanya terpapar pada pekerja tertentu.', value: 2 },
                    {label: 'Radiasi dilakukan pada waktu tertentu.', value: 3 },
                    {label: 'Radiasi dilakukan dalam waktu sebentar.', value: 4 },
                ],
                [
                    {label: 'Terganggunya indera pendengaran.', value: 0 },
                    {label: 'Masalah komunikasi antar pekerja.', value: 1 },
                    {label: 'Merasa pusing akibat berteriak.', value: 2 },
                    {label: 'Penurunan konsentrasi.', value: 3 },
                    {label: 'Tuli permanen.', value: 4 },
                ],
                [
                    {label: 'Apron.', value: 0 },
                    {label: 'Goggles.', value: 1 },
                    {label: 'Ear muffs.', value: 2 },
                    {label: 'Air Supplied respirator.', value: 3 },
                    {label: 'Safety Shoes.', value: 4 },
                ],
                [
                    {label: 'Apron.', value: 0 },
                    {label: 'Goggles.', value: 1 },
                    {label: 'Ear muffs.', value: 2 },
                    {label: 'Air Supplied respirator.', value: 3 },
                    {label: 'Safety Shoes.', value: 4 },
                ],
                [
                    {label: 'Apron.', value: 0 },
                    {label: 'Goggles.', value: 1 },
                    {label: 'Ear muffs.', value: 2 },
                    {label: 'Air Supplied respirator.', value: 3 },
                    {label: 'Safety Shoes.', value: 4 },
                ],
                [
                    {label: 'Apron.', value: 0 },
                    {label: 'Goggles.', value: 1 },
                    {label: 'Ear muffs.', value: 2 },
                    {label: 'Air Supplied respirator.', value: 3 },
                    {label: 'Safety Shoes.', value: 4 },
                ],
                [
                    {label: 'Apron.', value: 0 },
                    {label: 'Goggles.', value: 1 },
                    {label: 'Ear muffs.', value: 2 },
                    {label: 'Air Supplied respirator.', value: 3 },
                    {label: 'Safety Shoes.', value: 4 },
                ],
                [
                    {label: 'Metode pengukuran faktor bahaya di tempat kerja ditentukan sesuai keinginan manajer.', value: 0 },
                    {label: 'Metode pengukuran faktor bahaya di tempat kerja ditentukan sesuai keinginan manajer. ', value: 1 },
                    {label: 'Pengukuran faktor bahaya di tempat kerja dilakukan sesuai standar dan pemetaan titik sampling.', value: 2 },
                    {label: 'Alat ukur faktor bahaya K3 digunakan sesuai prosedur.', value: 3 },
                    {label: 'Hasil pengukuran dibandingkan dengan peraturan perundang-undangan atau standar yang berlaku.', value: 4 },
                ],
                [
                    {label: '12 jam.', value: 0 },
                    {label: '11 jam. ', value: 1 },
                    {label: '10 jam.', value: 2 },
                    {label: '9 jam .', value: 3 },
                    {label: '8 jam.', value: 4 },
                ],
                [
                    {label: 'Mengalami gangguan emosional.', value: 0 },
                    {label: 'Mengalami gangguan pernapasan. ', value: 1 },
                    {label: 'Mengalami gangguan pencernaan.', value: 2 },
                    {label: 'Mengalami gangguan pendengaran .', value: 3 },
                    {label: 'Mengalami gangguan penciuman.', value: 4 },
                ]
            ]
            return (
                <ScrollView style={{padding: 10}}>
                    <Header />
                    <View style={{backgroundColor: "#9b59b6", marginTop: 50, padding: 10, borderRadius: 10}}>
                        <Text style={{fontFamily: "Roboto-Bold", color: "white",  fontSize: 18, alignSelf: "center"}}>{"Soal Kuis dan Jawaban".toUpperCase()}</Text>
                        <Text style={{fontFamily: "Roboto", color: "white",  fontSize: 16, alignSelf: "center", textAlign: "center", marginTop: 10}}>{"Melakukan Pengukuran Faktor Bahaya di Tempat Kerja.".toUpperCase()}</Text>
                    </View>
                    {/* Soal */}


                    

                    <View>
                        <View style={{flexDirection: "row", marginTop: 10, flexWrap: 'wrap'}}>
                            <Text style={style.textSoalNomer}>1. </Text>
                            <Text style={style.textSoal}>Beberapa persyaratan berikut yang tidak termasuk dalam form identifikasi bahaya adalah?</Text>
                        </View>
                        <RadioForm
                            radio_props={dataJawaban[0]}
                            initial={this.state.jawaban[0]}
                            style={{margin:10, width: '90%'}}
                            onPress={(value) => {
                                let jawaban = this.state.jawaban;
                                jawaban[0] = value;
                                this.setState({jawaban:jawaban})
                            }}
                            />
                    </View>

                    <View>
                        <View style={{flexDirection: "row", marginTop: 10, flexWrap: 'wrap'}}>
                            <Text style={style.textSoalNomer}>2. </Text>
                            <Text style={style.textSoal}>Menemukan atau mengetahui risiko–risiko yang mungkin timbul dalam kegiatan yang dilakukan oleh perusahaan atau perorangan, termasuk penjelasan dari?</Text>
                        </View>
                        <RadioForm
                            radio_props={dataJawaban[1]}
                            initial={this.state.jawaban[1]}
                            style={{margin:10, width: '90%'}}
                            onPress={(value) => {
                                let jawaban = this.state.jawaban;
                                jawaban[1] = value;
                                this.setState({jawaban:jawaban})
                            }}
                            />
                    </View>

                    <View>
                        <View style={{flexDirection: "row", marginTop: 10, flexWrap: 'wrap'}}>
                            <Text style={style.textSoalNomer}>3. </Text>
                            <Text style={style.textSoal}>Beberapa aspek berikut yang tidak termasuk dalam form identifikasi risiko pada aktivitas operasional pekerjaan adalah?</Text>
                        </View>
                        <RadioForm
                            radio_props={dataJawaban[2]}
                            initial={this.state.jawaban[2]}
                            style={{margin:10, width: '90%'}}
                            onPress={(value) => {
                                let jawaban = this.state.jawaban;
                                jawaban[2] = value;
                                this.setState({jawaban:jawaban})
                            }}
                            />
                    </View>

                    <View>
                        <View style={{flexDirection: "row", marginTop: 10, flexWrap: 'wrap'}}>
                            <Text style={style.textSoalNomer}>4. </Text>
                            <Text style={style.textSoal}>Form identifikasi bahaya biasanya digunakan untuk .... semua potensi bahaya K3 yang terdapat di dalam aktivitas-aktivitas Organisasi/Perusahaan di tempat kerja, dilanjutkan dengan melakukan ..... dari potensi bahaya tersebut serta menentukan langkah-langkah ...... dan risiko K3. Isilah titik-titik sesuai jawaban yang benar pada kalimat tersebut.</Text>
                        </View>
                        <RadioForm
                            radio_props={dataJawaban[3]}
                            initial={this.state.jawaban[3]}
                            style={{margin:10, width: '90%'}}
                            onPress={(value) => {
                                let jawaban = this.state.jawaban;
                                jawaban[3] = value;
                                this.setState({jawaban:jawaban})
                            }}
                            />
                    </View>

                    <View>
                        <View style={{flexDirection: "row", marginTop: 10, flexWrap: 'wrap'}}>
                            <Text style={style.textSoalNomer}>5. </Text>
                            <Text style={style.textSoal}>Apakah hasil dari penerapan form identifikasi risiko di tempat kerja?</Text>
                        </View>
                        <RadioForm
                            radio_props={dataJawaban[4]}
                            initial={this.state.jawaban[4]}
                            style={{margin:10, width: '90%'}}
                            onPress={(value) => {
                                let jawaban = this.state.jawaban;
                                jawaban[4] = value;
                                this.setState({jawaban:jawaban})
                            }}
                            />
                    </View>

                    <View>
                        <View style={{flexDirection: "row", marginTop: 10, flexWrap: 'wrap'}}>
                            <Text style={style.textSoalNomer}>6. </Text>
                            <Text style={style.textSoal}>Beberapa persyaratan berikut yang tidak termasuk dalam aktivitas siapa saja yang mendapat akses ke tempat kerja adalah?</Text>
                        </View>
                        <RadioForm
                            radio_props={dataJawaban[5]}
                            initial={this.state.jawaban[5]}
                            style={{margin:10, width: '90%'}}
                            onPress={(value) => {
                                let jawaban = this.state.jawaban;
                                jawaban[5] = value;
                                this.setState({jawaban:jawaban})
                            }}
                            />
                    </View>



                    <View>
                        <View style={{flexDirection: "row", marginTop: 10, flexWrap: 'wrap'}}>
                            <Text style={style.textSoalNomer}>7. </Text>
                            <Text style={style.textSoal}>Beberapa aspek berikut yang tidak termasuk dalam bahaya aspek lingkungan di tempat kerja adalah?</Text>
                        </View>
                        <RadioForm
                            radio_props={dataJawaban[6]}
                            initial={this.state.jawaban[6]}
                            style={{margin:10, width: '90%'}}
                            onPress={(value) => {
                                let jawaban = this.state.jawaban;
                                jawaban[6] = value;
                                this.setState({jawaban:jawaban})
                            }}
                            />
                    </View>


                    <View>
                        <View style={{flexDirection: "row", marginTop: 10, flexWrap: 'wrap'}}>
                            <Text style={style.textSoalNomer}>8. </Text>
                            <Text style={style.textSoal}>Potensi bahaya yang ada di tempat kerja salah satunya adalah potensi bahaya fisik, dibawah ini yang merupakan kategori potensi bahaya fisik adalah?</Text>
                        </View>
                        <RadioForm
                            radio_props={dataJawaban[7]}
                            initial={this.state.jawaban[7]}
                            style={{margin:10, width: '90%'}}
                            onPress={(value) => {
                                let jawaban = this.state.jawaban;
                                jawaban[7] = value;
                                this.setState({jawaban:jawaban})
                            }}
                            />
                    </View>

                    <View>
                        <View style={{flexDirection: "row", marginTop: 10, flexWrap: 'wrap'}}>
                            <Text style={style.textSoalNomer}>9. </Text>
                            <Text style={style.textSoal}>Efek tertunda dari terpapar radiasi dalam waktu yang lama adalah?</Text>
                        </View>
                        <RadioForm
                            radio_props={dataJawaban[8]}
                            initial={this.state.jawaban[8]}
                            style={{margin:10, width: '90%'}}
                            onPress={(value) => {
                                let jawaban = this.state.jawaban;
                                jawaban[8] = value;
                                this.setState({jawaban:jawaban})
                            }}
                            />
                    </View>

                    <View>
                        <View style={{flexDirection: "row", marginTop: 10, flexWrap: 'wrap'}}>
                            <Text style={style.textSoalNomer}>10. </Text>
                            <Text style={style.textSoal}>Salah satu efek dari terpaparnya radiasi adalah terjadinya perubahan sel molekul pada manusia. Jika perubahan tersebut terjadi pada sel genetik maka yang terjadi selanjutnya adalah?</Text>
                        </View>
                        <RadioForm
                            radio_props={dataJawaban[9]}
                            initial={this.state.jawaban[9]}
                            style={{margin:10, width: '90%'}}
                            onPress={(value) => {
                                let jawaban = this.state.jawaban;
                                jawaban[9] = value;
                                this.setState({jawaban:jawaban})
                            }}
                            />
                    </View>


                    <View>
                        <View style={{flexDirection: "row", marginTop: 10, flexWrap: 'wrap'}}>
                            <Text style={style.textSoalNomer}>11. </Text>
                            <Text style={style.textSoal}>Radiasi sebenarnya boleh dilakukan apabila mematuhi prinsip-prinsip yang telah ditentukan oleh lembaga yang berwenang. Salah satu prinsip yang harus dipatuhi tersebut adalah?</Text>
                        </View>
                        <RadioForm
                            radio_props={dataJawaban[10]}
                            initial={this.state.jawaban[10]}
                            style={{margin:10, width: '90%'}}
                            onPress={(value) => {
                                let jawaban = this.state.jawaban;
                                jawaban[10] = value;
                                this.setState({jawaban:jawaban})
                            }}
                            />
                    </View>

                    <View>
                        <View style={{flexDirection: "row", marginTop: 10, flexWrap: 'wrap'}}>
                            <Text style={style.textSoalNomer}>12. </Text>
                            <Text style={style.textSoal}>Beberapa hal berikut yang bukan merupakan efek kebisingan akut dari kebisingan adalah?</Text>
                        </View>
                        <RadioForm
                            radio_props={dataJawaban[11]}
                            initial={this.state.jawaban[11]}
                            style={{margin:10, width: '90%'}}
                            onPress={(value) => {
                                let jawaban = this.state.jawaban;
                                jawaban[11] = value;
                                this.setState({jawaban:jawaban})
                            }}
                            />
                    </View>


                    <View>
                        <View style={{flexDirection: "row", marginTop: 10, flexWrap: 'wrap'}}>
                            <Text style={style.textSoalNomer}>13. </Text>
                            <Text style={style.textSoal}>Alat Pelindung Diri (APD) yang digunakan untuk melindungi kulit adalah?</Text>
                        </View>
                        <RadioForm
                            radio_props={dataJawaban[12]}
                            initial={this.state.jawaban[12]}
                            style={{margin:10, width: '90%'}}
                            onPress={(value) => {
                                let jawaban = this.state.jawaban;
                                jawaban[12] = value;
                                this.setState({jawaban:jawaban})
                            }}
                            />
                    </View>


                    <View>
                        <View style={{flexDirection: "row", marginTop: 10, flexWrap: 'wrap'}}>
                            <Text style={style.textSoalNomer}>14. </Text>
                            <Text style={style.textSoal}>Alat Pelindung Diri (APD) yang digunakan untuk melindungi telinga adalah?</Text>
                        </View>
                        <RadioForm
                            radio_props={dataJawaban[13]}
                            initial={this.state.jawaban[13]}
                            style={{margin:10, width: '90%'}}
                            onPress={(value) => {
                                let jawaban = this.state.jawaban;
                                jawaban[13] = value;
                                this.setState({jawaban:jawaban})
                            }}
                            />
                    </View>


                    <View>
                        <View style={{flexDirection: "row", marginTop: 10, flexWrap: 'wrap'}}>
                            <Text style={style.textSoalNomer}>15. </Text>
                            <Text style={style.textSoal}>Alat Pelindung Diri (APD) yang digunakan untuk melindungi pernapasan adalah?</Text>
                        </View>
                        <RadioForm
                            radio_props={dataJawaban[14]}
                            initial={this.state.jawaban[14]}
                            style={{margin:10, width: '90%'}}
                            onPress={(value) => {
                                let jawaban = this.state.jawaban;
                                jawaban[14] = value;
                                this.setState({jawaban:jawaban})
                            }}
                            />
                    </View>


                    <View>
                        <View style={{flexDirection: "row", marginTop: 10, flexWrap: 'wrap'}}>
                            <Text style={style.textSoalNomer}>16. </Text>
                            <Text style={style.textSoal}>Alat Pelindung Diri (APD) yang digunakan untuk melindungi kaki adalah?</Text>
                        </View>
                        <RadioForm
                            radio_props={dataJawaban[15]}
                            initial={this.state.jawaban[15]}
                            style={{margin:10, width: '90%'}}
                            onPress={(value) => {
                                let jawaban = this.state.jawaban;
                                jawaban[15] = value;
                                this.setState({jawaban:jawaban})
                            }}
                            />
                    </View>


                    <View>
                        <View style={{flexDirection: "row", marginTop: 10, flexWrap: 'wrap'}}>
                            <Text style={style.textSoalNomer}>17. </Text>
                            <Text style={style.textSoal}>Alat Pelindung Diri (APD) yang digunakan untuk melindungi mata adalah?</Text>
                        </View>
                        <RadioForm
                            radio_props={dataJawaban[16]}
                            initial={this.state.jawaban[16]}
                            style={{margin:10, width: '90%'}}
                            onPress={(value) => {
                                let jawaban = this.state.jawaban;
                                jawaban[16] = value;
                                this.setState({jawaban:jawaban})
                            }}
                            />
                    </View>


                    <View>
                        <View style={{flexDirection: "row", marginTop: 10, flexWrap: 'wrap'}}>
                            <Text style={style.textSoalNomer}>18. </Text>
                            <Text style={style.textSoal}>Beberapa hal berikut yang tidak termasuk dalam langkah pengukuran faktor bahaya ditempat kerja, yaitu?</Text>
                        </View>
                        <RadioForm
                            radio_props={dataJawaban[17]}
                            initial={this.state.jawaban[17]}
                            style={{margin:10, width: '90%'}}
                            onPress={(value) => {
                                let jawaban = this.state.jawaban;
                                jawaban[17] = value;
                                this.setState({jawaban:jawaban})
                            }}
                            />
                    </View>


                    <View>
                        <View style={{flexDirection: "row", marginTop: 10, flexWrap: 'wrap'}}>
                            <Text style={style.textSoalNomer}>19. </Text>
                            <Text style={style.textSoal}>Berapakah jam kerja ideal agar beban kerja tidak melebihi batas kemampuan tenaga kerja?</Text>
                        </View>
                        <RadioForm
                            radio_props={dataJawaban[18]}
                            initial={this.state.jawaban[18]}
                            style={{margin:10, width: '90%'}}
                            onPress={(value) => {
                                let jawaban = this.state.jawaban;
                                jawaban[18] = value;
                                this.setState({jawaban:jawaban})
                            }}
                            />
                    </View>


                    <View>
                        <View style={{flexDirection: "row", marginTop: 10, flexWrap: 'wrap'}}>
                            <Text style={style.textSoalNomer}>20. </Text>
                            <Text style={style.textSoal}>Apa akibat yang ditimbulkan dari stress yang ditimbulkan di tempat kerja?</Text>
                        </View>
                        <RadioForm
                            radio_props={dataJawaban[19]}
                            initial={this.state.jawaban[19]}
                            style={{margin:10, width: '90%'}}
                            onPress={(value) => {
                                let jawaban = this.state.jawaban;
                                jawaban[19] = value;
                                this.setState({jawaban:jawaban})
                            }}
                            />
                    </View>
                    

                    
                </ScrollView>
            )
        }else if(this.state.idMateri == 6) {
            var dataJawaban = [
                //1
                [
                    {label: 'Menyelamatkan nyawa korban.', value: 0 },
                    {label: 'Menekan kerugian perusahaan.', value: 1 },
                    {label: 'Mencegah cedera lebih parah.', value: 2 },
                    {label: 'Mencari pertolongan yang lebih lanjut.', value: 3 },
                    {label: 'Meringankan penderitaan korban.', value: 4 },
                ],
                //2
                [
                    {label: 'Pekerja yang telah mendapatkan training/latihan tentang P3K.', value: 0 },
                    {label: 'Pekerja yang berani. ', value: 1 },
                    {label: 'Pekerja yang sudah lama masa kerjanya.', value: 2 },
                    {label: 'Pekerja yang masih muda.', value: 3 },
                    {label: 'Pekerja yang jarak tempat tinggalnya dekat dengan perusahaan.', value: 4 },
                ],
                //3
                [
                    {label: 'Menyebabkan keributan di tempat kerja.', value: 0 },
                    {label: 'Meningkatnya angka kecelakaan kerja. ', value: 1 },
                    {label: 'Memperburuk akibat kecelakaan bahkan kematian.', value: 2 },
                    {label: 'Mengurangi penderitaan korban.', value: 3 },
                    {label: 'Korban merasa tidak diperhatikan.', value: 4 },
                ],
                //4
                [
                    {label: 'Bersikap tenang.', value: 0 },
                    {label: 'Berani. ', value: 1 },
                    {label: 'Cerdas.', value: 2 },
                    {label: 'Cekatan.', value: 3 },
                    {label: 'Tanggung jawab.', value: 4 },
                ],
                //5
                [
                    {label: 'Tekanan darah tinggi.', value: 0 },
                    {label: 'Luka sayatan. ', value: 1 },
                    {label: 'Pendarahan ringan.', value: 2 },
                    {label: 'Asam lambung naik.', value: 3 },
                    {label: 'Tingkat kolesterol tinggi.', value: 4 },
                ],
                //6
                [
                    {label: 'Sarung tangan.', value: 0 },
                    {label: 'Pisau kecil. ', value: 1 },
                    {label: 'Thermometer.', value: 2 },
                    {label: 'Tensimeter.', value: 3 },
                    {label: 'Alkohol atau cairan pembersih luka lainnya.', value: 4 },
                ],
                //7
                [
                    {label: 'Antibiotik.', value: 0 },
                    {label: 'Obat analgesik. ', value: 1 },
                    {label: 'Obat tekanan darah tinggi.', value: 2 },
                    {label: 'Antidepresi.', value: 3 },
                    {label: 'Obat pilek.', value: 4 },
                ],
                //8
                [
                    {label: 'Bersikap panik.', value: 0 },
                    {label: 'Merasa takut. ', value: 1 },
                    {label: 'Melakukan pertolongan sesuai hasil diagnosa.', value: 2 },
                    {label: 'Langsung membawa korban ke rumah sakit/puskesmas.', value: 3 },
                    {label: 'Memanggil bantuan polisi.', value: 4 },
                ],
                //9
                [
                    {label: 'Bersih dan steril.', value: 0 },
                    {label: 'Ditempat kecelakaan berlangsung. ', value: 1 },
                    {label: 'Ditempat aman dan nyaman bagi korban.', value: 2 },
                    {label: 'Tempat yang jauh dari kebisingan.', value: 3 },
                    {label: 'Tempat yang datar.', value: 4 },
                ],
                //10
                [
                    {label: 'Memberikan minum kepada korban.', value: 0 },
                    {label: 'Memberikan oksigen kepada korban. ', value: 1 },
                    {label: 'Membaringkan korban.', value: 2 },
                    {label: 'Membawa korban ke rumah sakit/puskesmas.', value: 3 },
                    {label: 'Membiarkan korban di tempat kecelakaan.', value: 4 },
                ],
                //11
                [
                    {label: 'Mual dan muntah.', value: 0 },
                    {label: 'Nadi cepat dan tidak teratur. ', value: 1 },
                    {label: 'Keringat dingin.', value: 2 },
                    {label: 'Biji mata melebar.', value: 3 },
                    {label: 'Sesak nafas atau berbunyi saat menarik nafas.', value: 4 },
                ],
                //12
                [
                    {label: 'Melonggarkan pakaian korban.', value: 0 },
                    {label: 'Memberikan obat antibiotik. ', value: 1 },
                    {label: 'Mengukur tekanan darah korban.', value: 2 },
                    {label: 'Menyelimuti korban.', value: 3 },
                    {label: 'Melakukan tindakan nafas buatan.', value: 4 },
                ],
                //13
                [
                    {label: 'Menggunakan teknik yang baik dan benar.', value: 0 },
                    {label: 'Evakuasi dilakukan jika ada teman. ', value: 1 },
                    {label: 'Evakuasi dilakukan oleh orang medis.', value: 2 },
                    {label: 'Evakuasi dilakukan tidak secepat mungkin.', value: 3 },
                    {label: 'Evakuasi menunggu orang yang datang dari rumah sakit.', value: 4 },
                ],
                //14
                [
                    {label: 'Jumlah pekerja dan potensi kecelakaan.', value: 0 },
                    {label: 'Jarak dari tempat kerja ke rumah sakit. ', value: 1 },
                    {label: 'Tingkat keparahan kecelakaan.', value: 2 },
                    {label: 'Intensitas kecelakaan.', value: 3 },
                    {label: 'Situasi tempat kerja.', value: 4 },
                ],
                //15
                [
                    {label: 'Muda.', value: 0 },
                    {label: 'Masa kerja >1tahun. ', value: 1 },
                    {label: 'Berbadan sehat dan bersedia.', value: 2 },
                    {label: 'Pekerja yang berdomisili di tempat kerja.', value: 3 },
                    {label: 'Pekerja yang memiliki wawasan luas.', value: 4 },
                ],
                //16
                [
                    {label: 'Dasar-dasar P3K ditempat kerja.', value: 0 },
                    {label: 'Farmasikologi. ', value: 1 },
                    {label: 'Epidemiologi.', value: 2 },
                    {label: 'Alat Pelindung Diri.', value: 3 },
                    {label: 'Program kesehatan kerja.', value: 4 },
                ],
                //17
                [
                    {label: 'Memastikan semua pekerja dalam keadaan aman.', value: 0 },
                    {label: 'Membawa korban ke rumah sakit/puskesmas. ', value: 1 },
                    {label: 'Melaksanakan tindakan pertolongan pertama di setiap kecelakaan.', value: 2 },
                    {label: 'Selalu berada di tempat kerja.', value: 3 },
                    {label: 'Membantu petugas medis.', value: 4 },
                ],
                //18
                [
                    {label: 'Luas.', value: 0 },
                    {label: 'Tempat steril. ', value: 1 },
                    {label: 'Tersembunyi.', value: 2 },
                    {label: 'Bangunan paling depan perusahaan.', value: 3 },
                    {label: 'Lokasi harus strategis.', value: 4 },
                ],
                //19
                [
                    {label: 'Timbangan.', value: 0 },
                    {label: 'Kotak P3K dan isinya. ', value: 1 },
                    {label: 'APAR.', value: 2 },
                    {label: 'Kursi roda.', value: 3 },
                    {label: 'Alat radiologi.', value: 4 },
                ],
                //20
                [
                    {label: 'Kotak P3K harus kuat, mudah dipindah, dan diberi label P3K.', value: 0 },
                    {label: 'Ukuran kotak kecil sehingga mudah di bawa. ', value: 1 },
                    {label: 'Hanya berisi obat-obatan.', value: 2 },
                    {label: 'Harus terdapat kaca.', value: 3 },
                    {label: 'Berwarna mencolok/terang.', value: 4 },
                ]
            ]
            return (
                <ScrollView style={{padding: 10}}>
                    <Header />
                    <View style={{backgroundColor: "#9b59b6", marginTop: 50, padding: 10, borderRadius: 10}}>
                        <Text style={{fontFamily: "Roboto-Bold", color: "white",  fontSize: 18, alignSelf: "center"}}>{"Soal Kuis dan Jawaban".toUpperCase()}</Text>
                        <Text style={{fontFamily: "Roboto", color: "white",  fontSize: 16, alignSelf: "center", textAlign: "center", marginTop: 10}}>{"Mengelola Pertolongan Pertama pada Kecelakaan Kerja (P3K) di Tempat Kerja.".toUpperCase()}</Text>
                    </View>
                    {/* Soal */}

                   

                    
                    <View>
                        <View style={{flexDirection: "row", marginTop: 10, flexWrap: 'wrap'}}>
                            <Text style={style.textSoalNomer}>1. </Text>
                            <Text style={style.textSoal}>Beberapa persyaratan berikut yang bukan merupakan tujuan adanya P3K ditempat kerja adalah?</Text>
                        </View>
                        <RadioForm
                            radio_props={dataJawaban[0]}
                            initial={this.state.jawaban[0]}
                            style={{margin:10, width: '90%'}}
                            onPress={(value) => {
                                let jawaban = this.state.jawaban;
                                jawaban[0] = value;
                                this.setState({jawaban:jawaban})
                            }}
                            />
                    </View>

                    <View>
                        <View style={{flexDirection: "row", marginTop: 10, flexWrap: 'wrap'}}>
                            <Text style={style.textSoalNomer}>2. </Text>
                            <Text style={style.textSoal}>Kualifikasi pekerja yang ditunjuk perusahaan sebagai petugas P3K di tempat kerja adalah?</Text>
                        </View>
                        <RadioForm
                            radio_props={dataJawaban[1]}
                            initial={this.state.jawaban[1]}
                            style={{margin:10, width: '90%'}}
                            onPress={(value) => {
                                let jawaban = this.state.jawaban;
                                jawaban[1] = value;
                                this.setState({jawaban:jawaban})
                            }}
                            />
                    </View>

                    <View>
                        <View style={{flexDirection: "row", marginTop: 10, flexWrap: 'wrap'}}>
                            <Text style={style.textSoalNomer}>3. </Text>
                            <Text style={style.textSoal}>Apa akibat dari tindakan pertolongan pertama yang tidak dilakukan dengan cepat dan tepat?</Text>
                        </View>
                        <RadioForm
                            radio_props={dataJawaban[2]}
                            initial={this.state.jawaban[2]}
                            style={{margin:10, width: '90%'}}
                            onPress={(value) => {
                                let jawaban = this.state.jawaban;
                                jawaban[2] = value;
                                this.setState({jawaban:jawaban})
                            }}
                            />
                    </View>

                    <View>
                        <View style={{flexDirection: "row", marginTop: 10, flexWrap: 'wrap'}}>
                            <Text style={style.textSoalNomer}>4. </Text>
                            <Text style={style.textSoal}>Prinsip yang harus ditanamkan pada jiwa petugas P3K adalah?</Text>
                        </View>
                        <RadioForm
                            radio_props={dataJawaban[3]}
                            initial={this.state.jawaban[3]}
                            style={{margin:10, width: '90%'}}
                            onPress={(value) => {
                                let jawaban = this.state.jawaban;
                                jawaban[3] = value;
                                this.setState({jawaban:jawaban})
                            }}
                            />
                    </View>

                    <View>
                        <View style={{flexDirection: "row", marginTop: 10, flexWrap: 'wrap'}}>
                            <Text style={style.textSoalNomer}>5. </Text>
                            <Text style={style.textSoal}>Salah satu jenis kecelakaan kerja yang menjadi prioritas adalah?</Text>
                        </View>
                        <RadioForm
                            radio_props={dataJawaban[4]}
                            initial={this.state.jawaban[4]}
                            style={{margin:10, width: '90%'}}
                            onPress={(value) => {
                                let jawaban = this.state.jawaban;
                                jawaban[4] = value;
                                this.setState({jawaban:jawaban})
                            }}
                            />
                    </View>

                    <View>
                        <View style={{flexDirection: "row", marginTop: 10, flexWrap: 'wrap'}}>
                            <Text style={style.textSoalNomer}>6. </Text>
                            <Text style={style.textSoal}>Dibawah ini yang merupakan salah satu alat dan bahan P3K untuk diperusahaan adalah?</Text>
                        </View>
                        <RadioForm
                            radio_props={dataJawaban[5]}
                            initial={this.state.jawaban[5]}
                            style={{margin:10, width: '90%'}}
                            onPress={(value) => {
                                let jawaban = this.state.jawaban;
                                jawaban[5] = value;
                                this.setState({jawaban:jawaban})
                            }}
                            />
                    </View>



                    <View>
                        <View style={{flexDirection: "row", marginTop: 10, flexWrap: 'wrap'}}>
                            <Text style={style.textSoalNomer}>7. </Text>
                            <Text style={style.textSoal}>Salah satu obat yang harus ada dikotak P3K adalah?</Text>
                        </View>
                        <RadioForm
                            radio_props={dataJawaban[6]}
                            initial={this.state.jawaban[6]}
                            style={{margin:10, width: '90%'}}
                            onPress={(value) => {
                                let jawaban = this.state.jawaban;
                                jawaban[6] = value;
                                this.setState({jawaban:jawaban})
                            }}
                            />
                    </View>


                    <View>
                        <View style={{flexDirection: "row", marginTop: 10, flexWrap: 'wrap'}}>
                            <Text style={style.textSoalNomer}>8. </Text>
                            <Text style={style.textSoal}>Tindakan pertama yang harus dilakukan petugas P3K saat menemukan korban adalah?</Text>
                        </View>
                        <RadioForm
                            radio_props={dataJawaban[7]}
                            initial={this.state.jawaban[7]}
                            style={{margin:10, width: '90%'}}
                            onPress={(value) => {
                                let jawaban = this.state.jawaban;
                                jawaban[7] = value;
                                this.setState({jawaban:jawaban})
                            }}
                            />
                    </View>

                    <View>
                        <View style={{flexDirection: "row", marginTop: 10, flexWrap: 'wrap'}}>
                            <Text style={style.textSoalNomer}>9. </Text>
                            <Text style={style.textSoal}>Pertolongan pertama sebaiknya dilakukan ditempat yang sesuai, yaitu?</Text>
                        </View>
                        <RadioForm
                            radio_props={dataJawaban[8]}
                            initial={this.state.jawaban[8]}
                            style={{margin:10, width: '90%'}}
                            onPress={(value) => {
                                let jawaban = this.state.jawaban;
                                jawaban[8] = value;
                                this.setState({jawaban:jawaban})
                            }}
                            />
                    </View>

                    <View>
                        <View style={{flexDirection: "row", marginTop: 10, flexWrap: 'wrap'}}>
                            <Text style={style.textSoalNomer}>10. </Text>
                            <Text style={style.textSoal}>Pertolongan pertama apa yang harus dilakukan saat ada pekerja yang menghirup gas beracun adalah?</Text>
                        </View>
                        <RadioForm
                            radio_props={dataJawaban[9]}
                            initial={this.state.jawaban[9]}
                            style={{margin:10, width: '90%'}}
                            onPress={(value) => {
                                let jawaban = this.state.jawaban;
                                jawaban[9] = value;
                                this.setState({jawaban:jawaban})
                            }}
                            />
                    </View>


                    <View>
                        <View style={{flexDirection: "row", marginTop: 10, flexWrap: 'wrap'}}>
                            <Text style={style.textSoalNomer}>11. </Text>
                            <Text style={style.textSoal}>Gejala asma atau penyempitan saluran pernapasan adalah?</Text>
                        </View>
                        <RadioForm
                            radio_props={dataJawaban[10]}
                            initial={this.state.jawaban[10]}
                            style={{margin:10, width: '90%'}}
                            onPress={(value) => {
                                let jawaban = this.state.jawaban;
                                jawaban[10] = value;
                                this.setState({jawaban:jawaban})
                            }}
                            />
                    </View>

                    <View>
                        <View style={{flexDirection: "row", marginTop: 10, flexWrap: 'wrap'}}>
                            <Text style={style.textSoalNomer}>12. </Text>
                            <Text style={style.textSoal}>Tindakan yang tepat untuk korban yang mengalami pingsan adalah?</Text>
                        </View>
                        <RadioForm
                            radio_props={dataJawaban[11]}
                            initial={this.state.jawaban[11]}
                            style={{margin:10, width: '90%'}}
                            onPress={(value) => {
                                let jawaban = this.state.jawaban;
                                jawaban[11] = value;
                                this.setState({jawaban:jawaban})
                            }}
                            />
                    </View>


                    <View>
                        <View style={{flexDirection: "row", marginTop: 10, flexWrap: 'wrap'}}>
                            <Text style={style.textSoalNomer}>13. </Text>
                            <Text style={style.textSoal}>Prinsip dasar dalam melakukan evakuasi korban kecelakaan adalah?</Text>
                        </View>
                        <RadioForm
                            radio_props={dataJawaban[12]}
                            initial={this.state.jawaban[12]}
                            style={{margin:10, width: '90%'}}
                            onPress={(value) => {
                                let jawaban = this.state.jawaban;
                                jawaban[12] = value;
                                this.setState({jawaban:jawaban})
                            }}
                            />
                    </View>


                    <View>
                        <View style={{flexDirection: "row", marginTop: 10, flexWrap: 'wrap'}}>
                            <Text style={style.textSoalNomer}>14. </Text>
                            <Text style={style.textSoal}>Jumlah petugas P3K disetiap perusahaan ditentukan oleh?</Text>
                        </View>
                        <RadioForm
                            radio_props={dataJawaban[13]}
                            initial={this.state.jawaban[13]}
                            style={{margin:10, width: '90%'}}
                            onPress={(value) => {
                                let jawaban = this.state.jawaban;
                                jawaban[13] = value;
                                this.setState({jawaban:jawaban})
                            }}
                            />
                    </View>


                    <View>
                        <View style={{flexDirection: "row", marginTop: 10, flexWrap: 'wrap'}}>
                            <Text style={style.textSoalNomer}>15. </Text>
                            <Text style={style.textSoal}>Syarat yang harus dipenuhi oleh petugas P3K adalah?</Text>
                        </View>
                        <RadioForm
                            radio_props={dataJawaban[14]}
                            initial={this.state.jawaban[14]}
                            style={{margin:10, width: '90%'}}
                            onPress={(value) => {
                                let jawaban = this.state.jawaban;
                                jawaban[14] = value;
                                this.setState({jawaban:jawaban})
                            }}
                            />
                    </View>


                    <View>
                        <View style={{flexDirection: "row", marginTop: 10, flexWrap: 'wrap'}}>
                            <Text style={style.textSoalNomer}>16. </Text>
                            <Text style={style.textSoal}>Materi yang harus ada didalam proses training petugas P3K adalah?</Text>
                        </View>
                        <RadioForm
                            radio_props={dataJawaban[15]}
                            initial={this.state.jawaban[15]}
                            style={{margin:10, width: '90%'}}
                            onPress={(value) => {
                                let jawaban = this.state.jawaban;
                                jawaban[15] = value;
                                this.setState({jawaban:jawaban})
                            }}
                            />
                    </View>


                    <View>
                        <View style={{flexDirection: "row", marginTop: 10, flexWrap: 'wrap'}}>
                            <Text style={style.textSoalNomer}>17. </Text>
                            <Text style={style.textSoal}>Tugas dan tanggung jawab petugas P3K di tempat kerja adalah?</Text>
                        </View>
                        <RadioForm
                            radio_props={dataJawaban[16]}
                            initial={this.state.jawaban[16]}
                            style={{margin:10, width: '90%'}}
                            onPress={(value) => {
                                let jawaban = this.state.jawaban;
                                jawaban[16] = value;
                                this.setState({jawaban:jawaban})
                            }}
                            />
                    </View>


                    <View>
                        <View style={{flexDirection: "row", marginTop: 10, flexWrap: 'wrap'}}>
                            <Text style={style.textSoalNomer}>18. </Text>
                            <Text style={style.textSoal}>Apa yang harus diperhatikan dalam memilih lokasi ruang P3K?</Text>
                        </View>
                        <RadioForm
                            radio_props={dataJawaban[17]}
                            initial={this.state.jawaban[17]}
                            style={{margin:10, width: '90%'}}
                            onPress={(value) => {
                                let jawaban = this.state.jawaban;
                                jawaban[17] = value;
                                this.setState({jawaban:jawaban})
                            }}
                            />
                    </View>


                    <View>
                        <View style={{flexDirection: "row", marginTop: 10, flexWrap: 'wrap'}}>
                            <Text style={style.textSoalNomer}>19. </Text>
                            <Text style={style.textSoal}>Fasilitas yang harus ada dalam ruang P3K adalah?</Text>
                        </View>
                        <RadioForm
                            radio_props={dataJawaban[18]}
                            initial={this.state.jawaban[18]}
                            style={{margin:10, width: '90%'}}
                            onPress={(value) => {
                                let jawaban = this.state.jawaban;
                                jawaban[18] = value;
                                this.setState({jawaban:jawaban})
                            }}
                            />
                    </View>


                    <View>
                        <View style={{flexDirection: "row", marginTop: 10, flexWrap: 'wrap'}}>
                            <Text style={style.textSoalNomer}>20. </Text>
                            <Text style={style.textSoal}>Apa yang harus diperhatikan dalam pengadaan kotak P3K?</Text>
                        </View>
                        <RadioForm
                            radio_props={dataJawaban[19]}
                            initial={this.state.jawaban[19]}
                            style={{margin:10, width: '90%'}}
                            onPress={(value) => {
                                let jawaban = this.state.jawaban;
                                jawaban[19] = value;
                                this.setState({jawaban:jawaban})
                            }}
                            />
                    </View>
                    

                    
                </ScrollView>
            )
        }
        
        else {
            return (<Text>ID yang dituju tidak ada</Text>)
        }
    }
}

const style = StyleSheet.create({
    textSoal: {
        fontFamily: "Roboto-Medium",
        fontSize: 15,
        color:"black",
        width: "90%",
    },
    textSoalNomer: {
        fontFamily: "Roboto-Medium",
        fontSize: 15,
        color:"black",
        width: "7%",
    },
    input: {
        height: 40,
        borderWidth: 1,
        padding: 10,
        color:"black"
      },
})