import React, { Component } from 'react'
import { View, Text, StyleSheet,ScrollView, ActivityIndicator, Image, ImageBackground, TouchableOpacity } from 'react-native'
import Header from "../component/header.js"
import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome'
import { faStickyNote, faArrowRight, faPen, faInfo, faNewspaper } from '@fortawesome/free-solid-svg-icons'

export default class menuMateri extends Component {



    render() {
        return (
            <ScrollView style={{backgroundColor: "white", height: "100%"}}>
                <Header></Header>
                <View style={style.titleContainerStyle}>
                    <FontAwesomeIcon icon={faStickyNote} color={"#9b59b6"} size={32} />
                    <Text style={style.textTitle}>Materi K3</Text>
                </View>
                <View style={style.container}>
                    <View style={style.list}>
                        <View style={style.containerLeft}>
                            <Text style={style.containerLeftText}>1</Text>
                        </View>
                        <View style={{width: "80%"}}>
                            <Text style={style.listText}>{"Merancang Strategi Pengendalian Risiko K3 di Tempat Kerja".toUpperCase()}</Text>
                            <Text style={style.listTextSubTitle}>{"M.71KKK01.001.1".toUpperCase()}</Text>
                            <TouchableOpacity style={style.buttonLihatSelengkapnya}
                                onPress={() => {
                                    this.props.navigation.navigate("SubMateriMenu", {id: 1, judul: "Merancang Strategi Pengendalian Risiko K3 di Tempat Kerja"})
                                }}
                            >
                                <Text style={{color: "white", fontFamily: "Roboto"}}>Sub Materi</Text>
                            </TouchableOpacity>
                        </View>
                    </View>

                    <View style={style.list}>
                        <View style={style.containerLeft}>
                            <Text style={style.containerLeftText}>2</Text>
                        </View>
                        <View style={{width: "80%"}}>
                            <Text style={style.listText}>{"Merancang Sistem Tanggap Darurat".toUpperCase()}</Text>
                            <Text style={style.listTextSubTitle}>{"M.71KKK01.002.1".toUpperCase()}</Text>
                            <TouchableOpacity style={style.buttonLihatSelengkapnya}
                                onPress={() => {
                                    this.props.navigation.navigate("SubMateriMenu", {id: 2, judul: "Merancang Sistem Tanggap Darurat"})
                                }}
                            >
                                <Text style={{color: "white", fontFamily: "Roboto"}}>Sub Materi</Text>
                            </TouchableOpacity>
                        </View>
                    </View>

                    <View style={style.list}>
                        <View style={style.containerLeft}>
                            <Text style={style.containerLeftText}>3</Text>
                        </View>
                        <View style={{width: "80%"}}>
                            <Text style={style.listText}>{"Melakukan Komunikasi K3.".toUpperCase()}</Text>
                            <Text style={style.listTextSubTitle}>{"M.71KKK01.003.1".toUpperCase()}</Text>
                            <TouchableOpacity style={style.buttonLihatSelengkapnya}
                                onPress={() => {
                                    this.props.navigation.navigate("SubMateriMenu", {id: 3, judul: "Melakukan Komunikasi K3"})
                                }}
                            >
                                <Text style={{color: "white", fontFamily: "Roboto"}}>Sub Materi</Text>
                            </TouchableOpacity>
                        </View>
                    </View>


                    <View style={style.list}>
                        <View style={style.containerLeft}>
                            <Text style={style.containerLeftText}>4</Text>
                        </View>
                        <View style={{width: "80%"}}>
                            <Text style={style.listText}>{"Mengawasi Pelaksanaan Izin Kerja.".toUpperCase()}</Text>
                            <Text style={style.listTextSubTitle}>{"M.71KKK01.004.1".toUpperCase()}</Text>
                            <TouchableOpacity style={style.buttonLihatSelengkapnya}
                                onPress={() => {
                                    this.props.navigation.navigate("SubMateriMenu", {id: 4, judul: "Mengawasi Pelaksanaan Izin Kerja."})
                                }}
                            >
                                <Text style={{color: "white", fontFamily: "Roboto"}}>Sub Materi</Text>
                            </TouchableOpacity>
                        </View>
                    </View>

                    <View style={style.list}>
                        <View style={style.containerLeft}>
                            <Text style={style.containerLeftText}>5</Text>
                        </View>
                        <View style={{width: "80%"}}>
                            <Text style={style.listText}>{"Melakukan Pengukuran Faktor Bahaya di Tempat Kerja.".toUpperCase()}</Text>
                            <Text style={style.listTextSubTitle}>{"M.71KKK01.005.1".toUpperCase()}</Text>
                            <TouchableOpacity style={style.buttonLihatSelengkapnya}
                                onPress={() => {
                                    this.props.navigation.navigate("SubMateriMenu", {id: 5, judul: "Melakukan Pengukuran Faktor Bahaya di Tempat Kerja."})
                                }}
                            >
                                <Text style={{color: "white", fontFamily: "Roboto"}}>Sub Materi</Text>
                            </TouchableOpacity>
                        </View>
                    </View>

                    <View style={style.list}>
                        <View style={style.containerLeft}>
                            <Text style={style.containerLeftText}>6</Text>
                        </View>
                        <View style={{width: "80%"}}>
                            <Text style={style.listText}>{"Mengelola Pertolongan Pertama pada Kecelakaan Kerja (P3K) di Tempat Kerja.".toUpperCase()}</Text>
                            <Text style={style.listTextSubTitle}>{"M.71KKK01.006.1".toUpperCase()}</Text>
                            <TouchableOpacity style={style.buttonLihatSelengkapnya}
                                onPress={() => {
                                    this.props.navigation.navigate("SubMateriMenu", {id: 6, judul: "Mengelola Pertolongan Pertama pada Kecelakaan Kerja (P3K) di Tempat Kerja."})
                                }}
                            >
                                <Text style={{color: "white", fontFamily: "Roboto"}}>Sub Materi</Text>
                            </TouchableOpacity>
                        </View>
                    </View>
                </View>
            </ScrollView>
        
        )
    }
}

const style = StyleSheet.create({
    container: {
        width: "100%",
        alignSelf: "center",
    },
    titleContainerStyle: {
        flexDirection: "row",
        marginTop: 50,
        marginBottom: 20,
        marginLeft: 10
    },
    textTitle: {
        fontSize: 20,
        color: "black",
        fontFamily: "Roboto-Medium",
        marginLeft: 5,
        color: "#9b59b6"
    },
    containerLeftText: {
        fontFamily: "Roboto-Bold",
        fontSize: 50,
    },
    containerLeft: {
        backgroundColor: "#9b59b6",
        padding: 10,
        width: "20%",
        height: "100%",
        alignItems:"center",
        borderTopLeftRadius:5,
        borderBottomLeftRadius:5
    },
    list: {
        alignItems: "center",
        flexDirection: "row",
        borderRadius: 10,
        margin: 10,
        borderWidth: 1,
        height: 200
    },
    listText: {
        fontSize: 16,
        marginLeft: 10,
        marginTop: 10,
        marginRight: 5,
        fontFamily: "Roboto-Bold",
        color: "#9b59b6",
        flex: 1,
        flexWrap: "wrap",
        marginBottom: 0
    },
    listTextSubTitle: {
        fontSize: 15,
        marginLeft: 10,
        fontFamily: "Roboto",
        color: "#9b59b6",
        flex: 1,
        flexWrap: "wrap"
    },
    listArrow: {
        position: "absolute",
        right: 15
    },
    buttonLihatSelengkapnya: {
        backgroundColor: "#9b59b6",
        width: "40%",
        padding: 8,
        borderRadius: 10,
        marginLeft: 10,
        alignItems: 'center',
        marginTop: 20,
        marginBottom: 10
    }
})
