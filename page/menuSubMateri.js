import React, { Component } from 'react'
import { View, Text, StyleSheet,ScrollView, ActivityIndicator, Image, ImageBackground, TouchableOpacity } from 'react-native'
import Header from "../component/header.js"
import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome'
import { faStickyNote, faArrowRight, faPen, faInfo, faNewspaper } from '@fortawesome/free-solid-svg-icons'

export default class menuSubMateri extends Component {

    constructor(props) {
        super();
        this.state = {
            id_materi: 0,
            judul: '',
            data: [],
        }
    }

    componentDidMount() {
        let id_materi = this.props.route.params.id;
        let title_materi = this.props.route.params.judul;
        let data = [];
        let url = [];
        if(id_materi === 1) {
            data = [
                "Merencanakan pengendalian risiko K3 di tempat kerja",
                "Merancang pengendalian risiko K3 di tempat kerja sesuai hirarki",
                "Meninjau kembali rancangan pengendalian risiko K3 di tempat kerja",
                "Melaporkan hasil rancangan pengendalian risiko K3",
            ]
            url = [
                "https://docs.google.com/presentation/d/e/2PACX-1vTKfrh_sXS1MlQ9OcxErOMPskrdLn3Jq_IHkcRA-ZxL96oG1MVSC-qvDgt6bTomVA/embed?start=false&loop=false",
                "https://docs.google.com/presentation/d/e/2PACX-1vSk-Ik2ACdhb51QV8kr8sp0TtLwQ7_NdfjijURyFc4EDL50j3WM9L96rGNi8ayowA/embed?start=false&loop=false",
                "https://docs.google.com/presentation/d/e/2PACX-1vTqqv0oTf3pZcLBgs7sOGxpuUGH38snES8kJ5VFaY31tZZ7gUqAIAjacSEI1mo11g/pub?start=false&loop=false",
                "https://docs.google.com/presentation/d/e/2PACX-1vRS0cFpaSYcz-ix4q6oQM808BPYZ4atJa5mE3IjD1uB6vNSy7PkJ3r5PII-Ikb0pg/pub?start=false&loop=false",
            ]
        }else if(id_materi === 2) {
            data = [
                "Merencanakan sistem tanggap darurat",
                "Membuat rancangan sistem tanggap darurat ditempat kerja",
                "Meninjau kembali rancangan sistem tanggap darurat",
                "Melaporkan hasil rancangan sistem tanggap darurat",
            ]
            url = [
                "https://docs.google.com/presentation/d/e/2PACX-1vRmgPmFy7i5fiFb02pqdvUv6CPQBGerluR5I76Jfh_cKDv5y3oqw1dEdGLkViEEFg/pub?start=false&loop=false",
                "https://docs.google.com/presentation/d/e/2PACX-1vT_AGCX3z7Jgb371d14ulx1Nsi-vKbKW5KdQCo2MRIm-AMGeq-OPG_23ix-hPyUtg/pub?start=false&loop=false",
                "https://docs.google.com/presentation/d/e/2PACX-1vQlsQpkh8ztbgXe76grzZ7RbfNZangjdWGeZtr5JOtA9mn2iWeZCKEg10B38QCIow/pub?start=false&loop=false",
                "https://docs.google.com/presentation/d/e/2PACX-1vSYBhQmZ_cFMHZqU-VHRnaGTdi_IU_LIQ97mK75xLl6TMJMLJeTLkwictwlTChkjA/pub?start=false&loop=false",
            ]
        }else if(id_materi === 3) {
            data = [
                "Merencanakan proses kegiatan komunikasi K3",
                "Melaksanakan proses komunikasi  K3",
                "Memonitor pelaksanaan tindak lanjut hasil komunikasi K3",
                "Melaporkan kegiatan komunikasi K3",
            ]
            url = [
                "https://docs.google.com/presentation/d/e/2PACX-1vSJ5l1q_xFkzgaQwWAT6HY4uRhq3CEoKIk2_ScMWPKG0eDtGVP1DS3pNuiGddrecw/pub?start=false&loop=false",
                "https://docs.google.com/presentation/d/e/2PACX-1vSjxdpY6MZNuwLsTRmRfnQ5dG1QMPUaWGvW3H9xOxYIk3Gu6jVm3ZsrYcvfEnx-TQ/pub?start=false&loop=false",
                "https://docs.google.com/presentation/d/e/2PACX-1vSsDu4bWx5_hcEcyK3tT0UPSQJ4No2OUNyDa7bf4VJin9wpGoI8U4vF3XAuq6aQEQ/pub?start=false&loop=false",
                "https://docs.google.com/presentation/d/e/2PACX-1vSytEi7-kEUV0R_LSz5eKQw6B_XMRqXyqVjENfR9JrKtUT0PhsOfwWK4dDF6o4Wzw/pub?start=false&loop=false",
            ]
        }else if(id_materi === 4) {
            data = [
                "Mempersiapkan Izin Kerja",
                "Mengawasi penerapan izin kerja di tempat kerja",
                "Melaporkan hasil pengawasan izin kerja",
            ]
            url = [
                "https://docs.google.com/presentation/d/e/2PACX-1vSPJAWJA2A6jbNHNIva2-H2yf1qRpNwX4CZuEFjwX0SE8-nQiWGp9c14m6Mr3u5sg/pub?start=false&loop=false",
                "https://docs.google.com/presentation/d/e/2PACX-1vQ5865CYkgAI2jxhWq9nkQFYEzEN7CJUs8SiqnLfRZnKBK_4AoPot2FKn9pvRjW1w/pub?start=false&loop=false",
                "https://docs.google.com/presentation/d/e/2PACX-1vQP9GtXVMAgbfiz3Sp2EC5wDrVJ5LGCKys6xVhJNIfY_piIVy_NkaSzECqXORYj2A/pub?start=false&loop=false",
            ]
        }else if(id_materi === 5) {
            data = [
                "Mempersiapkan pengukuran faktor bahaya ditempat kerja",
                "Melaksanakan pengukuran faktor bahaya di tempat kerja",
                "Melaporkan hasil pengukuran faktor bahaya ditempat kerja",
            ]
            url = [
                "https://docs.google.com/presentation/d/e/2PACX-1vQB3oTDg31hpyG6C8GtFaKmwMysfmM59UglWM7FzBCxWUlfDZXzz47lMJ2j5q1GPA/pub?start=false&loop=false",
                "https://docs.google.com/presentation/d/e/2PACX-1vRMJLRdSvKQBJNhadrRO7H7dUoQ2nCVa1W5L-yEv2H_cnCYTPHiFNAwF6PYwpjEKg/pub?start=false&loop=false",
                "https://docs.google.com/presentation/d/e/2PACX-1vRkDAsHjFZwpCd95hWQqhYb5_jEgjuy7ykaGY6r__Ir3dV--p05B-8l59ZJnnn-bw/pub?start=false&loop=false"
            ]
        }else if(id_materi === 6) {
            data = [
                "Mempersiapkan pengelolaan P3K",
                "Melaksanakan pengelolaan P3K",
                "Membuat laporan pengelolaan P3K",
            ]
            url = [
                "https://docs.google.com/presentation/d/e/2PACX-1vSfYlqypHgolKzWFqcqyf3o--9CUe-_W7sNCl-PNZXjJW2AdcUuvFddkfixV_S1iQ/pub?start=false&loop=false",
                "https://docs.google.com/presentation/d/e/2PACX-1vTBN3FZt0O3kYyyA-Q6WEVDQHmN2VZSuepiL-IvXF5FPKjpNWiCCTxf3k7Jy4CY_Q/pub?start=false&loop=false",
                "https://docs.google.com/presentation/d/e/2PACX-1vQHhmaNX76ZzqHfkFZeu0EOA2Bg0-CbAZ_wK7womI1wo4ITJKED8CA3wlwgCzbpfQ/pub?start=false&loop=false",
            ]
        }
        this.setState({id_materi: id_materi, judul: title_materi, data: data, url: url});
    }

    


    render() {
        return (
            <ScrollView style={{backgroundColor: "white", height: "100%"}}>
                <Header></Header>
                <View style={style.titleContainerStyle}>
                    <FontAwesomeIcon icon={faStickyNote} color={"#9b59b6"} size={32} />
                    <Text style={style.textTitle}>Sub Materi {this.state.judul}</Text>
                    
                </View>
                <View style={style.container}>
                    {

                    this.state.data.map( (item, index) => (
                        <View style={style.list}>
                            <View style={style.containerLeft}>
                                <Text style={style.containerLeftText}>{index + 1}</Text>
                            </View>
                            <View style={{width: "80%"}}>
                                <Text style={style.listText}>{item.toUpperCase()}</Text>
                                <TouchableOpacity style={style.buttonLihatSelengkapnya}
                                    onPress={() => {
                                        this.props.navigation.navigate("DetailMateri", {id: index, url: this.state.url[index], judul: item})
                                    }}
                                >
                                    <Text style={{color: "white", fontFamily: "Roboto"}}>Lihat Materi</Text>
                                </TouchableOpacity>
                            </View>
                        </View>
                    ))
                    }

                   

                    
                </View>
            </ScrollView>
        
            
        )
    }
}

const style = StyleSheet.create({
    container: {
        width: "100%",
        alignSelf: "center",
    },
    titleContainerStyle: {
        flexDirection: "row",
        marginTop: 50,
        marginLeft: 10,
        width: "80%"
    },
    textTitle: {
        fontSize: 20,
        color: "black",
        fontFamily: "Roboto-Medium",
        marginLeft: 5,
        color: "#9b59b6",
        flexWrap: 'wrap'
    },
    containerLeftText: {
        fontFamily: "Roboto-Bold",
        fontSize: 50,
    },
    containerLeft: {
        backgroundColor: "#9b59b6",
        padding: 10,
        width: "20%",
        height: "100%",
        alignItems:"center",
        borderTopLeftRadius:5,
        borderBottomLeftRadius:5
    },
    list: {
        alignItems: "center",
        flexDirection: "row",
        borderRadius: 10,
        margin: 10,
        borderWidth: 1,
        height: 130
    },
    listText: {
        fontSize: 16,
        marginLeft: 10,
        marginTop: 10,
        marginRight: 5,
        fontFamily: "Roboto-Bold",
        color: "#9b59b6",
        flex: 1,
        flexWrap: "wrap",
        marginBottom: 0
    },
    listTextSubTitle: {
        fontSize: 15,
        marginLeft: 10,
        fontFamily: "Roboto",
        color: "#9b59b6",
        flex: 1,
        flexWrap: "wrap"
    },
    listArrow: {
        position: "absolute",
        right: 15
    },
    buttonLihatSelengkapnya: {
        backgroundColor: "#9b59b6",
        width: "40%",
        padding: 8,
        borderRadius: 10,
        marginLeft: 10,
        alignItems: 'center',
        marginTop: 20,
        marginBottom: 10
    }
})
