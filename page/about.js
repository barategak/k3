import React, { Component } from 'react'
import { View, Text, StyleSheet,ScrollView, ActivityIndicator, Image, ImageBackground, TouchableOpacity } from 'react-native'
import Header from "../component/header.js"
import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome'
import WebView from 'react-native-webview'
import { faStickyNote, faArrowRight, faPhone, faInfo, faEnvelope, faHome } from '@fortawesome/free-solid-svg-icons'

const about_text = "Andreas Matulandi dilahirkan di Indramayu tanggal 29 Oktober 1994, anak tunggal dari pasangan Bapak Albertus Matulandi dan Heni Suryati. Pendidikan formal di Taman Kanak-kanak (TK) Tunas Harapan Kab. Indramayu pada tahun 1999-2001. Dilanjutkan pendidikan dasar di tempuh di SDN 1 Ujung Jaya Kab. Indramayu pada tahun 2001-2007, kemudian pada tahun 2007 melanjutkan ke pendidikan menengah di SMPN 1 Jatibarang Kab. Indramayu dan lulus pada tahun 2010. Pendidikan selanjutnya di tempuh di SMKN 1 Singosari Kab. Malang pada program studi Ototronik pada tahun 2010-2013. Pada tahun 2013 melanjutkan pendidikan di perguruan tinggi tepatnya Universitas Negeri Malang dan mengambil program studi S1 Pendidikan Teknik Otomotif. Penulis juga pernah bergabung dengan anggota OSIS dan Marching Band sewaktu menempuh jenjang pendidikan SMP, serta penulis pernah bergabung bersama keluarga HMJ TM, PM3, Jongring Salaka, sewaktu Kuliah di Universitas Negeri Malang, serta Organisasi Daerah Indramayu (Ikawiradharma) Malang. Pada tahun 2018 penulis pernah melanjutkan jenjang PPG di Universitas Negeri Malang, dan tahun 2019 hingga sekarang penulis melanjutkan studi lanjut Pascasarjana yaitu Magister Pendidikan Kejuruan di Universitas Negeri Malang juga."

export default class about extends Component {

    

    render() {
        return (
            <ScrollView style={{backgroundColor: "white", height: "100%"}}>
                <Header></Header>

                <View style={style.titleContainerStyle}>
                    <FontAwesomeIcon icon={faInfo} color={"#9b59b6"} size={30} />
                    <Text style={style.textTitle}>Tentang Pengembang</Text>
                </View>

                <View style={style.content}>
                    <Image source={require("../asset/mase.jpg")} style={style.gambarIlus} />
                    <Text style={{fontFamily:"Roboto-Bold", color:"white", fontSize: 20, marginTop: 20}}>ANDREAS MATULANDI</Text>
                    <View style={{alignItems: "flex-start", width: "80%", marginBottom: 20}}>
                        <View style={{flexDirection: "row", marginTop: 10}}>
                            <FontAwesomeIcon icon={faEnvelope} color={"white"} size={18} style={{marginTop: 3}} />
                            <Text style={{fontFamily:"Roboto-Bold", color:"white", fontSize: 16, marginLeft: 10}}>mat.andreas29@gmail.com</Text>
                        </View>
                        <View style={{flexDirection: "row", marginTop: 10}}>
                            <FontAwesomeIcon icon={faPhone} color={"white"} size={18} style={{marginTop: 3}} />
                            <Text style={{fontFamily:"Roboto-Bold", color:"white", fontSize: 16, marginLeft: 10}}>087717881771</Text>
                        </View>
                        <View style={{flexDirection: "row", marginTop: 10}}>
                            <FontAwesomeIcon icon={faHome} color={"white"} size={18} style={{marginTop: 3}} />
                            <Text style={{fontFamily:"Roboto-Bold", color:"white", fontSize: 16, marginLeft: 10}}>Desa Ujung Aris Blok Pintu Air, Widasari, Kab. Indramayu</Text>
                        </View>
                    </View>
                </View>
                

                <View style={{marginTop: 30, marginLeft: 30, marginRight: 30, marginBottom: 0}}>
                    <Text style={style.aboutTextStyle}>{about_text}</Text>
                </View>


                <View style={style.titleContainerStyle}>
                    <FontAwesomeIcon icon={faInfo} color={"#9b59b6"} size={30} />
                    <Text style={style.textTitle}>Dosen Pembimbing 1</Text>
                </View>

                <View style={style.content}>
                    <Image source={require("../asset/dosbing1.jpg")} style={style.gambarIlus2} />
                    <Text style={{fontFamily:"Roboto-Bold", color:"white", fontSize: 20, marginTop: 20}}>Prof. Dr. Ir. Djoko Kustono, M.Pd.</Text>
                    <View style={{alignItems: "flex-start", width: "80%", marginBottom: 20}}>
                        <View style={{flexDirection: "row", marginTop: 10}}>
                            <FontAwesomeIcon icon={faEnvelope} color={"white"} size={18} style={{marginTop: 3}} />
                            <Text style={{fontFamily:"Roboto-Bold", color:"white", fontSize: 16, marginLeft: 10}}>djoko.kustono.ft@um.ac.id</Text>
                        </View>
                        <View style={{flexDirection: "row", marginTop: 10}}>
                            <Text style={{fontFamily:"Roboto-Bold", color:"white", fontSize: 16, marginLeft: 0}}>Rank/Class : </Text>
                            <Text style={{fontFamily:"Roboto-Bold", color:"white", fontSize: 16, marginLeft: 10}}>Intermediate Main Advisor, IV / d</Text>
                        </View>
                        <View style={{flexDirection: "row", marginTop: 10}}>
                            <Text style={{fontFamily:"Roboto-Bold", color:"white", fontSize: 16, marginLeft: 0}}>Date of Birth : </Text>
                            <Text style={{fontFamily:"Roboto-Bold", color:"white", fontSize: 16, marginLeft: 10}}>Solo, 15 September 1953</Text>
                        </View>
                        <View style={{flexDirection: "row", marginTop: 10}}>
                            <Text style={{fontFamily:"Roboto-Bold", color:"white", fontSize: 16, marginLeft: 0}}>TMT Professor : </Text>
                            <Text style={{fontFamily:"Roboto-Bold", color:"white", fontSize: 16, marginLeft: 10}}>01 August 2004</Text>
                        </View>
                        <View style={{flexDirection: "row", marginTop: 10, width: "50%"}}>
                            <Text style={{fontFamily:"Roboto-Bold", color:"white", fontSize: 16, marginLeft: 0}}>Knowledge Field : </Text>
                            <Text style={{fontFamily:"Roboto-Bold", color:"white", fontSize: 16, marginLeft: 10}}>Occupational Safety and Health Sciences</Text>
                        </View>
                        <View style={{flexDirection: "row", marginTop: 10}}>
                            <Text style={{fontFamily:"Roboto-Bold", color:"white", fontSize: 16, marginLeft: 0}}>Department : </Text>
                            <Text style={{fontFamily:"Roboto-Bold", color:"white", fontSize: 16, marginLeft: 10}}>Mechanical Engineering</Text>
                        </View>
                    </View>
                </View>


                <View style={style.titleContainerStyle}>
                    <FontAwesomeIcon icon={faInfo} color={"#9b59b6"} size={30} />
                    <Text style={style.textTitle}>Dosen Pembimbing 2</Text>
                </View>

                <View style={style.content}>
                    <Image source={require("../asset/dosbing2.jpg")} style={style.gambarIlus2} />
                    <Text style={{fontFamily:"Roboto-Bold", color:"white", fontSize: 20, marginTop: 20}}>Dr. H. Hakkun Elmunsyah, S.T, M.T.</Text>
                    <View style={{alignItems: "flex-start", width: "80%", marginBottom: 20}}>
                        <View style={{flexDirection: "row", marginTop: 10}}>
                            <FontAwesomeIcon icon={faEnvelope} color={"white"} size={18} style={{marginTop: 3}} />
                            <Text style={{fontFamily:"Roboto-Bold", color:"white", fontSize: 16, marginLeft: 10}}>hakkun@um.ac.id</Text>
                        </View>
                        <View style={{flexDirection: "row", marginTop: 10}}>
                            <Text style={{fontFamily:"Roboto-Bold", color:"white", fontSize: 16, marginLeft: 0}}>Rank/Class : </Text>
                            <Text style={{fontFamily:"Roboto-Bold", color:"white", fontSize: 16, marginLeft: 10}}>Main Advisor, IV / b</Text>
                        </View>
                        <View style={{flexDirection: "row", marginTop: 10, width: "50%"}}>
                            <Text style={{fontFamily:"Roboto-Bold", color:"white", fontSize: 16, marginLeft: 0}}>Knowledge Field : </Text>
                            <Text style={{fontFamily:"Roboto-Bold", color:"white", fontSize: 16, marginLeft: 10}}>Sistem Information Management and Education Development Vovational Highscool</Text>
                        </View>
                    </View>
                </View>
                

                
            </ScrollView>
        )
    }
}

const style = StyleSheet.create({
    content: {
        marginTop: 0,
        alignItems: "center",
        backgroundColor: "#9b59b6",
        borderRadius: 20,
        marginLeft: 10,
        marginRight: 10
    },
    
    textItem: {
        color: "#9b59b6",
        fontSize: 25,
        fontFamily: "Roboto"
    },
    gambarIlus: {
        height: 400,
        resizeMode: "cover",
        marginTop: 20
    },
    gambarIlus2: {
        height: 400,
        resizeMode: "cover",
        width: "100%",
        marginTop: 20
    },
    button: {
        backgroundColor: "#9b59b6",
        padding: 10,
        width: "70%",
        borderRadius: 1000,
        alignItems: "center"
    },
    textButton: {
        color: "white",
        fontSize: 18,
        fontFamily: "Roboto-Bold"
    },titleContainerStyle: {
        flexDirection: "row",
        marginTop: 50,
        marginBottom: 20,
        marginLeft: 10
    },
    textTitle: {
        fontSize: 20,
        color: "black",
        fontFamily: "Roboto-Medium",
        marginLeft: 5,
        color: "#9b59b6",
        marginTop: 5
    },
    aboutTextStyle: {
        fontFamily: "Roboto",
        fontSize: 15,
        color: "black",
        flexWrap: 'wrap'
    }
})