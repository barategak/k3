import React, { Component } from 'react'
import {View, Text, TouchableOpacity, ScrollView, ImageBackground} from "react-native"

export default class Materi extends Component {

    onPressButton = () => {
        console.log("Hello World")
    }

    render() {
        return (
            <ScrollView>
                <ImageBackground source={require("../asset/6a038ee8253fd1f155ca504a6e3f25b1.jpg")}>
                <View style={{ padding: 10}}>
                    
                    <TouchableOpacity style={{width: "100%", backgroundColor: "#00a8ff", height: 130, padding: 5, elevation: 10}} 
                        onPress={() => {
                            this.props.navigation.navigate("DetailMateri", {id: 1})
                        }}
                    >
                        <Text style={{fontFamily: "Roboto-Bold", fontSize: 20, color: "black", marginTop: 30, marginLeft: 10, color:"white"}}>Merancang Strategi Pengendalian Resiko K3 di Tempat Kerja</Text>
                    </TouchableOpacity>



                    <TouchableOpacity style={{width: "100%", backgroundColor: "#9c88ff", height: 130, padding: 5, marginTop: 15, elevation: 10}} onPress={() => {
                            this.props.navigation.navigate("DetailMateri", {id: 2})
                        }}>
                        <Text style={{fontFamily: "Roboto-Bold", fontSize: 20, color: "black", marginTop: 30, marginLeft: 10, color:"white"}}>Merancang Sistem Tanggap Darurat</Text>
                        
                    </TouchableOpacity>

                    <TouchableOpacity style={{width: "100%", backgroundColor: "#0097e6", height: 130, padding: 5, marginTop: 15, elevation: 10}} 
                        onPress={() => {
                            this.props.navigation.navigate("DetailMateri", {id: 3})
                        }}
                    >
                            <Text style={{fontFamily: "Roboto-Bold", fontSize: 20, color: "black", marginTop: 30, marginLeft: 10, color:"white"}}>Melakukan Komunikasi K3</Text>
                    </TouchableOpacity>

                    <TouchableOpacity style={{width: "100%", backgroundColor: "#4cd137", height: 130, padding: 5, marginTop: 15, elevation: 10}} 
                        onPress={() => {
                            this.props.navigation.navigate("DetailMateri", {id: 4})
                        }}
                    >
                        <Text style={{fontFamily: "Roboto-Bold", fontSize: 20, color: "black", marginTop: 30, marginLeft: 10, color:"white"}}>Mengawasi Pelaksanaan Izin Kerja</Text>
                    </TouchableOpacity>


                    <TouchableOpacity style={{width: "100%", backgroundColor: "#e1b12c", height: 130, padding: 5, marginTop: 10, elevation: 10}} 
                        onPress={() => {
                            this.props.navigation.navigate("DetailMateri", {id: 5})
                        }}
                    >
                        <Text style={{fontFamily: "Roboto-Bold", fontSize: 20, color: "black", marginTop: 30, marginLeft: 10, color:"white"}}>Melakukan Pengukuran Faktor Bahaya di Tempat Kerja</Text>
                        
                    </TouchableOpacity>
                    <TouchableOpacity style={{width: "100%", backgroundColor: "#fbc531", height: 130, padding: 5, marginTop: 10, elevation: 10}} 
                        onPress={() => {
                            this.props.navigation.navigate("DetailMateri", {id: 6})
                        }}
                    >
                        <Text style={{fontFamily: "Roboto-Bold", fontSize: 20, color: "black", marginTop: 30, marginLeft: 10, color:"white"}}>Mengelola Pertolongan Pertama pada Kecelakaan (P3K) di Tempat Kerja</Text>
                    </TouchableOpacity>
                    
                    
                </View>

                </ImageBackground>

            </ScrollView>
        )
    }
}
