import React, { Component } from 'react'
import { View, Text, StyleSheet,ScrollView, ActivityIndicator, Image, ImageBackground, TouchableOpacity } from 'react-native'
import Header from "../component/header.js"
import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome'
import { faStickyNote, faArrowRight, faUser, faInfo, faBook } from '@fortawesome/free-solid-svg-icons'
export default class menuAbout extends Component {
    render() {
        return (
            <ScrollView style={{backgroundColor: "white", height: "100%"}}>
                <Header></Header>

                <View style={style.titleContainerStyle}>
                    <FontAwesomeIcon icon={faInfo} color={"#9b59b6"} size={30} />
                    <Text style={style.textTitle}>Menu About</Text>
                </View>

                <View style={style.container}>
                    <TouchableOpacity style={style.list}
                        onPress={() => {
                            this.props.navigation.navigate("About")
                        }}
                    >
                        <FontAwesomeIcon icon={ faUser } color={'white'} size={30} />
                        <Text style={style.listText}>Tentang Pengembang</Text>
                        <FontAwesomeIcon icon={ faArrowRight } color={'white'} size={28} style={style.listArrow} />
                    </TouchableOpacity>
                    <TouchableOpacity style={style.list}
                        onPress={() => {
                            this.props.navigation.navigate("MenuDafPus")
                        }}
                    >
                        <FontAwesomeIcon icon={ faBook } color={'white'} size={30} />
                        <Text style={style.listText}>Daftar Pustaka</Text>
                        <FontAwesomeIcon icon={ faArrowRight } color={'white'} size={28} style={style.listArrow} />
                    </TouchableOpacity>
                    
                </View>
            </ScrollView>
        
        )
    }
  
}

const style = StyleSheet.create({
    container: {
        marginTop: 10
    },
    list: {
        alignItems: "center",
        flexDirection: "row",
        padding: 20,
        backgroundColor: "#9b59b6",
        borderRadius: 10,
        margin: 10,
    },
    listText: {
        fontSize: 18,
        marginLeft: 10,
        fontFamily: "Roboto-Medium",
        color: "white",
    },
    listArrow: {
        position: "absolute",
        right: 15
    },textTitle: {
        fontSize: 20,
        color: "black",
        fontFamily: "Roboto-Medium",
        marginLeft: 5,
        color: "#9b59b6",
        marginTop: 5
    },titleContainerStyle: {
        flexDirection: "row",
        marginTop: 50,
        marginBottom: 0,
        marginLeft: 10
    }
})