import React, { Component } from 'react'
import { View, Text, StyleSheet,ScrollView, ActivityIndicator, Image, ImageBackground, TouchableOpacity } from 'react-native'
import Header from "../component/header.js"

export default class preload extends Component {
    render() {
        return (
            <ScrollView style={{backgroundColor: "white", height: "100%"}}>
                <Header></Header>

                <View style={style.content}>
                    <Text style={style.textItem}>KOMPETENSI K3</Text>
                    <Text style={style.textItem2}>Berdasarkan Keputusan MENAKER</Text>
                    <Text style={style.textItem2}>No. 38 Tahun 2019</Text>
                    <Text style={style.textItem2}>Aplikasi Untuk Dosen</Text>

                    <Image source={require("../asset/10088.jpg")} style={style.gambarIlus} />

                    <TouchableOpacity style={style.button}
                        onPress={() => {
                            this.props.navigation.navigate("MainMenu")
                        }}
                    >
                        <Text style={style.textButton}>MULAI</Text>
                    </TouchableOpacity>
                </View>
            </ScrollView>
        )
    }
}

const style = StyleSheet.create({
    content: {
        marginTop: 50,
        alignItems: "center"
    },
    textItem: {
        color: "#9b59b6",
        fontSize: 25,
        fontFamily: "Roboto"
    },
    textItem2: {
        color: "#9b59b6",
        fontSize: 20,
        fontFamily: "Roboto",
        textAlign: "center"
    },
    gambarIlus: {
        width: "100%",
        height: 300,
        resizeMode: "center",
        marginTop: 20
    },
    button: {
        backgroundColor: "#9b59b6",
        padding: 10,
        width: "70%",
        borderRadius: 1000,
        alignItems: "center"
    },
    textButton: {
        color: "white",
        fontSize: 18,
        fontFamily: "Roboto-Bold"
    }
})