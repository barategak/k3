import React, { Component } from 'react'
import { View, Text, StyleSheet, ActivityIndicator, Image, ImageBackground } from 'react-native'
import styleComponent from "../component/style"

export default class SplashScreen extends Component {

    componentDidMount() {
    }

    render() {
        return (
            <View style={styles.container}>

                
                
                <View style={{marginTop: "30%"}}>
                    <Image source={require("../asset/Logo.png")} style={{width: "100%", height: 170}} resizeMode='contain' />
                </View>

                <View style={{marginTop: "10%", alignItems:"center"}}>
                    <Text style={styles.headerText}>Bahan Ajar K3</Text>
                    <Text style={styles.headerText}>Tentang Kompetensi K3</Text>
                    <Text style={styles.headerSubText}>Aplikasi Untuk Dosen</Text>
                </View>

                <View style={{marginTop: "5%", alignItems:"center"}}>
                    <Text style={styles.pembuatText}>Dibuat oleh Andreas Matulandi (190551856017)</Text>
                </View>
                <View style={{position:'absolute', bottom:80, left: "50%"}}>
                    <ActivityIndicator size="large" color="white" />
                </View>
                
                
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 0,
        backgroundColor: "white",
        width: "100%",
        height: "100%",
        padding: 20
    },
    headerText: {
        fontFamily: "Roboto-Bold",
        fontSize: 30,
        color: "black"
    },
    headerSubText: {
        fontFamily: "Roboto-Bold",
        fontSize: 25,
        color: "black"
    },
    pembuatText: {
        fontFamily: "Roboto-Medium",
        fontSize: 15,
        color: "black"
    }
})