import React, { Component } from 'react'
import { Text, View, TouchableOpacity, ScrollView, ActivityIndicator } from 'react-native'
import { WebView } from 'react-native-webview';

export default class detailMateri extends Component {

    constructor(props) {
        super();
        this.state = {
            url: "",
            isLoading: true
        }
      }

    componentDidMount() {
        //let id = this.props.route.params.id;
        let judulMateri = this.props.route.params.judul;
        let url = this.props.route.params.url;

        this.setState({url: url});

        // if(id == 1) {
        //     this.setState({
        //         url : "https://docs.google.com/presentation/d/e/2PACX-1vSPwohAU5l_GSLAmPRQBJYfFOSadz-DdH3RGVKWFe1jzaQRaPqRHL-JZwOKZBTWQg/pub?start=false&loop=false&delayms=3000#"
        //     })
        //     judulMateri = "Merancang Strategi Pengendalian Resiko K3 di Tempat Kerja"
        // }else if(id == 2) {
        //     this.setState({
        //         url : "https://docs.google.com/presentation/d/e/2PACX-1vTM4qhPuD1oKqSuTtAdXvWkUsfUAHPIaTJOp7t1VQHkVUtQW7eAx167QIEy4IOLaw/pub?start=false&loop=false&delayms=3000"
        //     })
        //     judulMateri = "Merancang Sistem Tanggap Darurat"
        // }else if(id == 3) {
        //     this.setState({
        //         url : "https://docs.google.com/presentation/d/e/2PACX-1vQTS6-FP1kLR-iMHCmsUCNOc62BBcBTEaCqnlqDGpXzxBRijFzkDDm43B2D71Q4Dw/pub?start=false&loop=false&delayms=3000"
        //     })
        //     judulMateri = "Melakukan Komunikasi K3"
        // }else if(id == 4) {
        //     this.setState({
        //         url : "https://docs.google.com/presentation/d/e/2PACX-1vQ1m56GoyhAGuMMPVPDeEC9XnMZ8HXPmn0tD4KjM2phREUS27MLyWPxj83wmTYU_A/pub?start=false&loop=false&delayms=3000"
        //     })
        //     judulMateri = "Mengawasi Pelaksanaan Izin Kerja"
        // }else if(id == 5) {
        //     this.setState({
        //         url : "https://docs.google.com/presentation/d/e/2PACX-1vQmiQ_IOmiLaBST_ySWqubx5OLVXLPZyyY_gmOsqW70HsJX8O6xBE0J8Z8AhI5aWg/pub?start=false&loop=false&delayms=3000"
        //     })
        //     judulMateri = "Melakukan Pengukuran Faktor Bahaya di Tempat Kerja"
        // }else if(id == 6) {
        //     this.setState({
        //         url : "https://docs.google.com/presentation/d/e/2PACX-1vSn369k7Y5xmXiYqP_xJh0d6YVbb26kwjgh5DCS3W3KpaJXnWORcHrDE70x5v_jzA/pub?start=false&loop=false&delayms=3000"
        //     })
        //     judulMateri = "Mengelola Pertolongan Pertama pada Kecelakaan (P3K) di Tempat Kerja"
        // }

        this.props.navigation.setOptions({ 
            title: judulMateri
        })
        
    }
    render() {
        return (

            <WebView source={{uri: this.state.url}} onLoadEnd={() => {
                this.setState({isLoading: false});
              }} />
            
        
               
        )
    }
}
