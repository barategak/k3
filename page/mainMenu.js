import React, { Component } from 'react'
import { View, Text, StyleSheet,ScrollView, ActivityIndicator, Image, ImageBackground, TouchableOpacity } from 'react-native'
import Header from "../component/header.js"
import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome'
import { faStickyNote, faArrowRight, faPen, faInfo, faBook, faQuestion } from '@fortawesome/free-solid-svg-icons'

export default class mainMenu extends Component {
    render() {
        return (
            <ScrollView style={{backgroundColor: "white", height: "100%"}}>
                <Header></Header>
                <View style={style.container}>
                    <TouchableOpacity style={style.list}
                        onPress={() => {
                            this.props.navigation.navigate("MateriMenu")
                        }}
                    >
                        <FontAwesomeIcon icon={ faStickyNote } color={'white'} size={30} />
                        <Text style={style.listText}>Materi</Text>
                        <FontAwesomeIcon icon={ faArrowRight } color={'white'} size={28} style={style.listArrow} />
                    </TouchableOpacity>
                    <TouchableOpacity style={style.list}
                        onPress={() => {
                            this.props.navigation.navigate("SoalMenu")
                        }}
                    >
                        <FontAwesomeIcon icon={ faQuestion } color={'white'} size={30} />
                        <Text style={style.listText}>Kuis</Text>
                        <FontAwesomeIcon icon={ faArrowRight } color={'white'} size={28} style={style.listArrow} />
                    </TouchableOpacity>
                    <TouchableOpacity style={style.list}
                        onPress={() => {
                            this.props.navigation.navigate("DetailMateri", {url: 'https://pdfhost.io/v/5EAXMypIe_Buku_Manual_Dosen_K3', judul: 'Buku Panduan'})
                        }}
                    >
                        <FontAwesomeIcon icon={ faBook } color={'white'} size={30} />
                        <Text style={style.listText}>Buku Panduan</Text>
                        <FontAwesomeIcon icon={ faArrowRight } color={'white'} size={28} style={style.listArrow} />
                    </TouchableOpacity>
                    <TouchableOpacity style={style.list}
                        onPress={() => {
                            this.props.navigation.navigate("subMenuAbout")
                        }}
                    >
                        <FontAwesomeIcon icon={ faInfo } color={'white'} size={30} />
                        <Text style={style.listText}>Tentang</Text>
                        <FontAwesomeIcon icon={ faArrowRight } color={'white'} size={28} style={style.listArrow} />
                    </TouchableOpacity>
                </View>
            </ScrollView>
        
        )
    }
}
const style = StyleSheet.create({
    container: {
        marginTop: 50
    },
    list: {
        alignItems: "center",
        flexDirection: "row",
        padding: 20,
        backgroundColor: "#9b59b6",
        borderRadius: 10,
        margin: 10,
    },
    listText: {
        fontSize: 18,
        marginLeft: 10,
        fontFamily: "Roboto-Medium",
        color: "white",
    },
    listArrow: {
        position: "absolute",
        right: 15
    }
})