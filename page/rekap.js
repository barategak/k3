import React, { Component } from 'react';
import { View, Text, StyleSheet,ScrollView, ActivityIndicator, Image, ImageBackground, TouchableOpacity } from 'react-native'
import { WebView } from 'react-native-webview';


export default class rekap extends Component {

    constructor(props) {
        super();
        this.state = {
            
        }
      }

    render() {
        return (
            <View style={{flex: 1}}>
                <WebView ref={(ref) => { this.webview = ref; }} style={{flex: 1}} source={{uri: "https://docs.google.com/spreadsheets/d/e/2PACX-1vQbH_yT7yavAW4ux-_YOBqodRC6TUsEBjWLndIB2lIzKxmSLLW8Vx6IcOLBQrt_Ucttc7xAfbuIdPGw/pubhtml?gid=0&single=true"}} onLoadEnd={() => {
                        this.setState({isLoading: false});
                    }} />
                <TouchableOpacity style={{backgroundColor: "#9b59b6"}} onPress={() => {
                    this.webview.reload();
                }}>
                    <Text style={style.textItem}>Refresh Data</Text>
                </TouchableOpacity>
            </View>
        )
    }
}

const style = StyleSheet.create({
    
    textItem: {
        color: "white",
        fontSize: 20,
        padding: 10,
        textAlign: "center",
        fontFamily: "Roboto"
    }
    
})